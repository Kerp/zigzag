// <copyright file="GPGSIds.cs" company="Google Inc.">
// Copyright (C) 2015 Google Inc. All Rights Reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//    limitations under the License.
// </copyright>

///
/// This file is automatically generated DO NOT EDIT!
///
/// These are the constants defined in the Play Games Console for Game Services
/// Resources.
///


public static class GPGSIds
{
        public const string achievement_action = "CgkIs5Ka1o4YEAIQFw"; // <GPGSID>
        public const string achievement_arcade = "CgkIs5Ka1o4YEAIQFQ"; // <GPGSID>
        public const string achievement_player = "CgkIs5Ka1o4YEAIQJg"; // <GPGSID>
        public const string achievement_informer = "CgkIs5Ka1o4YEAIQNw"; // <GPGSID>
        public const string achievement_dressy = "CgkIs5Ka1o4YEAIQLw"; // <GPGSID>
        public const string achievement_circle = "CgkIs5Ka1o4YEAIQGg"; // <GPGSID>
        public const string achievement_want_peace__quite = "CgkIs5Ka1o4YEAIQEA"; // <GPGSID>
        public const string achievement_flex = "CgkIs5Ka1o4YEAIQEg"; // <GPGSID>
        public const string achievement_empire = "CgkIs5Ka1o4YEAIQNg"; // <GPGSID>
        public const string achievement_casual = "CgkIs5Ka1o4YEAIQAg"; // <GPGSID>
        public const string achievement_seeker = "CgkIs5Ka1o4YEAIQHg"; // <GPGSID>
        public const string achievement_lodger = "CgkIs5Ka1o4YEAIQIw"; // <GPGSID>
        public const string achievement_miner = "CgkIs5Ka1o4YEAIQIA"; // <GPGSID>
        public const string achievement_fashionable = "CgkIs5Ka1o4YEAIQMA"; // <GPGSID>
        public const string achievement_master = "CgkIs5Ka1o4YEAIQKQ"; // <GPGSID>
        public const string achievement_socialism = "CgkIs5Ka1o4YEAIQPA"; // <GPGSID>
        public const string achievement_earner = "CgkIs5Ka1o4YEAIQHw"; // <GPGSID>
        public const string achievement_hypersphere = "CgkIs5Ka1o4YEAIQHQ"; // <GPGSID>
        public const string achievement_consumer = "CgkIs5Ka1o4YEAIQBg"; // <GPGSID>
        public const string achievement_racing = "CgkIs5Ka1o4YEAIQFg"; // <GPGSID>
        public const string achievement_parttime_job = "CgkIs5Ka1o4YEAIQMw"; // <GPGSID>
        public const string achievement_want_to_know_everything = "CgkIs5Ka1o4YEAIQDg"; // <GPGSID>
        public const string achievement_lucky = "CgkIs5Ka1o4YEAIQBA"; // <GPGSID>
        public const string achievement_owner = "CgkIs5Ka1o4YEAIQJQ"; // <GPGSID>
        public const string achievement_contentmaker = "CgkIs5Ka1o4YEAIQOQ"; // <GPGSID>
        public const string achievement_guest = "CgkIs5Ka1o4YEAIQBQ"; // <GPGSID>
        public const string achievement_collector = "CgkIs5Ka1o4YEAIQIQ"; // <GPGSID>
        public const string achievement_guide = "CgkIs5Ka1o4YEAIQCQ"; // <GPGSID>
        public const string achievement_influencer = "CgkIs5Ka1o4YEAIQOA"; // <GPGSID>
        public const string achievement_dont_write_to_me_anymore = "CgkIs5Ka1o4YEAIQPQ"; // <GPGSID>
        public const string achievement_just_wanna_play = "CgkIs5Ka1o4YEAIQCg"; // <GPGSID>
        public const string achievement_too_lazy = "CgkIs5Ka1o4YEAIQDQ"; // <GPGSID>
        public const string achievement_investor = "CgkIs5Ka1o4YEAIQLg"; // <GPGSID>
        public const string achievement_icon = "CgkIs5Ka1o4YEAIQMg"; // <GPGSID>
        public const string achievement_businessman = "CgkIs5Ka1o4YEAIQLQ"; // <GPGSID>
        public const string achievement_aaa = "CgkIs5Ka1o4YEAIQGA"; // <GPGSID>
        public const string achievement_king_of_the_mountain = "CgkIs5Ka1o4YEAIQDw"; // <GPGSID>
        public const string achievement_unbeaten = "CgkIs5Ka1o4YEAIQKg"; // <GPGSID>
        public const string achievement_star = "CgkIs5Ka1o4YEAIQMQ"; // <GPGSID>
        public const string achievement_shopaholic = "CgkIs5Ka1o4YEAIQLA"; // <GPGSID>
        public const string achievement_mouthpiece = "CgkIs5Ka1o4YEAIQEQ"; // <GPGSID>
        public const string achievement_dressed = "CgkIs5Ka1o4YEAIQBw"; // <GPGSID>
        public const string achievement_get_out_of_the_wrong_side_bed = "CgkIs5Ka1o4YEAIQCw"; // <GPGSID>
        public const string achievement_dancer = "CgkIs5Ka1o4YEAIQDA"; // <GPGSID>
        public const string achievement_avid_buyer = "CgkIs5Ka1o4YEAIQKw"; // <GPGSID>
        public const string achievement_ball = "CgkIs5Ka1o4YEAIQHA"; // <GPGSID>
        public const string achievement_professional = "CgkIs5Ka1o4YEAIQKA"; // <GPGSID>
        public const string achievement_host = "CgkIs5Ka1o4YEAIQJA"; // <GPGSID>
        public const string achievement_career = "CgkIs5Ka1o4YEAIQNA"; // <GPGSID>
        public const string achievement_point = "CgkIs5Ka1o4YEAIQAw"; // <GPGSID>
        public const string achievement_business = "CgkIs5Ka1o4YEAIQNQ"; // <GPGSID>
        public const string achievement_gift = "CgkIs5Ka1o4YEAIQCA"; // <GPGSID>
        public const string leaderboard_high_score = "CgkIs5Ka1o4YEAIQAA"; // <GPGSID>
        public const string achievement_enthusiast = "CgkIs5Ka1o4YEAIQIg"; // <GPGSID>
        public const string achievement_media = "CgkIs5Ka1o4YEAIQOg"; // <GPGSID>
        public const string achievement_beginner = "CgkIs5Ka1o4YEAIQEw"; // <GPGSID>
        public const string achievement_confident_user = "CgkIs5Ka1o4YEAIQJw"; // <GPGSID>
        public const string achievement_sphere = "CgkIs5Ka1o4YEAIQGw"; // <GPGSID>
        public const string achievement_circumference = "CgkIs5Ka1o4YEAIQGQ"; // <GPGSID>
        public const string achievement_native_speaker = "CgkIs5Ka1o4YEAIQOw"; // <GPGSID>

}

