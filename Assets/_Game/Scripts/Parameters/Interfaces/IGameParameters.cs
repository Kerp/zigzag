﻿
namespace _Game.Scripts.Parameters.Interfaces
{
    public interface IGameParameters
    {
        IBallParameters Ball { get; }
        IPlatformParameters Platform { get; }
        IDirectionParameters Direction { get; }
        IPlatformManagerParameters PlatformManager { get; }
        IGemManagerParameters GemManager { get; }
        ITurnsScoreParameters TurnsScore { get; }
        IDistanceScoreParameters DistanceScore { get; }
    }
}
