﻿namespace _Game.Scripts.Parameters.Interfaces
{
    public interface ITurnsScoreParameters
    {
        int scorePerChangingDirection { get; }
    }
}
