﻿using UnityEngine;

namespace _Game.Scripts.Parameters.Interfaces
{
    public interface IDirectionParameters
    {
        Vector3[] directions { get; }
    }
}
