﻿namespace _Game.Scripts.Parameters.Interfaces
{
    public interface IGemScoreParameters
    {
        int scorePerGem { get; }
    }
}
