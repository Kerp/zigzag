﻿using System;
using _Game.Scripts.Parameters.Interfaces;
using UnityEngine;

namespace _Game.Scripts.Parameters.Classes
{
    [Serializable]
    class BallParameters : IBallParameters
    {
        #region SERIALIZE_FIELDS
        [Tooltip("Начальная/постоянная скорость")]
        [SerializeField] private float _StartSpeed = 0f;

        [Tooltip("Ускорение. 0 - равномерное движение")]
        [SerializeField] private float _Acceleration = 0f;

        [Tooltip("Нижняя граница скорости")]
        [SerializeField] private float _MinSpeed = 0f;

        [Tooltip("Верхняя граница скорости")]
        [SerializeField] private float _MaxSpeed = 0f;
        #endregion // SERIALIZE_FIELDS

        #region PUBLIC_VALUES
        public float startSpeed => _StartSpeed;
        public float acceleration => _Acceleration;
        public float minSpeed => _MinSpeed;
        public float maxSpeed => _MaxSpeed;
        #endregion // PUBLIC_VALUES
    }
}
