﻿using System;
using _Game.Scripts.Parameters.Interfaces;
using UnityEngine;

namespace _Game.Scripts.Parameters.Classes
{
    [Serializable]
    class PlatformManagerParameters : IPlatformManagerParameters
    {
        #region SERIALIZE_FIELDS
        [Tooltip("Вероятность, с которой направление движение дороги меняет своё направление")]
        [SerializeField] private AnimationCurve _ProbailityChangeDirection = new AnimationCurve();

        [Min(0)] [Tooltip("Количество платформ, без учёта ширины дороги, которое может оставаться после шарика")]
        [SerializeField] private int _StepThresholdBack = 0;

        [Min(0)] [Tooltip("Количество платформ, без учёта ширины дороги, которое видит игрок")]
        [SerializeField] private int _StepsThresholdForward = 0;

        [Min(0)] [Tooltip("Количество платформ, без учёта ширины дороги, которое будет установленно фиксированно " +
            "в первом указанном в Directoins направлении. Чтобы на старте уровня не было резкого ухода в сторону.")]
        [SerializeField] private int _InitialStepsOneDirection = 0;

        [Min(1)] [Tooltip("Размер квадрата из платформ, на котором появляется шарик.")]
        [SerializeField] private int _InitialAreaSize = 0;
        
        [Min(0)] [Tooltip("Количество заранее просчитанных шагов.")]
        [SerializeField] private int _InitialPrecalculatedSteps = 0;
        
        #endregion // SERIALIZE_FIELDS

        #region PUBLIC_VALUES
        
        public AnimationCurve probabilityChangeDirection => _ProbailityChangeDirection;
        public int stepThresholdBack => _StepThresholdBack;
        public int stepThresholdForward => _StepsThresholdForward;
        public int initialStepsOneDirection => _InitialStepsOneDirection;
        public int initialAreaSize => _InitialAreaSize;
        public int initialPrecalculatedSteps => _InitialPrecalculatedSteps;

        #endregion // PUBLIC_VALUES
    }
}
