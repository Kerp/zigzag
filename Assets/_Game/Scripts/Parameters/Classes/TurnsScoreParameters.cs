﻿using System;
using _Game.Scripts.Parameters.Interfaces;
using UnityEngine;

namespace _Game.Scripts.Parameters.Classes
{
    [Serializable]
    public class TurnsScoreParameters : ITurnsScoreParameters
    {
        [Min(0)]
        [Tooltip("Количество очков за каждую смену направления")]
        [SerializeField] private int _ScorePerChangingDirection = 0;

        public int scorePerChangingDirection => _ScorePerChangingDirection;
    }
}
