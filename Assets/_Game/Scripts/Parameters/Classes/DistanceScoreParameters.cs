﻿using System;
using _Game.Scripts.Parameters.Interfaces;
using UnityEngine;

namespace _Game.Scripts.Parameters.Classes
{
    [Serializable]
    class DistanceScoreParameters : IDistanceScoreParameters
    {
        [Min(0)][Tooltip("Количество очков за каждый пройденный уровень платформ")]
        [SerializeField] private int _ScorePerStep = 0;

        public int scorePerStep => _ScorePerStep;
    }
}
