﻿using System;
using _Game.Scripts.Parameters.Interfaces;
using UnityEngine;

namespace _Game.Scripts.Parameters.Classes
{
    [Serializable]
    class GemManagerParameters : IGemManagerParameters
    {
        #region SERIALIZE_FIELDS
        [Tooltip("Количество платформ в блоке, для которого будет рассматриваться установка одного кристалла")]
        [SerializeField] private int _PlatformsInBlock = 0;

        [Tooltip("Тип алгоритма, по которому будет определяться вероятность установки кристалла в блоке." +
            "Random - на одну случайную платорму из блока. " +
            "Iterative - фиксированный порядковый номер платформы в блоке, который увеличивается на один с каждым блоком.")]
        [SerializeField] private PlacementType _PlacementType = PlacementType.Iterative;

        [Tooltip("Длина стороны квадрата, внутри которого будет генерироваться позиция. " +
            "Центр кварата - центр платформы.")]
        [SerializeField] private float _SquareSideOfGenerationArea = 0f;

        [Tooltip("Маска для определения допустимости смещения параметром выше. " +
            "0 - не смещать. !0 - смещать.")]
        [SerializeField] private Vector3 _PositionOffsetMask = Vector3.zero;
        #endregion // SERIALIZE_FIELDS

        #region PUBLIC_VALUES
        public int platformsInBlock => _PlatformsInBlock;
        public PlacementType placementType => _PlacementType;
        public float squareSideOfGenerationArea => _SquareSideOfGenerationArea;
        public Vector3 positionOffsetMask => _PositionOffsetMask;
        #endregion // PUBLIC_VALUES
    }

    public enum PlacementType
    { Random, Iterative }
}
