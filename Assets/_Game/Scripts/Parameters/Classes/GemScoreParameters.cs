﻿using System;
using _Game.Scripts.Parameters.Interfaces;
using UnityEngine;

namespace _Game.Scripts.Parameters.Classes
{
    [Serializable]
    class GemScoreParameters : IGemScoreParameters
    {
        [Min(0)] [Tooltip("Количество очков за каждый пойманный кристалл")]
        [SerializeField] private int _ScorePerGem = 0;

        public int scorePerGem => _ScorePerGem;
    }
}
