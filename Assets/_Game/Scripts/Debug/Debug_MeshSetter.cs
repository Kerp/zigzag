﻿using UnityEngine;

namespace _Game.Scripts.Debug
{
    public class Debug_MeshSetter : MonoBehaviour
    {
        [SerializeField] private Material _Material = null;

        private void OnValidate()
        {
            foreach (var renderer in FindObjectsOfType<Renderer>())
                renderer.material = _Material;
        }
    }
}
