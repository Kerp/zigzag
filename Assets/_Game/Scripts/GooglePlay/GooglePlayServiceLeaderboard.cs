﻿using System;
using _Game.Scripts.GooglePlay.Parameters;
using Core.Extensions;
using GooglePlayGames.BasicApi;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.GooglePlay
{
    [RequireComponent(typeof(GooglePlayServiceManager))]
    public class GooglePlayServiceLeaderboard : MyMonoBehaviour
    {
        [Inject] private GooglePlayServiceManager _manager { get; }
        [Inject] private GooglePlayServiceSettings _settings { get; }
        
        //////////////////////////////////////////////////////////////
        
        [ContextMenu("Show Leaderboard")]
        public async void ShowLeaderboard()
        {
            if (_manager.isAuthenticated == false)
                await _manager.Login();
            
            if (_manager.isAuthenticated == false)
            {
                Error("Isn't authenticated to show Leaderboard!");
                return;
            }

            Log("Show Leaderboard UI");
            _manager.playGamesPlatform.ShowLeaderboardUI(_settings.leaderboardId);
        }
        
        //////////////////////////////////////////////////////////////

        public void ReportScore(int score)
        {
            if (_manager.isAuthenticated == false)
                return;

            _manager.playGamesPlatform.ReportScore(score, _settings.leaderboardId, result => 
                Log($"Report score {score}: {result}"));
        }

        public void GetRank(Action<int> onSuccess)
        {
            if (_manager.isAuthenticated == false)
                return;
            
            _manager.playGamesPlatform.LoadScores(
                _settings.leaderboardId,
                LeaderboardStart.PlayerCentered,
                1,
                LeaderboardCollection.Public,
                LeaderboardTimeSpan.AllTime,
                data =>
                {
                    if (data == null || !data.Valid)
                    {
                        Error("Can't receive rank!");
                        return;
                    }
                    
                    Log("Score received: " +
                        $"Valid = {data.Valid}; " +
                        $"Id = {data.Id}; " +
                        $"userId = {data.PlayerScore.userID}; " +
                        $"formatted = {data.PlayerScore.formattedValue}; " +
                        $"rank = {data.PlayerScore.rank}");
                    
                    onSuccess?.Invoke(data.PlayerScore.rank);
                });
        }
    }
}