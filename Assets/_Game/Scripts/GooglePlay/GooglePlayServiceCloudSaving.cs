﻿using System;
using System.Globalization;
using _Game.Scripts.Game.Data;
using _Game.Scripts.Game.Data.Aggregator;
using _Game.Scripts.Game.Toast;
using _Game.Scripts.GooglePlay.Parameters;
using Core.Extensions;
using GooglePlayGames.BasicApi.SavedGame;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.GooglePlay
{
    [RequireComponent(typeof(GooglePlayServiceManager))]
    public class GooglePlayServiceCloudSaving : MyMonoBehaviour
    {
        [Inject] private ToastsManager _ToastsManager { get; }
        [Inject] private DataAggregator _DataAggregator { get; }
        [Inject] private GooglePlayServiceManager _Manager { get; }
        [Inject] private GooglePlayServiceSettings _Settings { get; }

        //////////////////////////////////////////////////////////////

        #region UI

        [ContextMenu("Show saves")]
        public async void ShowSelectUI()
        {
            if (_Manager.isAuthenticated == false)
                await _Manager.Login();

            if (_Manager.isAuthenticated == false)
            {
                Error("Isn't authenticated to show SaveData!");
                return;
            }

            _Manager.playGamesPlatform.SavedGame.ShowSelectSavedGameUI(
                "Select saved game", 5, false, true, (s, r) => LogRequestStatus("Open Save UI", s));
        }

        #endregion // UI

        #region BASE

        private void OpenSaveData(Action<SavedGameRequestStatus, ISavedGameMetadata> callback)
        {
            if (_Manager.isAuthenticated == false)
            {
                Error("User isn't authenticated!");
                return;
            }

            _Manager.playGamesPlatform.SavedGame.OpenWithAutomaticConflictResolution(
                _Settings.saveName, _Settings.dataSource, _Settings.conflictResolutionStrategy,
                (s, m) =>
                {
                    LogRequestStatus("OpenSaveData", s);
                    callback?.Invoke(s, m);
                });
        }

        #endregion // BASE


        #region SAVING

        public void SaveGame() =>
            OpenSaveData(OnSaveGameResponse);

        private void OnSaveGameResponse(SavedGameRequestStatus status, ISavedGameMetadata metadata)
        {
            var updatedMetadata = new SavedGameMetadataUpdate.Builder()
                .WithUpdatedDescription(DateTime.Now.ToString(CultureInfo.InvariantCulture)).Build();

            var saveData = _DataAggregator.ToBinary();
            _Manager.playGamesPlatform.SavedGame.CommitUpdate(metadata, updatedMetadata, saveData,
                (s, r) => LogRequestStatus("Saving", s));
        }

        #endregion // SAVING

        #region LOADING

        public void LoadGame() =>
            OpenSaveData(OnLoadGameResponse);

        private void OnLoadGameResponse(SavedGameRequestStatus status, ISavedGameMetadata metadata)
        {
            _Manager.playGamesPlatform.SavedGame.ReadBinaryData(metadata, (s, r) =>
            {
                LogRequestStatus("Loading", s);
                _DataAggregator.Apply(r, SaveApplyType.UseMaxValue);
            });
        }

        #endregion // LOADING

        #region DELETING

        public void DeleteGameData() =>
            OpenSaveData(OnDeleteGameDataResponse);

        private void OnDeleteGameDataResponse(SavedGameRequestStatus status, ISavedGameMetadata metadata)
        {
            LogRequestStatus("Deleting", status);
            _Manager.playGamesPlatform.SavedGame.Delete(metadata);
        }

        #endregion // DELETING


        #region LOGGING

        private void LogRequestStatus(string message, SavedGameRequestStatus status)
        {
            message = $"{message}: {status:F}";

            switch (status)
            {
                case SavedGameRequestStatus.Success:
                    Log(message);
                    break;

                case SavedGameRequestStatus.AuthenticationError:
                case SavedGameRequestStatus.InternalError:
                case SavedGameRequestStatus.TimeoutError:
                case SavedGameRequestStatus.BadInputError:
                    ShowErrorSyncToast();
                    Error(message);
                    break;
            }
        }

        private void LogRequestStatus(string message, SelectUIStatus status)
        {
            message = $"{message}: {status:F}";

            switch (status)
            {
                case SelectUIStatus.SavedGameSelected:
                case SelectUIStatus.UserClosedUI:
                    Log(message);
                    break;

                case SelectUIStatus.AuthenticationError:
                case SelectUIStatus.BadInputError:
                case SelectUIStatus.InternalError:
                case SelectUIStatus.TimeoutError:
                case SelectUIStatus.UiBusy:
                    Error(message);
                    break;
            }
        }

        private void ShowErrorSyncToast() => _ToastsManager.ShowToast(ToastType.CantSyncCloudData);

        #endregion // LOGGING
    }
}