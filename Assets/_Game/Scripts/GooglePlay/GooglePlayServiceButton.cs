﻿using _Game.Scripts.Game.Localization.Components;
using Core.Extensions;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Game.Scripts.GooglePlay
{
    class GooglePlayServiceButton : MyMonoBehaviour
    {
        [Inject] private GooglePlayServiceManager _GPService { get; }

        [Header("Components")]
        [SerializeField] private Button _Button = null;
        [SerializeField] private LocalizedUniversalText universalText = null;

        [Header("Settings")]
        [SerializeField] private string _SignInKey = "";
        [SerializeField] private string _SignOutKey = "";

        /////////////////////////////////////////////////////////

        protected override void OnChecking()
        {
            CheckField(universalText);
            CheckField(_Button);
            CheckField(_GPService);
        }

        private void OnEnable()
        {
            if (_GPService != null)
                _GPService.onAuthentificateStatusChanged += UpdateState;

            UpdateState();
            _Button.onClick.AddListener(OnClickAction);
        }

        private void OnDisable()
        {
            if (_GPService != null)
                _GPService.onAuthentificateStatusChanged -= UpdateState;

            _Button.onClick.RemoveListener(OnClickAction);
        }

        /////////////////////////////////////////////////////////

        private async void OnClickAction()
        {
            Log("Clicked");

            if (_GPService == null)
                return;

            await _GPService.LoginOrLogout();
            UpdateState();
        }

        private void UpdateState()
        {
            if (_GPService == null || universalText == null)
                return;

            universalText.SetKey(_GPService.isAuthenticated
                ? _SignOutKey : _SignInKey);
        }
    }
}
