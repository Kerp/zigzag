﻿using Core.Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace _Game.Scripts.UI
{
    class GridLayoutSizerComponent : MyMonoBehaviour
    {
        private GridLayoutGroup _gridLayoutGroup;
        private RectTransform _rectTransform;

        ///////////////////////////////////////////////////////

        protected override void OnAttaching()
        {
            Attach(ref _rectTransform);
            Attach(ref _gridLayoutGroup);
        }

        private void OnEnable()
        {
            UpdateSize();
        }

        ///////////////////////////////////////////////////////

        [ContextMenu("Update Size")]
        public void UpdateSize()
        {
            OnAttaching();

            if (_gridLayoutGroup == null || _rectTransform == null) 
                return;

            switch(_gridLayoutGroup.constraint) {
                case GridLayoutGroup.Constraint.FixedColumnCount: UpdateSizeForFixedColumns(); break;
                case GridLayoutGroup.Constraint.FixedRowCount: UpdateSizeForFixedRows(); break;
                case GridLayoutGroup.Constraint.Flexible: UpdateSizeForFlexible(); break;

            }
        }

        ///////////////////////////////////////////////////////

        private void UpdateSizeForFixedColumns()
        {
            var childInDirections = Vector2.zero;
            var childCount = transform.childCount;

            childInDirections.x = Mathf.Min(childCount, _gridLayoutGroup.constraintCount);
            childInDirections.y = Mathf.CeilToInt(childCount / childInDirections.x);

            _rectTransform.sizeDelta = Vector2.Scale(_gridLayoutGroup.cellSize, childInDirections);
        }

        private void UpdateSizeForFixedRows()
        {
            var childInDirections = Vector2.zero;
            var childCount = transform.childCount;

            childInDirections.y = Mathf.Min(childCount, _gridLayoutGroup.constraintCount);
            childInDirections.x = Mathf.CeilToInt(childCount / childInDirections.y);

            _rectTransform.sizeDelta = Vector2.Scale(_gridLayoutGroup.cellSize, childInDirections);
        }

        private void UpdateSizeForFlexible()
        {

        }
    }
}
