﻿using _Game.Scripts.Extensions;
using UnityEngine;
using UnityEngine.Events;

namespace _Game.Scripts.UI.Elements
{
    class SelectableElement : MonoBehaviour
    {
        [SerializeField] private bool _Selected;

        [SerializeField] private UnityEvent _OnSelected = new UnityEvent();
        [SerializeField] private UnityEvent _OnDeselested = new UnityEvent();
        [SerializeField] private BoolUnityEvent _OnSelectionChanged = new BoolUnityEvent();

        private bool _selected_Cache { get; set; }

        public bool selected => _Selected;

        public UnityEvent onSelected {
            get => _OnSelected;
            set => _OnSelected = value;
        }
        public UnityEvent OnDeselested { 
            get => _OnDeselested; 
            set => _OnDeselested = value; 
        }
        public BoolUnityEvent OnSelectionChanged { 
            get => _OnSelectionChanged; 
            set => _OnSelectionChanged = value; 
        }

        ////////////////////////////////////////////////////////

        private void Start()
        {
            _selected_Cache = !_Selected;
            UpdateSelectFlag();
        }

        private void Update()
            => UpdateSelectFlag();

        private void OnValidate()
            => UpdateSelectFlag();

        ////////////////////////////////////////////////////////

        public void Select()
            => SetSelected(true);

        public void Deselect()
            => SetSelected(false);

        public void SetSelected(bool status)
        {
            if (_selected_Cache == status)
                return;

            _selected_Cache = status;
            _Selected = status;

            if (_Selected) _OnSelected?.Invoke();
            else _OnDeselested?.Invoke();

            _OnSelectionChanged?.Invoke(_Selected);
        }

        ////////////////////////////////////////////////////////

        private void UpdateSelectFlag()
        {
            if (_Selected != _selected_Cache)
                SetSelected(_Selected);
        }
    }
}
