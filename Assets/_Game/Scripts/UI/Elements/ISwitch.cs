﻿using _Game.Scripts.Extensions;

namespace _Game.Scripts.UI.Elements
{
    public interface ISwitch
    {
        BoolUnityEvent onSwitch { get; set; }
        bool isOn { get; set; }
        bool isOff { get; set; }

        void SetEnabled(bool status);
        void SwitchData();
    }
}