﻿using _Game.Scripts.Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace _Game.Scripts.UI.Elements
{
    [RequireComponent(typeof(Button))]
    public abstract class Switch : MonoBehaviour, ISwitch
    {
        #region SERIALIZE_FIELDS

        [SerializeField] private bool _SwitchOn = false;
        [SerializeField] private BoolUnityEvent _OnSwitch = new BoolUnityEvent();
        #endregion // SERIALIZE_FIELDS
        
        #region PUBLIC_VALUES
        public bool isOn {
            get => _SwitchOn;
            set => SetEnabled(value);
        }

        public bool isOff {
            get => !_SwitchOn;
            set => SetEnabled(!value);
        }

        public BoolUnityEvent onSwitch {
            get => _OnSwitch;
            set => _OnSwitch = value;
        }
        #endregion // PUBLIC_VALUES

        /////////////////////////////////////////////////////

        private void OnValidate()
        {
            UpdateSwitch();
        }

        public void SwitchData()
            => SetEnabled(!_SwitchOn);

        public void SetEnabled(bool status)
        {
            if (status == _SwitchOn)
                return;

            _SwitchOn = status;
            UpdateSwitch();
            onSwitch?.Invoke(status);
        }

        protected abstract void UpdateSwitch();
    }
}