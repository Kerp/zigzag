﻿using UnityEngine;
using UnityEngine.UI;

namespace _Game.Scripts.UI.Elements
{
    [RequireComponent(typeof(Button))]
    public class TextSwitch : Switch
    {
        [SerializeField] private Text _TargetChangedText = null;
        [SerializeField] private string _OnText = "";
        [SerializeField] private string _OffText = "";
        
        protected override void UpdateSwitch()
        {
            if (_TargetChangedText == null)
                return;

            _TargetChangedText.text = isOn ? _OnText : _OffText;
        }
    }
}