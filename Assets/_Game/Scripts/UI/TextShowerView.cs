﻿using System.Collections.Generic;
using _Game.Scripts.AssetsLoading;
using _Game.Scripts.Game.Analytics;
using _Game.Scripts.Game.Localization.Components;
using _Game.Scripts.Game.Statistics;
using Core.Components;
using Core.Extensions;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Game.Scripts.UI
{
    class TextShowerView : MyMonoBehaviour
    {
        [Inject] private IAnalyticsManager _analytics = null;
        [Inject] private StatisticsData _statistics = null;

        [Header("Components")]
        [SerializeField] private ScrollRect _ScrollRect = null;
        [SerializeField] private LocalizedUniversalText _Header = null;
        [SerializeField] private UniversalText _Text = null;

        [Header("Settings")]
        [SerializeField] private bool _AutoClearOnDisable = false;

        private TextAsset _lastTextAsset;
        private AssetLoaderByName<TextAsset> _textLoader;
        private List<UniversalText> _additionalTexts = new List<UniversalText>();

        private const int _MAX_TEXT_LENGTH = 15000;

        /////////////////////////////////////////////////

        private void OnDisable()
        {
            if (_AutoClearOnDisable == false)
                return;

            Resources.UnloadAsset(_lastTextAsset);
            SetDefaultScroll();
            SetHeader("");
            SetText("");

            ClearAdditionalTexts();
        }

        private void ClearAdditionalTexts()
        {
            foreach (var text in _additionalTexts)
                Destroy(text.gameObject);

            _additionalTexts.Clear();
        }

        protected override void OnChecking()
        {
            CheckField(_ScrollRect);
            CheckField(_Header);
            CheckField(_Text);
        }

        /////////////////////////////////////////////////

        public void SetHeader(string header)
        {
            if (header == null)
                return;

            _Header?.SetKey(header);
            _statistics.ReadDocument(header);

            if (string.IsNullOrEmpty(header) == false)
                _analytics.ReadGDPR(AnalyticsHelper.GetGDPREventData(header));
        }

        public void SetText(string text)
        {
            if (_Text == null || text == null)
                return;

            _Text.SetText(PopAcceptablePart(ref text));

            while (text.Length > 0)
            {
                var newText = CreateNewText();
                newText.SetText(PopAcceptablePart(ref text));

                _additionalTexts.Add(newText);
            }
        }

        public void SetTextFromAsset(TextAsset textAsset) =>
            SetText(textAsset?.text);

        public async void SetTextByAssetName(string assetName)
        {
            _textLoader ??= new AssetLoaderByName<TextAsset>();
            var textAsset = await _textLoader.Load(assetName);
            SetTextFromAsset(textAsset);
        }

        /////////////////////////////////////////////////

        private UniversalText CreateNewText()
        {
            var prefab = _Text.gameObject;

            return Instantiate(prefab, prefab.transform.parent)
                .GetComponent<UniversalText>();
        }

        private string PopAcceptablePart(ref string inputData)
        {
            var length = Mathf.Min(inputData.Length, _MAX_TEXT_LENGTH);

            var output = inputData.Substring(0, length);
            inputData = inputData.Remove(0, length);

            return output;
        }

        private void SetDefaultScroll()
        {
            if (_ScrollRect == null)
                return;

            _ScrollRect.content.anchoredPosition = new Vector2(_ScrollRect.content.anchoredPosition.x, 0f);
        }
    }
}
