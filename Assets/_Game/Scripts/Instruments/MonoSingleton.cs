﻿using UnityEngine;

namespace _Game.Scripts.Instruments
{
    abstract class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;
        private static bool applicationIsQuitting = false;

        public static T instance {
            get {
                Instantiate();
                return _instance;
            }
        }

        public static void Instantiate()
        {
            if (applicationIsQuitting)
                return;

            if (_instance == null)
                _instance = (T)FindObjectOfType(typeof(T));

            if (_instance == null)
            {
                var res = Resources.LoadAll<T>("");
                var prefab = res.Length > 0 ? res[0] : null;
                _instance = prefab ? Instantiate(prefab) : null;
            }

            if (_instance == null) {
                var go = new GameObject(typeof(T).Name);
                _instance = go.AddComponent<T>();
            }
        }

        private void OnDestroy()
        {
            applicationIsQuitting = true;
        }
    }
}
