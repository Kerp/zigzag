﻿using UnityEngine;
using UnityEngine.Events;

namespace _Game.Scripts.Instruments.Bordering
{
    class BorderChecker : MonoBehaviour, IBorderChecker
    {
        #region SERIALIZE_FIELDS

        [SerializeField] private Transform _CheckedObject = null;

        [Tooltip("Ограничение по координатам")]
        [SerializeField] private Vector3 _BordersByCoordinates = Vector3.zero;

        [Tooltip("Как сравнивать координаты. Выход за границы: " +
            "< 0 - если граница оказалась меньше; " +
            "== 0 - если граница равна; " +
            "> 0 - если граница оказалась больше.")]
        [SerializeField] private Vector3Int _DirectionByCoordinates = Vector3Int.zero;

        [Tooltip("1 - Учитывать ограничение по координате, 0 - нет")]
        [SerializeField] private Vector3Int _MaskByCoordinates = Vector3Int.one;

        [SerializeField] private UnityEvent _OnBorderIn = new UnityEvent();
        [SerializeField] private UnityEvent _OnBorderOut = new UnityEvent();

        #endregion // SERIALIZE_FIELDS

        #region PRIVATE_VALUES

        private readonly bool[] _trespassStatus = new bool[3];

        #endregion // PRIVATE_VALUES

        #region PUBLIC_VALUES

        public Transform checkedObject => _CheckedObject;
        public Vector3 bordersByCoordinates => _BordersByCoordinates;
        public Vector3Int maskByCoordinates => _MaskByCoordinates;
        public Vector3Int directionByCoordinates => _DirectionByCoordinates;

        public event UnityAction onBorderIn
        {
            add => _OnBorderIn.AddListener(value);
            remove => _OnBorderIn.RemoveListener(value);
        }

        public event UnityAction onBorderOut
        {
            add => _OnBorderOut.AddListener(value);
            remove => _OnBorderOut.RemoveListener(value);
        }

        #endregion // PUBLIC_VALUES

        //////////////////////////////////////////

        #region MONO_BEHAVIOUR

        private void Start()
        {
            CorrectMask(ref _MaskByCoordinates);
            CorrectDirection(ref _DirectionByCoordinates);
            CheckBorders(false);
        }

        private void FixedUpdate() =>
            CheckBorders();

        #endregion // MONO_BEHAVIOUR

        /////////////////////////////////////////

        #region PUBLIC_METHODS

        public void SetCheckedObject(Transform checkedObject) => _CheckedObject = checkedObject;
        public void SetBorders(Vector3 bordersByCoordinates) => _BordersByCoordinates = bordersByCoordinates;
        public void SetDirection(Vector3Int directionByCoordinates) => _DirectionByCoordinates = directionByCoordinates;
        public void SetMask(Vector3Int maskByCoordinates) => _MaskByCoordinates = maskByCoordinates; 

        #endregion // PUBLIC_METHODS

        #region PRIVATE_METHODS

        private void CorrectMask(ref Vector3Int mask)
        {
            for (var i = 0; i < 3; i++)
                mask[i] = mask[i] == 0 ? 0 : 1;
        }

        private void CorrectDirection(ref Vector3Int direction)
        {
            for (var i = 0; i < 3; i++)
                direction[i] = direction[i] < 0 ? -1
                    : direction[i] == 0 ? 0 : 1;
        }

        private void CheckBorders(bool callEvents = true)
        {
            if (_CheckedObject == false)
                return;

            var position = _CheckedObject.localPosition;

            for (var i = 0; i < 3; i++)
            {
                if (_MaskByCoordinates[i] == 0)
                    continue;

                var trespass = IsTrespass(position[i], bordersByCoordinates[i], directionByCoordinates[i]);
                if (callEvents) IsStatusChanged(_trespassStatus[i], trespass);
                _trespassStatus[i] = trespass;
            }
        }

        private bool IsTrespass(float position, float border, int direction)
        {
            switch(direction)
            {
                case -1: return border < position;
                case 0: return border == position;
                case 1: return border > position;
                default: return false;
            }
        }

        private bool IsStatusChanged(bool oldStatus, bool newStatus)
        {
            if (oldStatus == newStatus)
                return false;

            if (newStatus)
                _OnBorderOut?.Invoke();
            else
                _OnBorderIn?.Invoke();

            return true;
        }

        #endregion // PRIVATE_METHODS
    }
}
