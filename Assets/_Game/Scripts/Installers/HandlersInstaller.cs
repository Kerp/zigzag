using _Game.Scripts.Game.Handlers.Direction;
using _Game.Scripts.Game.Handlers.Speed;
using _Game.Scripts.Parameters.Interfaces;
using Zenject;

namespace _Game.Scripts.Installers
{
    public class HandlersInstaller : MonoInstaller<HandlersInstaller>
    {
        [Inject] private IGameParameters _GameParameters { get; }

        public override void InstallBindings()
        {
            Container.Bind<ISpeedHandler>().To<ISpeedHandler>().FromInstance(_speedHandlerInstance).AsTransient();
            Container.Bind<IDirectionHandler>().To<IDirectionHandler>().FromInstance(_directionHandlerInstance).AsTransient();
        }

        private ISpeedHandler _speedHandlerInstance
            => new SpeedHandler(
                    _GameParameters.Ball.startSpeed,
                    _GameParameters.Ball.acceleration,
                    _GameParameters.Ball.minSpeed,
                    _GameParameters.Ball.maxSpeed);

        private IDirectionHandler _directionHandlerInstance
            => new DirectionHandler(_GameParameters.Direction.directions);
    }
}