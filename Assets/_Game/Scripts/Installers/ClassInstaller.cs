﻿using _Game.Scripts.Game.Ads.Save;
using _Game.Scripts.Game.Data;
using _Game.Scripts.Game.Data.Aggregator;
using _Game.Scripts.Game.Data.Purchasing;
using _Game.Scripts.Game.Data.Save;
using _Game.Scripts.Game.IAP.Management;
using _Game.Scripts.Game.IAP.Management.Saving;
using _Game.Scripts.Game.Localization.Save;
using _Game.Scripts.Game.Scoring;
using _Game.Scripts.Game.SkinsShop;
using _Game.Scripts.Game.SkinsShop.Data;
using _Game.Scripts.Game.Statistics;
using _Game.Scripts.Game.Vibration;
using _Game.Scripts.Instruments.Counter;
using _Game.Scripts.Parameters.Interfaces;
using Core.Modules;
using Zenject;

namespace _Game.Scripts.Installers
{
    class ClassInstaller : MonoInstaller
    {
        [Inject] private IGemScoreParameters _gemScoreParameters { get; }
        [Inject] private ITurnsScoreParameters _turnsScoreParameters { get; }
        [Inject] private IDistanceScoreParameters _distanceScoreParameters { get; }

        //////////////////////////////////////////////////////

        public override void InstallBindings()
        {
            var distanceScore = new ScoreInner(new IntCounter(0, _distanceScoreParameters.scorePerStep));
            var turnsScore = new ScoreInner(new IntCounter(0, _turnsScoreParameters.scorePerChangingDirection));
            var gemsScore = new ScoreInner(new IntCounter(0, _gemScoreParameters.scorePerGem));

            var score = new Score();
            score.AddCounter(ScoreType.Distance, distanceScore);
            score.AddCounter(ScoreType.Turns, turnsScore);
            score.AddCounter(ScoreType.Gems, gemsScore);

            Container.Bind<IScore>().To<Score>().FromInstance(score).AsSingle();

            Container.Bind<IScoreInner>().WithId(ScoreType.Gems).To<ScoreInner>()
                .FromInstance(new ScoreInner(new IntCounter(0, 1))).AsSingle();

            Container.Bind<SaveManager>().AsSingle();
            Container.Bind<ITimeScaleManager>().To<TimeScaleManager>().AsSingle();

            Container.Bind<NoAdsPurchasesManager>().To<NoAdsPurchasesManager>().AsSingle();
            Container.Bind<DiamondsPurchasesManager>().To<DiamondsPurchasesManager>().AsSingle();

            Container.Bind<IBuyingManager>().To<BuyingManager>().AsSingle();
            Container.Bind<ISkinsManager>().To<SkinsManager>().AsSingle();

            Container.Bind<IVibrator>().To<Vibrator>().AsSingle();
            Container.Bind<VibrationManager>().AsSingle();

            InstallDataClasses();
        }

        private void InstallDataClasses()
        {
            Container.Bind<GameData>().AsSingle();
            Container.Bind<SettingsData>().AsSingle();
            Container.Bind<LocalizationData>().AsSingle();

            Container.Bind<StatisticsData>().AsSingle();
            Container.Bind<PurchasingData>().AsSingle();

            Container.Bind<SkinsPurchasesData>().AsSingle();
            Container.Bind<SkinsSelectedData>().AsSingle();

            Container.Bind<RewardedCountingData>().AsSingle();

            Container.Bind<DataAggregator>().AsSingle();
        }
    }
}
