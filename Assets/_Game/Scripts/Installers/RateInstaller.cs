﻿using _Game.Scripts.Game;
using _Game.Scripts.Game.Rating;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Installers
{
    [CreateAssetMenu(fileName = nameof(RateInstaller), menuName = DefinitionsHelper.Installers + nameof(RateInstaller))]
    class RateInstaller : ScriptableObjectInstaller<RateInstaller>
    {
        [SerializeField] private RateSettings _RateSettings = null;


        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<RateSettings>().FromInstance(_RateSettings).AsSingle();
        }
    }
}
