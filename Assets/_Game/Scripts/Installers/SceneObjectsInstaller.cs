﻿using _Game.Scripts.Game.Analytics;
using _Game.Scripts.Game.IAP;
using _Game.Scripts.Game.Input;
using _Game.Scripts.Game.Localization;
using _Game.Scripts.Game.Objects.Ball;
using _Game.Scripts.Game.Objects.Generating.Gem;
using _Game.Scripts.Game.Objects.Generating.Platform;
using _Game.Scripts.Game.Toast;
using _Game.Scripts.GooglePlay;
using _Game.Scripts.SoundManagement;
using Cinemachine;
using Core.Components.UI;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Installers
{
    class SceneObjectsInstaller : MonoInstaller
    {
        [SerializeField] private GameObject _Ball = null;
        [SerializeField] private GameObject _VirtualCamera = null;
        [SerializeField] private GameObject _PlatformManager = null;
        [SerializeField] private GameObject _GemManager = null;
        [SerializeField] private GameObject _SoundManager = null;
        [SerializeField] private GameObject _IapManager = null;
        [SerializeField] private GameObject _Analytics = null;
        [SerializeField] private GameObject _GooglePlayServiceManager = null;
        [SerializeField] private GameObject _UIManager = null;
        [SerializeField] private GameObject _InputManager = null;
        [SerializeField] private GameObject _LocalizationManager = null;
        [SerializeField] private GameObject _ToastsManager = null;

        public override void InstallBindings()
        {
            Container.Bind<InputManager>().FromComponentOn(_InputManager).AsSingle();
            Container.Bind<ControlManager>().FromComponentOn(_InputManager).AsSingle();

            var gemManager = _GemManager.GetComponent<GemManager>();
            Container.Bind<GemManager>().To(gemManager.GetType()).FromInstance(gemManager).AsSingle();

            var platformManager = _PlatformManager.GetComponent<PlatformManager>();
            Container.Bind<PlatformManager>().To(platformManager.GetType()).FromInstance(platformManager).AsSingle();

            Container.Bind<IBall>().To<BallObject>().FromComponentOn(_Ball).AsSingle();
            Container.Bind<IUIManager>().To<UIManager>().FromComponentOn(_UIManager).AsSingle();
            Container.Bind<ISoundManager>().To<SoundManager>().FromComponentOn(_SoundManager).AsSingle();
            Container.Bind<CinemachineVirtualCamera>().FromComponentOn(_VirtualCamera).AsSingle();

            Container.Bind<ToastsManager>().FromComponentOn(_ToastsManager).AsSingle();
            Container.Bind<LocalizationManager>().FromComponentOn(_LocalizationManager).AsSingle();
            Container.Bind<I_IAPManager>().To<IAPManager>().FromComponentOn(_IapManager).AsSingle();
            Container.Bind<IAnalyticsManager>().To<AnalyticsManager>().FromComponentOn(_Analytics).AsSingle();
            Container.Bind<GooglePlayServiceManager>().FromComponentOn(_GooglePlayServiceManager).AsSingle();
        }
    }
}
