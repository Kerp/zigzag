using _Game.Scripts.Game.Handlers.Direction;
using _Game.Scripts.Game.Handlers.Probability;
using _Game.Scripts.Game.Objects.Generating.Platform.Calculator;
using _Game.Scripts.Game.Objects.Generating.Platform.Changer;
using _Game.Scripts.Game.Objects.Generating.Platform.Factory;
using _Game.Scripts.Game.Objects.Generating.Platform.Pool;
using _Game.Scripts.Game.Objects.Generating.Platform.Producer;
using _Game.Scripts.Game.Objects.Platform;
using _Game.Scripts.Parameters.Interfaces;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Installers
{
    public class PlatformManagerInstaller : MonoInstaller<PlatformManagerInstaller>
    {
        [SerializeField] private Transform _PoolContainer = null;

        [Inject] private IGamePrefabs _gamePrefabs { get; }
        [Inject] private IGameParameters _gameParameters { get; }

        private const bool _USE_POOL = true;

        public override void InstallBindings()
        {
            Container.Bind<IProbabilitierByCurve>().FromInstance(_probabilitier).AsTransient();
            Container.Bind<IDirectionHandler>().FromInstance(_directionHandlerInstance).AsTransient();

            Container.Bind<IDirectionChanger>().To<DirectionChangerByProbability>().AsTransient();

            Container.Bind<PositionCalculator>().AsTransient().WithArguments(_gameParameters.Platform.size);

            BindProducer(_USE_POOL);
        }

        private IProbabilitierByCurve _probabilitier
            => new ProbabilitierByCurve(_gameParameters.PlatformManager.probabilityChangeDirection);

        private IDirectionHandler _directionHandlerInstance
            => new DirectionHandler(_gameParameters.Direction.directions);

        private void BindProducer(bool usePool)
        {
            var prefab = _gamePrefabs.platform;

            if (usePool)
                BindProducerByPool(prefab);
            else
                BindProducerByFactory(prefab);
        }

        private void BindProducerByFactory(GameObject prefab)
        {
            Container.BindFactory<IPlatform, PlatformFactory>().FromComponentInNewPrefab(prefab).AsSingle();
            Container.Bind<IPlatformFactory>().To<PlatformFactory>().FromResolve().AsTransient();

            Container.Bind<IPlatformProducer>().To<PlatformProducer>().AsTransient();
        }

        private void BindProducerByPool(GameObject prefab)
        {
            var parameters = _gameParameters.PlatformManager;

            var poolSize = (int) ( Mathf.Pow(parameters.initialAreaSize, 2) +
                parameters.initialStepsOneDirection + parameters.stepThresholdForward);
             
            Container.Bind<Transform>().FromInstance(_PoolContainer).WhenInjectedInto<PlatformPool>();
            Container.BindMemoryPool<PlatformObject, PlatformPool>().WithInitialSize(poolSize)
                .FromComponentInNewPrefab(prefab).UnderTransform(_PoolContainer);

            Container.Bind<IPlatformPool>().To<PlatformPool>().FromResolve();
            Container.Bind<IPlatformProducer>().To<PlatformProducerByPool>().AsTransient();
        }
    }
}