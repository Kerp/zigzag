﻿using _Game.Scripts.Game;
using UnityEngine;

namespace _Game.Scripts.SoundManagement
{
    [CreateAssetMenu(fileName = nameof(SoundManagerSettings), menuName = DefinitionsHelper.Sound + nameof(SoundManagerSettings))]
    class SoundManagerSettings : ScriptableObject
    {
        [SerializeField] private int _MaxSameSoundsAtOneTime = 0;
        [SerializeField] private int _MaxDifferentSoundsAtOneTime = 0;
        [SerializeField] private float _MinOffsetBetweenSameSoudns = 0;

        public int maxSameSoundsAtOneTime => _MaxSameSoundsAtOneTime;
        public float minOffsetBetweenSameSounds => _MinOffsetBetweenSameSoudns;
        public int maxDifferenetSoundsAtOneTime => _MaxDifferentSoundsAtOneTime;
    }
}
