﻿using UnityEngine;

namespace _Game.Scripts.SoundManagement.Library
{
    interface IFXLibrary
    {
        AudioClip gameStart { get; }
        AudioClip gameOver { get; }
        AudioClip changeDirection { get; }
        AudioClip collectGem { get; }
        AudioClip platformCrack { get; }
    }
}
