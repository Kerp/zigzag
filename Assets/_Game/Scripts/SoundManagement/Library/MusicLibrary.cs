﻿using _Game.Scripts.Game;
using UnityEngine;

namespace _Game.Scripts.SoundManagement.Library
{
    [CreateAssetMenu(fileName = nameof(MusicLibrary), menuName = DefinitionsHelper.Sound + nameof(MusicLibrary))]
    class MusicLibrary : ScriptableObject, IMusicLibrary
    {
        [SerializeField] private AudioClip[] _Music = new AudioClip[0];

        public AudioClip[] musicClips => _Music;

        public AudioClip randomMusic =>
            _Music != null && _Music.Length > 0 ?
            _Music[Random.Range(0, _Music.Length)] : null;
    }
}
