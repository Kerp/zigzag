﻿using _Game.Scripts.Game;
using UnityEngine;

namespace _Game.Scripts.SoundManagement.Library
{
    [CreateAssetMenu(fileName = nameof(UILibrary), menuName = DefinitionsHelper.Sound + nameof(UILibrary))]
    class UILibrary : ScriptableObject, IUILibrary
    {
        [SerializeField] private AudioClip _ButtonClick = null;
        [SerializeField] private AudioClip _SwitchSwitched = null;
        [SerializeField] private AudioClip _ViewOpened = null;
        [SerializeField] private AudioClip _ViewClosed = null;

        public AudioClip buttonClick => _ButtonClick;
        public AudioClip switchSwitched => _SwitchSwitched;
        public AudioClip viewOpened => _ViewOpened;
        public AudioClip viewClosed => _ViewClosed;
    }
}
