﻿using UnityEngine;

namespace _Game.Scripts.SoundManagement.Library
{
    interface IUILibrary
    {
        AudioClip buttonClick { get; }
        AudioClip switchSwitched { get; }
        AudioClip viewOpened { get; }
        AudioClip viewClosed { get; }
    }
}
