﻿using _Game.Scripts.SoundManagement.Library;
using _Game.Scripts.SoundManagement.Sourcing;
using UnityEngine;

namespace _Game.Scripts.SoundManagement
{
    interface ISoundManager
    {
        ISoundLibrary library { get; }
        ISources sources { get; }

        void Play(AudioSource source, AudioClip clip);
        void PlayLooped(AudioSource source, AudioClip clip);
        void Stop(AudioSource source);
        void SetEnable(AudioSource source, bool status);
    }
}
