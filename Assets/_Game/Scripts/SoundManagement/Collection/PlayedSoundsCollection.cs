﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace _Game.Scripts.SoundManagement.Collection
{
    class PlayedSoundsCollection : IPlayedSoundsCollection
    {
        private readonly Dictionary<AudioClip, int> _playedClips;
        private readonly HashSet<AudioClip> _blockedClips;

        private readonly float _minOffsetBetweenSameSounds;
        private readonly int _maxDifferentSoundsAtOneTime;
        private readonly int _maxSameSoundsAtOneTime;
        private readonly MonoBehaviour _host;

        /////////////////////////////////////////////////////////

        public PlayedSoundsCollection(MonoBehaviour host, float minOffsetBetweenSameSounds, 
            int maxDifferentSoundsAtOneTime, int maxSameSoundsAtOneTime)
        {
            _host = host;

            _blockedClips = new HashSet<AudioClip>();
            _playedClips = new Dictionary<AudioClip, int>();

            _maxSameSoundsAtOneTime = maxSameSoundsAtOneTime;
            _minOffsetBetweenSameSounds = minOffsetBetweenSameSounds;
            _maxDifferentSoundsAtOneTime = maxDifferentSoundsAtOneTime;
        }

        /////////////////////////////////////////////////////////

        public void AddPlayed(AudioClip clip)
        {
            if (_playedClips.ContainsKey(clip))
                _playedClips[clip]++;
            else
                _playedClips[clip] = 1;

            _blockedClips.Add(clip);

            StartWaitingForClipUnblock(clip);
            StartWaitingWhileClipIsPlaying(clip);
        }

        public void RemovePlayed(AudioClip clip)
        {
            if (_playedClips.ContainsKey(clip) == false)
                return;

            if (--_playedClips[clip] <= 0)
                _playedClips.Remove(clip);
        }

        public bool CanBePlayed(AudioClip clip)
        {
            if (_blockedClips.Contains(clip))
                return false;

            if (_playedClips.Count >= _maxDifferentSoundsAtOneTime)
                return false;

            if (_playedClips.ContainsKey(clip) && _playedClips[clip] >= _maxSameSoundsAtOneTime)
                return false;

            return true;
        }

        public bool IsPlayed(AudioClip clip)
            => _playedClips.ContainsKey(clip);

        /////////////////////////////////////////////////////////

        private Coroutine StartWaitingWhileClipIsPlaying(AudioClip clip)
            => _host?.StartCoroutine(WaitForSeconds(clip.length, 
                () => RemovePlayed(clip)));

        private Coroutine StartWaitingForClipUnblock(AudioClip clip)
            => _host?.StartCoroutine(WaitForSeconds(_minOffsetBetweenSameSounds,
                () => _blockedClips.Remove(clip)));

        private IEnumerator WaitForSeconds(float seconds, Action callback)
        {
            yield return new WaitForSecondsRealtime(seconds);
            callback?.Invoke();
        }
    }
}
