﻿using UnityEngine;

namespace _Game.Scripts.SoundManagement.Collection
{
    interface IPlayedSoundsCollection
    {
        void AddPlayed(AudioClip clip);
        void RemovePlayed(AudioClip clip);
        bool CanBePlayed(AudioClip clip);
        bool IsPlayed(AudioClip clip);
    }
}
