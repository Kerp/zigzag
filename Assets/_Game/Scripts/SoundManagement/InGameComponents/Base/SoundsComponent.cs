﻿using UnityEngine;
using Zenject;

namespace _Game.Scripts.SoundManagement.InGameComponents.Base
{
    class SoundsComponent : MonoBehaviour
    {
        [SerializeField] private SoundManager _SoundManager = null;

        protected ISoundManager _soundManager { get; private set; }

        //////////////////////////////////////////////////////

        [Inject]
        public void Construct(ISoundManager soundManager)
        {
            SetSoundManager(soundManager);
        }

        //////////////////////////////////////////////////////

        private void Start()
        {
            SetSoundManager(_SoundManager);
        }

        protected virtual void OnSetSoundManager()
        { }

        //////////////////////////////////////////////////////

        private void SetSoundManager(ISoundManager soundManager)
        {
            if (_soundManager != null || soundManager == null)
                return;

            _soundManager = soundManager;
            OnSetSoundManager();
        }
    }
}
