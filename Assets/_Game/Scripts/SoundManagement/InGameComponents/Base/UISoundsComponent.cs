﻿using _Game.Scripts.SoundManagement.Library;
using UnityEngine;

namespace _Game.Scripts.SoundManagement.InGameComponents.Base
{
    class UISoundsComponent : SoundsComponent
    {
        protected IUILibrary _library => _soundManager?.library?.ui;
        protected AudioSource _source => _soundManager?.sources?.ui;
    }
}
