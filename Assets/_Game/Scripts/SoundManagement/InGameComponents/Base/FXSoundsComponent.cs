﻿using _Game.Scripts.SoundManagement.Library;
using UnityEngine;

namespace _Game.Scripts.SoundManagement.InGameComponents.Base
{
    class FXSoundsComponent : SoundsComponent
    {
        protected IFXLibrary _library => _soundManager?.library?.fx;
        protected AudioSource _source => _soundManager?.sources?.fx;
    }
}
