﻿using _Game.Scripts.SoundManagement.InGameComponents.Base;
using UnityEngine;

namespace _Game.Scripts.SoundManagement.InGameComponents
{
    class MusicSounds : MusicSoundsComponent
    {
        [SerializeField] private bool _PlayOnStart = false;

        ///////////////////////////////////////////////////////

        private void Start()
        {
            if (_PlayOnStart)
                PlayMusic();
        }

        ///////////////////////////////////////////////////////

        public void PlayMusic()
            => _soundManager?.PlayLooped(_source, _library?.randomMusic);

        public void StopMusic()
            => _soundManager?.Stop(_source);
    }
}
