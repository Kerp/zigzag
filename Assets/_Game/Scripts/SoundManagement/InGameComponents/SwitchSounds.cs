﻿using _Game.Scripts.SoundManagement.InGameComponents.Base;

namespace _Game.Scripts.SoundManagement.InGameComponents
{
    class SwitchSounds : UISoundsComponent
    {
        public void PlaySwitch()
            => _soundManager?.Play(_source, _library?.switchSwitched);
    }
}
