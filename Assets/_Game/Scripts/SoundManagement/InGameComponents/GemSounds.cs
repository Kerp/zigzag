﻿using _Game.Scripts.SoundManagement.InGameComponents.Base;

namespace _Game.Scripts.SoundManagement.InGameComponents
{
    class GemSounds : FXSoundsComponent
    {
        public void PlayCollected()
            => _soundManager?.Play(_source, _library?.collectGem);
    }
}
