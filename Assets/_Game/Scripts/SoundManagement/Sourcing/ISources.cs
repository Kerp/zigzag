﻿using UnityEngine;

namespace _Game.Scripts.SoundManagement.Sourcing
{
    interface ISources
    {
        AudioSource music { get; }
        AudioSource fx { get; }
        AudioSource ui { get; }
    }
}
