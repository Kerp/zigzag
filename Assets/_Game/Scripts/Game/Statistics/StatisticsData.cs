﻿using System;
using _Game.Scripts.Game.Achievments;
using _Game.Scripts.Game.Data;
using _Game.Scripts.Game.Data.Save;
using _Game.Scripts.Game.Social;

namespace _Game.Scripts.Game.Statistics
{
    public class StatisticsData : DataObject<SaveStatisticsData>
    {
        private SaveStatisticsData _data;
        private readonly AchievementsManager _achievementsManager;

        protected override string _save_key { get; } = "statistics_data";

        ////////////////////////////////////////////////////

        public StatisticsData(SaveManager saveManager, AchievementsManager achievementsManager) : base(saveManager)
        {
            _achievementsManager = achievementsManager;
        }

        ////////////////////////////////////////////////////

        public override SaveStatisticsData CreateSaveData() => _data;

        protected override void OnApply(SaveStatisticsData saveData, SaveApplyType applyType)
        {
            if (_data == null || applyType == SaveApplyType.Override)
                _data = saveData;
            else
                _data.SetMaxWith(saveData);
        }

        ////////////////////////////////////////////////////

        public void AddScoreForGame(int score)
        {
            SetData(ref _data.totalScore,
                ref _data.minScoreInGame,
                ref _data.maxScoreInGame, score);

            Log($"Score: {_data.totalScore}, {_data.minScoreInGame}, {_data.maxScoreInGame}");
            _achievementsManager.ReportScore(_data);
            InvokeOnChanged();
        }

        public void AddGemsForGame(int gems)
        {
            SetData(ref _data.totalGems,
                ref _data.minGemsInGame,
                ref _data.maxGemsInGame, gems);

            Log($"Gems: {_data.totalGems}, {_data.minGemsInGame}, {_data.maxGemsInGame}");
            _achievementsManager.ReportGems(_data);
            InvokeOnChanged();
        }

        public void AddDistanceForGame(int distance)
        {
            SetData(ref _data.totalDistance,
                ref _data.minDistanceInGame,
                ref _data.maxDistanceInGame, distance);

            Log($"Distance: {_data.totalDistance}, {_data.minDistanceInGame}, {_data.maxDistanceInGame}");
            InvokeOnChanged();
        }

        public void AddTurnsForGame(int turns)
        {
            SetData(ref _data.totalTurns,
                ref _data.minTurnsInGame,
                ref _data.maxTurnsInGame, turns);

            Log($"Turns: {_data.totalTurns}, {_data.minTurnsInGame}, {_data.maxTurnsInGame}");
            _achievementsManager.ReportTurns(_data);
            InvokeOnChanged();
        }

        public void SetTurnsOnPlatform(int turns)
        {
            if (SetMaxData(ref _data.maxTurnsOnPlatform, turns))
            {
                Log($"Turns on platforms: {turns}");
                InvokeOnChanged();
            }
        }

        ////////////////////////////////////////////////////

        public void SetGamesPlayed(int gamesPlayed)
        {
            _data.gamesPlayed = gamesPlayed;

            Log($"Games played: {_data.gamesPlayed}");
            _achievementsManager.ReportGamesPlayed(_data);
            InvokeOnChanged();
        }

        public void AddSession(DateTime session)
        {
            var lastDate = _data.lastDaySession;

            if (lastDate.Date == session.Date)
                return;

            var nextLastDay = lastDate.AddDays(1);
            if (nextLastDay.Date == session.Date)
            {
                _data.lastDaySession = session;
                AddToValue(ref _data.continuousDaysPlayed, 1);

                Log($"Sessions: {_data.continuousDaysPlayed}, {session}");
            }
            else
            {
                _data.lastDaySession = session;
                _data.continuousDaysPlayed = 1;

                Log($"Sessions cleared: {session}");
            }

            SetData(ref _data.minContinuousDaysPlayed, ref _data.maxContinuousDaysPlayed, _data.continuousDaysPlayed);

            _achievementsManager.ReportSessions(_data);
            InvokeOnChanged();
        }

        ////////////////////////////////////////////////////

        public void AddPurchase()
        {
            AddToValue(ref _data.purchases, 1);

            Log($"Purchase: {_data.purchases}");
            _achievementsManager.ReportPurchases(_data);
            InvokeOnChanged();
        }

        public void AddSkinBought()
        {
            AddToValue(ref _data.skinsBought, 1);

            Log($"SkinsBought: {_data.skinsBought}");
            _achievementsManager.ReportSkinsBought(_data);
            InvokeOnChanged();
        }

        ////////////////////////////////////////////////////

        public void SetAdsDisabled()
        {
            _data.disabledAds = true;

            Log("Ads disabled");
            _achievementsManager.ReportAdsDisabled(_data);
            InvokeOnChanged();
        }

        public void AddRewardedViewed()
        {
            AddToValue(ref _data.rewardedViewed, 1);

            Log($"Rewarded viewed: {_data.rewardedViewed}");
            _achievementsManager.ReportRewardedViews(_data);
            InvokeOnChanged();
        }

        ////////////////////////////////////////////////////

        public void ReadDocument(string header)
        {
            switch (header?.ToLower() ?? "")
            {
                case "privacy policy":
                    _data.readPrivacyPolicy = true;
                    break;
                case "terms of use":
                    _data.readTermsofUse = true;
                    break;
                case "partners data":
                    _data.readPartnersData = true;
                    break;
                default: return;
            }

            Log($"Read: {header}");
            _achievementsManager.ReportGDPR(_data);
            InvokeOnChanged();
        }

        ////////////////////////////////////////////////////

        public void ViewSocial(SocialParameters.Social socialType)
        {
            switch (socialType)
            {
                case SocialParameters.Social.VK:
                    _data.viewVk = true;
                    break;
                case SocialParameters.Social.Facebook:
                    _data.viewFacebook = true;
                    break;
                case SocialParameters.Social.Instagram:
                    _data.viewInstagram = true;
                    break;
                case SocialParameters.Social.Linkedin:
                    _data.viewLinkedin = true;
                    break;
                default: return;
            }

            Log($"View: {socialType}");
            _achievementsManager.ReportSocialViews(_data);
            InvokeOnChanged();
        }

        ////////////////////////////////////////////////////

        public void ChangeLanguage()
        {
            Log("Change language");
            _data.changeLanguage = true;
            _achievementsManager.ReportChangeLanguage(_data);
            InvokeOnChanged();
        }
        ////////////////////////////////////////////////////

        public void SetLeaderboardPosition(int position)
        {
            SetData(ref _data.minLeaderboardPosition, ref _data.maxLeaderboardPosition, position);

            Log($"LeaderboardPosition: {position}");
            _achievementsManager.ReportLeaderboard(_data);
            InvokeOnChanged();
        }

        ////////////////////////////////////////////////////

        public void SetSoundDisabledInPause()
        {
            _data.disableSoundInPause = true;

            Log("Sound disabled");
            _achievementsManager.ReportDisableSettings(_data);
            InvokeOnChanged();
        }

        public void SetMusicDisabledInPause()
        {
            _data.disableMusicInPause = true;

            Log("Music disabled");
            _achievementsManager.ReportDisableSettings(_data);
            InvokeOnChanged();
        }

        public void SetVibrationDisabledInPause()
        {
            _data.disabledVibrationInPause = true;

            Log("Vibration disabled");
            _achievementsManager.ReportDisableSettings(_data);
            InvokeOnChanged();
        }

        public void SetNotificationsDisabled()
        {
            _data.disabledNotifications = true;

            Log("Notifications disabled");
            _achievementsManager.ReportDisableSettings(_data);
            InvokeOnChanged();
        }


        ////////////////////////////////////////////////////

        public void AddGameShared()
        {
            AddToValue(ref _data.shareGame, 1);

            Log($"Game shared: {_data.shareGame}");
            _achievementsManager.ReportShares(_data);
            InvokeOnChanged();
        }

        public void AddResultShared()
        {
            AddToValue(ref _data.shareResult, 1);

            Log($"Result shared: {_data.shareResult}");
            _achievementsManager.ReportShares(_data);
            InvokeOnChanged();
        }

        ////////////////////////////////////////////////////

        public void FullReport()
        {
            Log("Full report");
            _achievementsManager.FullReport(_data);
        }

        ////////////////////////////////////////////////////

        private void SetData(ref int total, ref int min, ref int max, int value)
        {
            AddToValue(ref total, value);
            SetData(ref min, ref max, value);
        }

        private void SetData(ref int min, ref int max, int value)
        {
            if (max < value) max = value;
            if (min > value || min < 0) min = value;
        }

        private bool SetMaxData(ref int max, int value)
        {
            var result = max < value;
            if (result) max = value;
            return result;
        }

        private void AddToValue(ref int main, int addition)
        {
            var maxCurMain = int.MaxValue - addition;

            if (main < 0)
                main = 0;

            main = main < maxCurMain
                ? main + addition
                : int.MaxValue;
        }
    }
}