﻿using _Game.Scripts.Game.Objects.Ball;
using Core.Extensions;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.Statistics
{
    class BallStatisticsComponent : MyMonoBehaviour
    {
        [Inject] private StatisticsData _statistics { get; }
        
        [SerializeField] private BallObject _Ball = null;

        private int _counter;
        
        //////////////////////////////////////////////

        protected override void OnChecking()
        {
            CheckField(_Ball);
        }

        private void OnEnable()
        {
            _Ball.OnDirectionChanged.AddListener(OnDirectionChanged);
            _Ball.moveAllower.onSurfaceExit.AddListener(OnSurfaceExit);
        }

        //////////////////////////////////////////////

        private void OnDirectionChanged()
        {
            _counter++;
            _statistics.SetTurnsOnPlatform(_counter);
        }

        private void OnSurfaceExit()
        {
            _counter = 0;
        }
    }
}