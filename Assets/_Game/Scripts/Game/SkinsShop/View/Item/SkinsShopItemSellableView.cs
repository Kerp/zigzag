﻿using Core.Components;
using Core.Extensions;
using UnityEngine;

namespace _Game.Scripts.Game.SkinsShop.View.Item
{
    class SkinsShopItemSellableView : MyMonoBehaviour, ISkinsShopItemSellableView
    {
        [SerializeField] private CompositeText _PriceField = null;

        ///////////////////////////////////////////////////

        protected override void OnChecking()
            => CheckField(_PriceField);

        ////////////////////////////////////////////////////

        public void SetEnabled(bool status)
            => gameObject.SetActive(status);

        public void SetPrice(int price)
            => _PriceField.SetPart(price, 1);
    }
}
