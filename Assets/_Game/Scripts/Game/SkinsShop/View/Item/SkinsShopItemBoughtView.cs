using _Game.Scripts.AssetsLoading;
using Core.Extensions;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace _Game.Scripts.Game.SkinsShop.View.Item
{
    class SkinsShopItemBoughtView : MyMonoBehaviour, ISkinsShopItemBoughtView
    {
        [SerializeField] private Image _IconField = null;

        private AssetLoaderToImage _loaderCache;
        private AssetLoaderToImage _iconLoader => _loaderCache ??= new AssetLoaderToImage(_IconField);

        ///////////////////////////////////////////////////

        protected override void OnChecking()
            => CheckField(_IconField);

        private void OnDestroy() => _iconLoader.Unload();

        ////////////////////////////////////////////////////

        public void SetEnabled(bool status)
            => gameObject.SetActive(status);

        public void SetIcon(AssetReferenceSprite iconReference) => _iconLoader.Load(iconReference);
        public void SetIcon(AssetReferenceSprite iconReference, Color color) => _iconLoader.Load(iconReference, color);
    }
}
