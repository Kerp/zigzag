﻿namespace _Game.Scripts.Game.SkinsShop.View.Item
{
    interface ISkinsShopItemSellableView : ISkinsShopItemInnerView
    {
        void SetPrice(int price);
    }
}
