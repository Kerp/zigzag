﻿using UnityEngine;
using UnityEngine.AddressableAssets;

namespace _Game.Scripts.Game.SkinsShop.View.Item
{
    interface ISkinsShopItemBoughtView : ISkinsShopItemInnerView
    {
        void SetIcon(AssetReferenceSprite iconReference);
        void SetIcon(AssetReferenceSprite iconReference, Color color);
    }
}
