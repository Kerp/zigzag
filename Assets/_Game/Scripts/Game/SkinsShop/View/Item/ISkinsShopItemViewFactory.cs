﻿using Zenject;

namespace _Game.Scripts.Game.SkinsShop.View.Item
{
    interface ISkinsShopItemViewFactory : IFactory<ISkinsShopItemView>
    {
    }
}
