﻿using _Game.Scripts.Game.SkinsShop.View.Item;
using Core.Interfaces;
using UnityEngine;

namespace _Game.Scripts.Game.SkinsShop.View.Group
{
    interface ISkinsShopItemGroupView : IGameObjectHost
    {
        int groupId { get; }

        void Construct(Transform container, int groupId);
        ISkinsShopItemView[] GetItems();
    }
}
