﻿using System.Collections.Generic;
using _Game.Scripts.Game.SkinsShop.View.Item;
using UnityEngine;

namespace _Game.Scripts.Game.SkinsShop.View.Group
{
    class SkinsShopItemGroupView : MonoBehaviour, ISkinsShopItemGroupView
    {
        private HashSet<ISkinsShopItemView> _items { get; set; }

        public int groupId { get; private set; }

        /////////////////////////////////////////////////

        public void Construct(Transform container, int groupId)
        {
            this.groupId = groupId;
            transform.SetParent(container);
        }

        public ISkinsShopItemView[] GetItems()
            => GetComponentsInChildren<ISkinsShopItemView>(true);
    }
}
