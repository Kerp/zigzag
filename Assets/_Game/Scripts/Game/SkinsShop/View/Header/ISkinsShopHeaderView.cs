﻿using Core.Interfaces;
using UnityEngine;

namespace _Game.Scripts.Game.SkinsShop.View.Header
{
    interface ISkinsShopHeaderView : IGameObjectHost
    {
        void Construct(Transform container, string text);
    }
}
