﻿using _Game.Scripts.Game.Localization.Components;
using Core.Extensions;
using TMPro;
using UnityEngine;

namespace _Game.Scripts.Game.SkinsShop.View.Header
{
    [RequireComponent(typeof(LocalizedUniversalText))]
    [RequireComponent(typeof(TextMeshProUGUI))]
    class SkinsShopHeaderView : MyMonoBehaviour, ISkinsShopHeaderView
    {
        private LocalizedUniversalText _universalTextComponent;

        /////////////////////////////////////////////////////////

        public void Construct(Transform container, string text)
        {
            transform.SetParent(container);

            Attach(ref _universalTextComponent);
            _universalTextComponent.SetKey(text);
        }

        /////////////////////////////////////////////////////////

        protected override void OnAttaching()
            => Attach(ref _universalTextComponent);
    }
}
