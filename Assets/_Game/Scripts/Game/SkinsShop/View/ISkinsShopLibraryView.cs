﻿using System;
using _Game.Scripts.Game.SkinsShop.View.Item;

namespace _Game.Scripts.Game.SkinsShop.View
{
    interface ISkinsShopLibraryView
    {
        Action<ISkinsShopItemView> onItemClicked { get; set; }

        void Initialize();
        void Deinitialize();
    }
}
