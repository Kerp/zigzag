﻿using System;
using _Game.Scripts.Game.SkinsShop.Library;
using _Game.Scripts.Game.SkinsShop.View.Group;
using _Game.Scripts.Game.SkinsShop.View.Header;
using _Game.Scripts.Game.SkinsShop.View.Item;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Game.Scripts.Game.SkinsShop.View
{
    class SkinsShopLibraryView : ISkinsShopLibraryView
    {
        #region PRIVATE_FIELDS
        private readonly Transform _container;

        private readonly ISkinsManager _skinsManager;
        private readonly ISkinsShopItemViewFactory _itemsFactory;
        private readonly ISkinsShopHeaderViewFactory _headersFactory;
        private readonly ISkinsShopItemGroupViewFactory _itemContainerFactory;

        private readonly ISkinsShopLibrary _library;
        #endregion // PRIVATE_FIELDS

        #region PRIVATE_VALUES
        private bool _initialized { get; set; }
        #endregion // PRIVATE_VALUES

        #region PUBLIC_VALUES
        public Action<ISkinsShopItemView> onItemClicked { get; set; }
        #endregion // PUBLIC_VALUES

        /////////////////////////////////////////////////////

        #region CONSTRUCTORS
        public SkinsShopLibraryView(Transform container, ISkinsShopLibrary library, 
            ISkinsManager skinsManager, ISkinsShopItemViewFactory itemsFactory, 
            ISkinsShopHeaderViewFactory headersFactory, ISkinsShopItemGroupViewFactory itemContainerFactory)
        {
            _container = container;

            _library = library;
            _skinsManager = skinsManager;

            _itemsFactory = itemsFactory;
            _headersFactory = headersFactory;
            _itemContainerFactory = itemContainerFactory;
        }
        #endregion // CONSTRUCTORS

        /////////////////////////////////////////////////////

        #region PUBLIC_METHODS
        public void Initialize()
        {
            if (_initialized)
                return;

            foreach (var group in _library.itemsGroups)
                InitGroup(group);

            _initialized = true;
        }

        public void Deinitialize()
        {
            if (_initialized == false)
                return;

            foreach (Transform child in _container)
                Object.Destroy(child.gameObject);

            _initialized = false;
        }
        #endregion // PUBLIC_METHODS

        #region PRIVATE_INIT_METHODS
        private void InitGroup(SkinsShopGroup group)
        {
            InitHeader(group.header);
            InitItems(group.id, group.items);
        }

        private void InitHeader(string header)
        {
            _headersFactory.Create().Construct(_container, header);
        }

        private void InitItems(int idGroup, SkinsShopItem[] items)
        {
            var container = _itemContainerFactory.Create();
            container.Construct(_container, idGroup);

            foreach (var item in items)
                InitItem(idGroup, item, container);
        }

        private void InitItem(int idGroup, SkinsShopItem item, ISkinsShopItemGroupView group)
        {
            var itemPrefab = _itemsFactory.Create();

            itemPrefab.onClicked += OnItemClicked;

            itemPrefab.Construct(
                group.gameObject.transform,
                item,
                group);

            itemPrefab.SetBought(_skinsManager.IsBought(idGroup, item.id));
            itemPrefab.SetSelected(_skinsManager.IsSelected(idGroup, item.id));
        }
        #endregion // PRIVATE_INIT_METHODS

        #region PRIVATE_EVENT_HANDLERS
        private void OnItemClicked(ISkinsShopItemView itemView)
        {
            onItemClicked?.Invoke(itemView);
        }
        #endregion // PRIVATE_INIT_METHODS
    }
}
