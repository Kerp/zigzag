using _Game.Scripts.AssetsLoading;
using _Game.Scripts.Game.Objects.Background;
using _Game.Scripts.Game.SkinsShop.Data.SerializeObjects;
using UnityEngine;

namespace _Game.Scripts.Game.SkinsShop.SkinSetting
{
    [RequireComponent(typeof(BackgroundObject))]
    class BackgroundSkinsSetter : SkinSetter<BackgroundSkin>
    {
        private AssetLoaderByReference<Sprite> _spriteLoader = new AssetLoaderByReference<Sprite>();

        protected override IUnloadable[] _unloadableLoaders => new IUnloadable[] {_spriteLoader};

        //////////////////////////////////////

        protected override async void UpdateSkin()
        {
            if (TryGetContent(out var content) == false)
                return;

            if (TryGetComponent<BackgroundObject>(out var background) == false)
                return;

            var sprite = await _spriteLoader.Load(content.spriteRefernce);
            background.SetImage(sprite, content.color);
        }
    }
}
