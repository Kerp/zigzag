﻿using System;
using _Game.Scripts.Game.Data.Purchasing;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace _Game.Scripts.Game.SkinsShop.Library
{
    [Serializable]
    struct SkinsShopItem : IBuyable
    {
        [SerializeField] private int _Id;

        [SerializeField] private bool _AvailableByDefault;
        [SerializeField] private bool _SelectedByDefault;

        [SerializeField] private int _Price;
        [SerializeField] private AssetReferenceSprite _IconReference;
        [SerializeField] private Color _Color;

        [SerializeField] private ScriptableObject _Content;

        /////////////////////////////////////////////////

        public int id => _Id;
        public bool availableByDefault => _AvailableByDefault;
        public bool selectedByDefault => _SelectedByDefault;
        public int price => _Price;
        public AssetReferenceSprite iconReference => _IconReference;
        public Color color => _Color;
        public ScriptableObject content => _Content;

        /////////////////////////////////////////////////

        public SkinsShopItem(int id, int price, AssetReferenceSprite iconReference, Color color, ScriptableObject content) :
            this(id, price, iconReference, content) =>
            _Color = color;

        public SkinsShopItem(int id, int price, AssetReferenceSprite iconReference, ScriptableObject content)
        {
            _Id = id;
            _AvailableByDefault = false;
            _SelectedByDefault = false;
            _IconReference = iconReference;
            _Price = price;
            _Content = content;
            _Color = Color.white;
        }


        /////////////////////////////////////////////////

        public void SetId(int id) =>
            _Id = id;
    }
}