﻿using System;
using UnityEngine;

namespace _Game.Scripts.Game.SkinsShop.Library
{
    [Serializable]
    struct SkinsShopGroup
    {
        [SerializeField] private string _Header;
        [SerializeField] [Min(0)] private int _Id;

        [Space]
        [SerializeField] private SkinsShopItem[] _Items;

        public string header => _Header;
        public int id => _Id;
        public SkinsShopItem[] items => _Items;

        //////////////////////////////////////////////////////////

        public SkinsShopGroup(int id, params SkinsShopItem[] items)
        {
            _Id = id;
            _Header = "";
            _Items = items;
        }

        public SkinsShopGroup(int id, string header, params SkinsShopItem[] items) : this(id, items)
            => _Header = header;
    }
}
