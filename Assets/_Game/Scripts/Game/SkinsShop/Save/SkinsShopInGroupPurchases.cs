﻿using System;

namespace _Game.Scripts.Game.SkinsShop.Save
{
    [Serializable]
    public struct SkinsShopInGroupPurchases
    {
        public int groupId;
        public int[] itemIds;

        public SkinsShopInGroupPurchases(int groupId, params int[] itemIds)
        {
            this.groupId = groupId;
            this.itemIds = itemIds;
        }
    }
}
