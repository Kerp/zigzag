﻿using UnityEngine;
using UnityEngine.AddressableAssets;

namespace _Game.Scripts.Game.SkinsShop.Data.SerializeObjects
{
    [CreateAssetMenu(fileName = nameof(BallSkin), menuName = DefinitionsHelper.Skins + nameof(BallSkin))]
    class BallSkin : ScriptableObject
    {
        [SerializeField] private AssetReference _MaterialReference = null;

        public AssetReference materialReference => _MaterialReference;
    }
}
