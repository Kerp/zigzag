﻿using UnityEngine;
using UnityEngine.AddressableAssets;

namespace _Game.Scripts.Game.SkinsShop.Data.SerializeObjects
{
    [CreateAssetMenu(fileName = nameof(BackgroundSkin), menuName = DefinitionsHelper.Skins + nameof(BackgroundSkin))]
    class BackgroundSkin : ScriptableObject
    {
        [Header("Canvas")]
        [SerializeField] private AssetReferenceSprite _SpriteReference = null;
        [SerializeField] private Color _Color = Color.white;

        [Header("Skybox")]
        [SerializeField] private AssetReference _MaterialReference = null;
        [SerializeField] private Color _CameraColor = Color.white;

        public AssetReferenceSprite spriteRefernce => _SpriteReference;
        public Color color => _Color;
        public AssetReference materialReference => _MaterialReference;
        public Color cameraColor => _CameraColor;
    }
}
