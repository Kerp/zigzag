﻿using UnityEngine;
using UnityEngine.AddressableAssets;

namespace _Game.Scripts.Game.SkinsShop.Data.SerializeObjects
{
    [CreateAssetMenu(fileName = nameof(PlatformSkin), menuName = DefinitionsHelper.Skins + nameof(PlatformSkin))]
    class PlatformSkin : ScriptableObject
    {
        [SerializeField] private AssetReference _MaterialReference = null;

        public AssetReference materialReference => _MaterialReference;
    }
}
