﻿using System;
using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Game.Data;
using _Game.Scripts.Game.Data.Save;
using _Game.Scripts.Game.SkinsShop.Save;

namespace _Game.Scripts.Game.SkinsShop.Data
{
    public class SkinsSelectedData : DataObject<SaveSkinsSelectedData>
    {
        private const int _MAX_SELECTED = 1;
        private const int _DEFAULT_ID = -1;

        private Dictionary<int, int> _selectedItemsByGroups { get; set; }

        public event Action<int> onChangeSelectedInGroup;

        protected override string _save_key { get; } = "skins_selections_data";

        /////////////////////////////////////////////////////

        public SkinsSelectedData(SaveManager saveManager) : base(saveManager)
        { }

        /////////////////////////////////////////////////////

        public bool TryGetSelectedItem(int groupId, out int itemId)
            => _selectedItemsByGroups.TryGetValue(groupId, out itemId);

        public bool IsSelected(int groupId, int idItem)
            => TryGetSelectedItem(groupId, out var id) && id == idItem;

        public void SetItemAsSelected(int groupId, int itemId)
        {
            if (IsSelected(groupId, itemId))
                return;

            _selectedItemsByGroups[groupId] = itemId;

            onChangeSelectedInGroup?.Invoke(groupId);
            InvokeOnChanged();

            Save();
        }

        /////////////////////////////////////////////////////

        public override SaveSkinsSelectedData CreateSaveData() =>
            new SaveSkinsSelectedData()
            {
                groupedItems = ConvertToSaveFormat(_selectedItemsByGroups)
            };

        protected override void OnApply(SaveSkinsSelectedData saveData, SaveApplyType applyType)
        {
            var saveDictionary = ConvertFromSaveFormat(saveData.groupedItems);

            _selectedItemsByGroups =
                _selectedItemsByGroups == null || applyType == SaveApplyType.Override
                    ? saveDictionary
                    : ResultDataToApply(saveDictionary, _selectedItemsByGroups, applyType);

            foreach (var groupId in _selectedItemsByGroups.Keys)
                onChangeSelectedInGroup?.Invoke(groupId);
        }

        /////////////////////////////////////////////////////

        private SkinsShopInGroupSelections[] ConvertToSaveFormat(Dictionary<int, int> originalFormat)
        {
            if (originalFormat == null)
                return new SkinsShopInGroupSelections[0];

            return originalFormat
            .Select(x => new SkinsShopInGroupSelections(x.Key, x.Value)).ToArray();
        }

        private Dictionary<int, int> ConvertFromSaveFormat(SkinsShopInGroupSelections[] saveFormat)
        {
            if (saveFormat == null)
                return new Dictionary<int, int>();

            return saveFormat.ToDictionary(x => x.groupId, x => x.itemId);
        }
    }
}
