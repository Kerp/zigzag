﻿using _Game.Scripts.Game.Objects.Interfaces;

namespace _Game.Scripts.Game.Objects.Generating
{
    public abstract class AbstractProducer<T> : IProducer<T> where T : IRemoteDestroyable
    {
        #region PUBLIC_METHODS

        public T Produce()
        {
            var entity = CreateEntity();

            if (entity != null)
                entity.onBecomeDestroyable += HandleUnused;

            return entity;
        }

        public void HandleUnused(T[] entities)
        {
            for(var i = 0; i < entities.Length; i++)
                HandleUnused(entities[i]);
        }

        public void HandleUnused(T entity)
        {
            if (entity == null)
                return;

            entity.onBecomeDestroyable -= HandleUnused;
            HandleUnusedEntity(entity);
        }

        #endregion // PUBLIC_METHODS

        #region PRIVATE_METHODS

        private void HandleUnused(IRemoteDestroyable destroyable)
        {
            if (destroyable != null)
                HandleUnused(destroyable.gameObject.GetComponent<T>());
        }

        #endregion // PRIVATE_METHODS

        protected abstract T CreateEntity();
        protected abstract void HandleUnusedEntity(T entity);
    }
}
