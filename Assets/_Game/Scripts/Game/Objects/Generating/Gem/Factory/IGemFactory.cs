﻿using _Game.Scripts.Game.Objects.Gem;
using _Game.Scripts.Game.Objects.Interfaces;

namespace _Game.Scripts.Game.Objects.Generating.Gem.Factory
{
    interface IGemFactory : IFactory<IGem>
    {

    }
}
