﻿using _Game.Scripts.Game.Objects.Gem;
using _Game.Scripts.Game.Objects.Generating.Gem.Pool;

namespace _Game.Scripts.Game.Objects.Generating.Gem.Producer
{
    class GemProducerByPool : AbstractProducer<IGem>, IGemProducer
    {
        private readonly IGemPool _pool;

        public GemProducerByPool(IGemPool pool)
            => _pool = pool;

        protected override IGem CreateEntity()
            => _pool.Spawn();

        protected override void HandleUnusedEntity(IGem entity)
            => _pool.Despawn(entity);
    }
}
