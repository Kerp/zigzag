﻿using _Game.Scripts.Game.Objects.Gem;
using _Game.Scripts.Game.Objects.Generating.Gem.Factory;

namespace _Game.Scripts.Game.Objects.Generating.Gem.Producer
{
    class GemProducer : AbstractProducer<IGem>, IGemProducer
    {
        private readonly IGemFactory _factory;

        public GemProducer(IGemFactory factory)
        {
            _factory = factory;
        }

        protected override IGem CreateEntity()
            => _factory.Create();

        protected override void HandleUnusedEntity(IGem entity)
            => UnityEngine.Object.Destroy(entity.gameObject);
    }
}
