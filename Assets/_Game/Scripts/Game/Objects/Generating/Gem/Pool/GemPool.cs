﻿using _Game.Scripts.Game.Objects.Gem;
using UnityEngine;

namespace _Game.Scripts.Game.Objects.Generating.Gem.Pool
{
    class GemPool : AbstractPool<GemObject>, IGemPool
    {
        public GemPool(Transform parent) : base(parent)
        { }
    }
}
