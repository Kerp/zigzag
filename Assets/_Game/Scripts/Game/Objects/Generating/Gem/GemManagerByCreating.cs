﻿using _Game.Scripts.Game.Objects.Generating.Gem.Positioning;
using _Game.Scripts.Game.Objects.Generating.Gem.Producer;
using _Game.Scripts.Game.Objects.Generating.Gem.Variator;
using _Game.Scripts.Game.Objects.Platform;
using Zenject;

namespace _Game.Scripts.Game.Objects.Generating.Gem
{
    public class GemManagerByCreating : GemManager
    {
        [Inject] private IVariator _variator { get; }
        [Inject] private IGemProducer _producer { get; }
        [Inject] private IPositionNoiser _positionNoiser { get; }
        
        ////////////////////////////////////////////

        public override void Init()
        { }
        
        public void TryGenerate(IPlatform platform)
        {
            if (_variator.IsSuccess() == false)
                return;

            var position = _positionNoiser.NoisePosition(platform.positionToPlace);
            var gem = ConfigureGem(_producer.Produce(), position, platform);
            _gems.Add(gem);
        }

        public override void Reset()
        {
            base.Reset();
            _variator.Reset();
        }
    }
}
