﻿using _Game.Scripts.Game.Objects.Gem;
using _Game.Scripts.Game.Objects.Platform;
using UnityEngine;

namespace _Game.Scripts.Game.Objects.Generating.Gem
{
    public class GemManagerByMockup : GemManager
    {
        [SerializeField] private Transform _PlatformsContainer;
        
        public override void Init()
        {
            var platforms = _PlatformsContainer.gameObject.GetComponentsInChildren<IPlatform>();
            var gems = _Container.gameObject.GetComponentsInChildren<IGem>();

            for (var i = 0; i < gems.Length; i++)
            {
                var gem = gems[i];
                var position = gem.gameObject.transform.localPosition;
                var platform = GetNearestPlatform(position, platforms);
                
                _gems.Add(ConfigureGem(gem, position, platform));
            }
        }

        private IPlatform GetNearestPlatform(Vector3 position, IPlatform[] platforms)
        {
            var targetIndex = -1;
            var minDistance = float.MaxValue;

            for (var i = 0; i < platforms.Length; i++)
            {
                var distance = Vector3.Distance(position, platforms[i].gameObject.transform.position);
                if (distance < minDistance)
                {
                    minDistance = distance;
                    targetIndex = i;
                }
            }

            return targetIndex >= 0 ? platforms[targetIndex] : null;
        }
    }
}
