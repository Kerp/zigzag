﻿using _Game.Scripts.Game.Objects.Generating.Platform.Pool;
using _Game.Scripts.Game.Objects.Platform;

namespace _Game.Scripts.Game.Objects.Generating.Platform.Producer
{
    class PlatformProducerByPool : AbstractProducer<IPlatform>, IPlatformProducer
    {
        private readonly IPlatformPool _pool;

        public PlatformProducerByPool(IPlatformPool pool)
        {
            _pool = pool;
        }

        protected override IPlatform CreateEntity()
            => _pool.Spawn();

        protected override void HandleUnusedEntity(IPlatform entity)
            => _pool.Despawn(entity);
    }
}
