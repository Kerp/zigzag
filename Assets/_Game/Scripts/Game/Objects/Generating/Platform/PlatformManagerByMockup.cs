﻿using _Game.Scripts.Game.Objects.Platform;
using UnityEngine;

namespace _Game.Scripts.Game.Objects.Generating.Platform
{
    public class PlatformManagerByMockup : PlatformManager
    {
        public override void Init()
        {
            var areaSize = _parameters.initialAreaSize;
            var platforms = _PlatformsContainer.gameObject.GetComponentsInChildren<IPlatform>();
            
            var initialPlatformsCount = (int)Mathf.Pow(areaSize, 2);
            if (platforms.Length < initialPlatformsCount)
            {
                Error("Hasn't enough platforms!");
                return;
            }
            
            for (var y = 0; y < areaSize; y++)
            for (var x = 0; x < areaSize; x++)
            {
                var platform = platforms[y * areaSize + x];
                var position = platform.gameObject.transform.localPosition;
                
                if (x == 0) _calculator.AddNextStepByPosition(position);
                _initialPlatforms.Add(ConfigurePlatform(platform, position, y));
            }

            for (var i = initialPlatformsCount; i < platforms.Length; i++)
            {
                var platform = platforms[i];
                var step = i - initialPlatformsCount + areaSize;
                var position = platform.gameObject.transform.localPosition;
                
                _calculator.AddNextStepByPosition(position);
                _platformsBySteps.Add(step, ConfigurePlatform(platform, position, step));
            }

            OnChangeCurrentStep();
        }

        protected override void OnChangeCurrentStep()
        {
            if (currentStep > _parameters.initialAreaSize / 2)
                DeactivateBehindInitialPlatforms();
            
            CorrectPlatforms();
        }
    }
}