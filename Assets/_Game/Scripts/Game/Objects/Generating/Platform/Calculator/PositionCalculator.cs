﻿using System.Collections.Generic;
using Core.Systems.Reset;
using UnityEngine;

namespace _Game.Scripts.Game.Objects.Generating.Platform.Calculator
{
    public class PositionCalculator : IResetable
    {
        private SortedDictionary<int, Vector3> _preCalculatedPositions { get; }
        private Vector3 _positionOnLastStep { get; set; }

        private float _platformSize { get; }


        public int lastStep { get; private set; }
        public IEnumerable<int> calculatedSteps => _preCalculatedPositions.Keys;


        private const int DEFAULT_LAST_STEP = 0;
        private static readonly Vector3 DEFAULT_START_POSITIONS = Vector3.zero;

        ///////////////////////////////////////////////

        public PositionCalculator(float platformSize)
        {
            _platformSize = platformSize;

            lastStep = DEFAULT_LAST_STEP;
            _positionOnLastStep = DEFAULT_START_POSITIONS;
            _preCalculatedPositions = new SortedDictionary<int, Vector3>();
        }

        ///////////////////////////////////////////////

        public bool IsStepCalculated(int step) =>
            _preCalculatedPositions.ContainsKey(step);

        public Vector3 GetPosition(int step) =>
            IsStepCalculated(step) ? _preCalculatedPositions[step] : Vector3.zero;

        public void ClearStep(int step) =>
            _preCalculatedPositions.Remove(step);

        ///////////////////////////////////////////////

        public void Reset()
        {
            _preCalculatedPositions.Clear();

            lastStep = DEFAULT_LAST_STEP;
            _positionOnLastStep = DEFAULT_START_POSITIONS;
        }

        ///////////////////////////////////////////////

        public void AddNextStepByDirection(Vector3 direction, int count)
        {
            for (var i = 0; i < count; i++)
                AddNextStepByDirection(direction);
        }

        public void AddNextStepByDirection(Vector3 direction) =>
            AddNextStepByPosition(_positionOnLastStep + direction.normalized * _platformSize);


        public void AddNextStepByPosition(IEnumerable<Vector3> positions)
        {
            foreach (var position in positions)
                AddNextStepByPosition(position);
        }

        public void AddNextStepByPosition(Vector3 position)
        {
            _preCalculatedPositions[lastStep++] = position;
            _positionOnLastStep = position;
        }


        public Vector3[,] CalcNextRectSteps(Vector3 direction, int size)
        {
            var result = new Vector3[size, size];

            var width = _platformSize * size;
            var offset = width / 2f - _platformSize / 2f;
            var pDirection = new Vector3(direction.z, direction.y, -direction.x);
            var startPoint = _positionOnLastStep + (direction * _platformSize) - (pDirection * offset);

            for (var x = 0; x < size; x++)
            for (var z = 0; z < size; z++)
                result[x, z] = startPoint + (direction * z + pDirection * x) * _platformSize;

            return result;
        }
    }
}
