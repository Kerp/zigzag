﻿using _Game.Scripts.Game.Objects.Generating.Platform.Producer;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.Objects.Generating.Platform
{
    public class PlatformManagerByCreation : PlatformManager
    {
        [Inject] private IPlatformProducer _producer { get; }

        ///////////////////////////////////////////////////////////

        #region PUBLIC_METHODS

        public override void Init()
        {
            var rect = _calculator.CalcNextRectSteps(_direction.currentDirection, _parameters.initialAreaSize);
            CalculateFurtherStepsOneDirection(_parameters.initialAreaSize);
            CalculateFurtherStepsOneDirection(_parameters.initialStepsOneDirection);
            CalculateFurtherSteps(_parameters.initialAreaSize + _parameters.initialStepsOneDirection + 1, _parameters.initialPrecalculatedSteps);

            // в silentMode не отсылаются уведомления о создании. Тем самым начальная активность
            // не будет триггерить другие системы, в т.ч. генератор кристаллов.
            _silentMode = true;

            CreateInitialRectPlatforms(rect);

            _silentMode = false;

            CreateFurtherPlatforms();

            OnChangeCurrentStep();
        }

        #endregion // PUBLIC_METHODS

        #region STEP_OPERATIONS

        protected override void OnChangeCurrentStep()
        {
            if (currentStep > _parameters.initialAreaSize / 2)
                DeactivateBehindInitialPlatforms();

            CalculateFurtherSteps();
            CreateFurtherPlatforms();

            CorrectPlatforms();
        }

        #endregion // STEP_OPERATIONS

        #region CALCULATION_STEPS

        private void CalculateFurtherSteps() =>
            CalculateFurtherSteps(_calculator.lastStep + 1, currentStep + _parameters.stepThresholdForward);

        private void CalculateFurtherStepsOneDirection(int steps)
        {
            for (var i = 0; i < steps; i++)
                _calculator.AddNextStepByDirection(_direction.currentDirection);
        }

        private void CalculateFurtherSteps(int startStep, int finishStep)
        {
            for (var i = startStep; i <= finishStep; i++)
                _calculator.AddNextStepByDirection(_direction.ChangeDirection(i));
        }

        #endregion // CALCULATION_STEPS

        #region SHOW_FURTHER_PLATFORMS

        private void CreateInitialRectPlatforms(Vector3[,] rect)
        {
            for(var x = 0; x < rect.GetLength(0); x++)
            for(var y = 0; y < rect.GetLength(1); y++)
            {
                var step = y;
                var platform = ConfigurePlatform(_producer.Produce(), rect[x, y], step);
                _initialPlatforms.Add(platform);

                if (x == 0)
                    _platformsBySteps[step] = platform;
            }
        }

        private void CreateFurtherPlatforms()
        {
            var startStep = currentStep;
            var endStep = currentStep + _parameters.stepThresholdForward + 1;

            for (var step = startStep; step < endStep; step++)
                if (_calculator.IsStepCalculated(step) && !_platformsBySteps.ContainsKey(step))
                    _platformsBySteps[step] = ConfigurePlatform(_producer.Produce(), _calculator.GetPosition(step), step);
        }

        #endregion // HANDLING_PLATFORMS_EVENTS
    }
}