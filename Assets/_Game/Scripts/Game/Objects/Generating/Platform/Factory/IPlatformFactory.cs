﻿using _Game.Scripts.Game.Objects.Interfaces;
using _Game.Scripts.Game.Objects.Platform;

namespace _Game.Scripts.Game.Objects.Generating.Platform.Factory
{
    interface IPlatformFactory : IFactory<IPlatform>
    {
    }
}
