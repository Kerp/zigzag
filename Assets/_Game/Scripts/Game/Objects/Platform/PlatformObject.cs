﻿using System;
using _Game.Scripts.Game.Objects.Moving.Catcher;
using _Game.Scripts.Parameters.Interfaces;
using Core.Systems.Reset;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.Objects.Platform
{
    [RequireComponent(typeof(Animator))]
    class PlatformObject : DestroyableMonoBehaviour, IPlatform, IResetable
    {
        #region SERIALIZE_FIELDS

        [Header("Attachments")]
        [SerializeField] private Animator _Animator = null;
        [SerializeField] private PlatformSizer _PlatformSizer = null;
        [SerializeField] private MovableCatcher _Catcher = null;

        [SerializeField] private Collider[] _Colliders = new Collider[0];

        #endregion // SERIALIZE_FIELDS

        #region PUBLIC_VALUES

        public bool isActive { get; private set; } = true;
        public bool isUsing { get; private set; }
        public int index { get; set; }

        public Action<IPlatform> onStartBeUsing { get; set; }
        public Action<IPlatform> onFinishBeUsing { get; set; }
        public Action onDeactivating { get; set; }

        public Vector3 positionToPlace {
            get {
                if (_PlatformSizer == null)
                    return transform.localPosition;

                var mainPosition = transform.localPosition;
                var offset = Vector3.up * ( _PlatformSizer.thickness / 2f );

                return mainPosition + offset;
            }
        }

        #endregion // PUBLIC_VALUES

        #region CONSTS

        private const string APPEAR_TRIGGER = "Appear";
        private const string INITIAL_TRIGGER = "Initial";
        private const string DEACTIVATE_TRIGGER = "Deactivate";
        private const string DEACTIVATING_STATE = "Deactivating";

        #endregion // CONSTS

        //////////////////////////////////////

        #region CHECKING

        protected override void OnChecking()
        {
            CheckField(_Catcher);
            CheckField(_Animator);
            CheckField(_PlatformSizer);
        }

        #endregion // CHECKING

        #region INJECT

        [Inject]
        public void Conctruct(IPlatformParameters parameters)
            => _PlatformSizer.Set(parameters.size, parameters.thickness);

        #endregion // INJECT

        #region PUBLIC_METHODS

        public void Appear(bool animated)
        {
            if (animated)
                _Animator.SetTrigger(APPEAR_TRIGGER);
        }

        public void StartBeUsing()
        {
            if (isActive == false || isUsing)
                return;

            isUsing = true;
            onStartBeUsing?.Invoke(this);
        }

        public void FinishBeUsing()
        {
            if (isActive == false || isUsing == false)
                return;

            isUsing = false;
            onFinishBeUsing?.Invoke(this);
            Deactivate();
        }

        public void OnAnimationStart(AnimatorStateInfo stateInfo)
        { }

        public void OnAnimationEnd(AnimatorStateInfo stateInfo)
        { }

        public void Reset()
        {
            index = default;
            isUsing = false;
            isActive = true;

            onStartBeUsing = null;
            onFinishBeUsing = null;
            onDeactivating = null;

            _Animator.ResetTrigger(APPEAR_TRIGGER);
            _Animator.ResetTrigger(INITIAL_TRIGGER);
            _Animator.ResetTrigger(DEACTIVATE_TRIGGER);
            _Animator.WriteDefaultValues();

            _Catcher.Reset();

            transform.localPosition = Vector3.zero;
        }

        public void Deactivate()
        {
            if (isActive == false)
                return;

            isActive = false;
            _Animator.SetTrigger(DEACTIVATE_TRIGGER);

            onDeactivating?.Invoke();
        }

        #endregion // PUBLIC_METHODS
    }
}
