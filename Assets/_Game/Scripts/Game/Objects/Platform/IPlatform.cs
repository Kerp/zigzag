﻿using System;
using _Game.Scripts.Game.Objects.AnimatedObjects;
using _Game.Scripts.Game.Objects.Interfaces;
using Core.Systems.Reset;
using UnityEngine;

namespace _Game.Scripts.Game.Objects.Platform
{
    public interface IPlatform : IRemoteDestroyable, IAnimated, IResetable
    {
        bool isActive { get; }
        bool isUsing { get; }
        int index { get; set; }

        Action<IPlatform> onStartBeUsing { get; set; }
        Action<IPlatform> onFinishBeUsing { get; set; }
        Action onDeactivating { get; set; }

        Vector3 positionToPlace { get; }

        void Appear(bool animated);
        void Deactivate();
    }
}
