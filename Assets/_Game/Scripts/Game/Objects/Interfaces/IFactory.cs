﻿
namespace _Game.Scripts.Game.Objects.Interfaces
{
    interface IFactory<T>
    {
        T Create();
    }
}
