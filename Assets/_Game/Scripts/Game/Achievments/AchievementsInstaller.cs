﻿using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.Achievments
{
    [CreateAssetMenu(fileName = nameof(AchievementParameters), menuName = DefinitionsHelper.Installers + nameof(AchievementParameters))]
    public class AchievementsInstaller : ScriptableObjectInstaller<AchievementsInstaller>
    {
        [SerializeField] private AchievementParameters _Parameters = null;

        public override void InstallBindings()
        {
            Container.Bind<AchievementParameters>().FromInstance(_Parameters).AsSingle();
            Container.Bind<AchievementsManager>().AsSingle();
        }
    }
}