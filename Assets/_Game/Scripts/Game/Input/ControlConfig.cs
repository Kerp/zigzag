﻿using UnityEngine;

namespace _Game.Scripts.Game.Input
{
    [CreateAssetMenu(fileName = nameof(ControlConfig), menuName = DefinitionsHelper.Parameters + nameof(ControlConfig))]
    class ControlConfig : ScriptableObject
    {
        [Tooltip("Позволяет поворачивать тапом по экрану")]
        [SerializeField] private bool _ChangeByTouch = false;

        [Tooltip("Позволяет поворачивать кликом мыши")]
        [SerializeField] private bool _ChangeByClick = false;

        [Tooltip("Позволяет поворачивать клавишей на клавиатуре. None - отключено.")]
        [SerializeField] private KeyCode _ChangeDirectionKey = KeyCode.None;

        [Tooltip("Позволяет производить закрытие UI.")]
        [SerializeField] private KeyCode _BackKey = KeyCode.Escape;
        
        
        public bool changeByTouch => _ChangeByTouch;
        public bool changeByClick => _ChangeByClick;
        public KeyCode changeDirectionKey => _ChangeDirectionKey;
        public KeyCode backKey => _BackKey;
    }
}
