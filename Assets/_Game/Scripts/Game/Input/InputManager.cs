﻿using System.Collections.Generic;
using Core.Extensions;
using UnityEngine;
using UnityEngine.Events;

namespace _Game.Scripts.Game.Input
{
    using Input = UnityEngine.Input;

    class InputManager : LoggedMonoBehaviour
    {
        #region INSPECTOR

        [Header("Settings")]
        [SerializeField] private bool _ListenMouse = false;
        [SerializeField] private bool _ListenTouches = false;
        [SerializeField] private KeyCode[] _ListnedKeyCodes = new KeyCode[0];
        
        [Header("Events")]
        [SerializeField] private IntUnityEvent _OnMouseButtonDown = new IntUnityEvent();
        [SerializeField] private IntUnityEvent _OnMouseButtonUp = new IntUnityEvent();
        
        [SerializeField] private IntUnityEvent _OnTouchDown = new IntUnityEvent();
        [SerializeField] private IntUnityEvent _OnTouchUp = new IntUnityEvent();
        
        [SerializeField] private KeyCodeUnityEvent _OnKeyDown = new KeyCodeUnityEvent();
        [SerializeField] private KeyCodeUnityEvent _OnKeyUp = new KeyCodeUnityEvent();

        #endregion // INSPECTOR

        #region VALUES

        public bool listenMouse => _ListenMouse;
        public bool listenTouches => _ListenTouches;
        public List<KeyCode> keyCodes { get; private set; } = new List<KeyCode>();
        
        public event UnityAction<int> onMouseButtonDown
        {
            add => _OnMouseButtonDown.AddListener(value);
            remove => _OnMouseButtonDown.RemoveListener(value);
        }
        public event UnityAction<int> onMouseButtonUp
        {
            add => _OnMouseButtonUp.AddListener(value);
            remove => _OnMouseButtonUp.RemoveListener(value);
        }
        public event UnityAction<int> onTouchDown
        {
            add => _OnTouchDown.AddListener(value);
            remove => _OnTouchDown.RemoveListener(value);
        }
        public event UnityAction<int> onTouchUp
        {
            add => _OnTouchUp.AddListener(value);
            remove => _OnTouchUp.RemoveListener(value);
        }
        public event UnityAction<KeyCode> onKeyDown
        {
            add => _OnKeyDown.AddListener(value);
            remove => _OnKeyDown.RemoveListener(value);
        }
        public event UnityAction<KeyCode> onKeyUp
        {
            add => _OnKeyUp.AddListener(value);
            remove => _OnKeyUp.RemoveListener(value);
        }
        
        #endregion // VALUES

        //////////////////////////////////////

        private void Awake()
        {
            keyCodes.AddRange(_ListnedKeyCodes);
        }

        private void Update()
        {
            HandleKeyPress();
            HandleMousePress();
            HandleTouchPress();
        }

        //////////////////////////////////////

        public void AddListenedKeyCode(KeyCode keyCode) =>
            keyCodes.Add(keyCode);

        public void RemoveListenedKeyCode(KeyCode keyCode) =>
            keyCodes.Remove(keyCode);
        
        //////////////////////////////////////

        private void HandleKeyPress()
        {
            for (var i = 0; i < keyCodes.Count; i++)
            {
                var keyCode = keyCodes[i];
                
                if (Input.GetKeyDown(keyCode))
                {
                    _OnKeyDown.Invoke(keyCode);
                    Log($"Key down: {keyCode:F}");
                }
                else if (Input.GetKeyUp(keyCode))
                {
                    _OnKeyUp.Invoke(keyCode);
                    Log($"Key up: {keyCode:F}");
                }
            }
        }

        private void HandleMousePress()
        {
            if (_ListenMouse == false)
                return;

            for (var i = 0; i < 2; i++)
            {
                if (Input.GetMouseButtonDown(i))
                {
                    _OnMouseButtonDown.Invoke(i);
                    Log($"Mouse down: {i}");
                }
                else if (Input.GetMouseButtonUp(i))
                {
                    _OnMouseButtonUp.Invoke(i);
                    Log($"Mouse up: {i}");
                }
            }
        }

        private void HandleTouchPress()
        {
            if (_ListenTouches == false)
                return;
            
            if (Input.touchSupported == false)
                return;

            for (var i = 0; i < Input.touchCount; i++)
            {
                var touch = Input.touches[i];

                if (touch.phase == TouchPhase.Began)
                {
                    _OnTouchDown?.Invoke(i);
                    Log($"Touch down: {i}");
                }
                else if (touch.phase == TouchPhase.Ended)
                {
                    _OnTouchUp?.Invoke(i);
                    Log($"Touch up: {i}");
                }
            }
        }
    }
}
