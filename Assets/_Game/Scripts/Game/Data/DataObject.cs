﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using _Game.Scripts.Game.Data.Save;
using Core;
using UnityEngine;

namespace _Game.Scripts.Game.Data
{
    public enum SaveApplyType
    {
        Override,
        UseMaxValue
    }

    public abstract class DataObject<T> : LoggedClass where T : ISaveData, new()
    {
        public event Action onChanged;

        private static readonly BinaryFormatter _binaryFormatter;
        private readonly SaveManager _saveManager;

        protected abstract string _save_key { get; }

        ////////////////////////////////////////////

        static DataObject()
        {
            if (_binaryFormatter == null)
                _binaryFormatter = new BinaryFormatter();
        }

        protected DataObject(SaveManager saveManager, bool autoLoad = true)
        {
            _saveManager = saveManager;
            if (autoLoad)
                Apply(Load(), SaveApplyType.Override);
        }

        ////////////////////////////////////////////

        public void Save()
        {
            _saveManager.Save(_save_key, ToJson());
            Log("Save");
        }

        public T Load()
        {
            var result = FromJson(_saveManager.Load(_save_key));
            Log("Load");

            return result;
        }

        ////////////////////////////////////////////

        public byte[] ToBinary()
        {
            byte[] bytes;

            try
            {
                using (var stream = new MemoryStream())
                {
                    _binaryFormatter.Serialize(stream, CreateSaveData());
                    bytes = stream.ToArray();
                    Log("Serialized to binary");
                }
            }
            catch
            {
                bytes = new byte[0];
                Error("Can't serialize to binary!");
            }

            return bytes;
        }

        public T FromBinary(byte[] bytes)
        {
            if (bytes.Length == 0)
                return new T();

            T result;

            try
            {
                using (var stream = new MemoryStream(bytes))
                    result = (T) _binaryFormatter.Deserialize(stream);
                Log("Deserialized from binary");
            }
            catch
            {
                result = new T();
                Error("Can't deserialize from binary!");
            }

            return result;
        }

        ////////////////////////////////////////////

        public string ToJson()
        {
            string json;

            try
            {
                json = JsonUtility.ToJson(CreateSaveData());
                Log("Serialized to Json");
            }
            catch
            {
                json = "";
                Error("Can't serialize to Json!");
            }

            return json;
        }

        public T FromJson(string convertedData)
        {
            if (string.IsNullOrEmpty(convertedData))
                return new T();

            T result;

            try
            {
                result = JsonUtility.FromJson<T>(convertedData);
                Log("Deserialize from Json");
            }
            catch
            {
                result = new T();
                Error("Can't deserialize from Json!");
            }

            return result;
        }

        ////////////////////////////////////////////

        public abstract T CreateSaveData();

        public void Apply(T saveData, SaveApplyType applyType)
        {
            if (saveData == null)
            {
                Error("Null saveData");
                return;
            }

            OnApply(saveData, applyType);
            Log($"Applied: {applyType:F}");

            onChanged?.Invoke();
        }

        public void Apply(string json, SaveApplyType applyType) =>
            Apply(FromJson(json), applyType);

        public void Apply(byte[] bytes, SaveApplyType applyType) =>
            Apply(FromBinary(bytes), applyType);

        protected abstract void OnApply(T saveData, SaveApplyType applyType);

        ////////////////////////////////////////////

        protected void InvokeOnChanged() => onChanged?.Invoke();

        protected int ResultDataToApply(int saveData, int originalData, SaveApplyType applyType)
        {
            switch (applyType)
            {
                case SaveApplyType.Override: return saveData;
                case SaveApplyType.UseMaxValue: return Mathf.Max(saveData, originalData);
                default: return default;
            }
        }

        protected float ResultDataToApply(float saveData, float originalData, SaveApplyType applyType)
        {
            switch (applyType)
            {
                case SaveApplyType.Override: return saveData;
                case SaveApplyType.UseMaxValue: return Mathf.Max(saveData, originalData);
                default: return default;
            }
        }

        protected bool ResultDataToApply(bool saveData, bool originalData, SaveApplyType applyType)
        {
            switch (applyType)
            {
                case SaveApplyType.Override: return saveData;
                case SaveApplyType.UseMaxValue: return saveData || originalData;
                default: return false;
            }
        }

        protected DateTime ResultDataToApply(DateTime saveData, DateTime originalData, SaveApplyType applyType)
        {
            switch (applyType)
            {
                case SaveApplyType.Override: return saveData;
                case SaveApplyType.UseMaxValue: return saveData > originalData ? saveData : originalData;
                default: return DateTime.MinValue;
            }
        }

        protected SystemLanguage ResultDataToApply(SystemLanguage saveData, SystemLanguage originalData, SaveApplyType applyType)
        {
            switch (applyType)
            {
                case SaveApplyType.Override: return saveData;
                case SaveApplyType.UseMaxValue: return saveData > originalData ? saveData : originalData;
                default: return SystemLanguage.English;
            }
        }

        protected Dictionary<int, HashSet<int>> ResultDataToApply(Dictionary<int, HashSet<int>> saveData, Dictionary<int, HashSet<int>> originalData, SaveApplyType applyType)
        {
            switch (applyType)
            {
                case SaveApplyType.Override:
                    return saveData;

                case SaveApplyType.UseMaxValue:
                    foreach (var key in saveData.Keys)
                        if (originalData.ContainsKey(key))
                            originalData[key].UnionWith(saveData[key]);
                        else
                            originalData[key] = saveData[key];

                    return saveData;

                default: return null;
            }
        }

        protected Dictionary<int, int> ResultDataToApply(Dictionary<int, int> saveData, Dictionary<int, int> originalData, SaveApplyType applyType)
        {
            switch (applyType)
            {
                case SaveApplyType.Override:
                    return saveData;

                case SaveApplyType.UseMaxValue:
                    foreach (var key in saveData.Keys)
                        originalData[key] = originalData.ContainsKey(key)
                            ? ResultDataToApply(saveData[key], originalData[key], applyType)
                            : saveData[key];

                    return saveData;

                default: return null;
            }
        }
    }
}