﻿using _Game.Scripts.Game.Ads.Save;
using _Game.Scripts.Game.Data.Save;
using _Game.Scripts.Game.IAP.Management.Saving;
using _Game.Scripts.Game.Localization.Save;
using _Game.Scripts.Game.SkinsShop.Data;
using _Game.Scripts.Game.Statistics;

namespace _Game.Scripts.Game.Data.Aggregator
{
    public class DataAggregator : DataObject<SaveDataAggregator>
    {
        private readonly GameData _gameData;
        private readonly SettingsData _settingsData;
        private readonly StatisticsData _statisticsData;
        private readonly PurchasingData _purchasingData;
        private readonly LocalizationData _localizationData;
        private readonly RewardedCountingData _rewardedData;
        private readonly SkinsSelectedData _skinsSelectedData;
        private readonly SkinsPurchasesData _skinsPurchasesData;

        protected override string _save_key { get; } = "data_aggregator";

        //////////////////////////////////////////////////

        public DataAggregator(
            SaveManager saveManager,
            GameData gameData,
            SettingsData settingsData,
            StatisticsData statisticsData,
            PurchasingData purchasingData,
            LocalizationData localizationData,
            RewardedCountingData rewardedData,
            SkinsSelectedData skinsSelectedData,
            SkinsPurchasesData skinsPurchasesData) : base(saveManager, false)
        {
            _gameData = gameData;
            _settingsData = settingsData;
            _statisticsData = statisticsData;
            _purchasingData = purchasingData;
            _localizationData = localizationData;
            _rewardedData = rewardedData;
            _skinsSelectedData = skinsSelectedData;
            _skinsPurchasesData = skinsPurchasesData;
        }

        //////////////////////////////////////////////////

        public override SaveDataAggregator CreateSaveData() =>
            new SaveDataAggregator()
            {
                GameData = _gameData.CreateSaveData(),
                SettingsData = _settingsData.CreateSaveData(),
                StatisticsData = _statisticsData.CreateSaveData(),
                PurchasingData = _purchasingData.CreateSaveData(),
                LocalizationData = _localizationData.CreateSaveData(),
                RewardedData = _rewardedData.CreateSaveData(),
                SkinsPurchasesData = _skinsPurchasesData.CreateSaveData(),
                SkinsSelectedData = _skinsSelectedData.CreateSaveData()
            };

        protected override void OnApply(SaveDataAggregator saveData, SaveApplyType applyType)
        {
            _gameData.Apply(saveData.GameData, applyType);
            _gameData.Save();

            _settingsData.Apply(saveData.SettingsData, applyType);
            _settingsData.Save();

            _statisticsData.Apply(saveData.StatisticsData, applyType);
            _statisticsData.Save();

            _purchasingData.Apply(saveData.PurchasingData, applyType);
            _purchasingData.Save();

            _localizationData.Apply(saveData.LocalizationData, applyType);
            _localizationData.Save();

            _rewardedData.Apply(saveData.RewardedData, applyType);
            _rewardedData.Save();

            _skinsPurchasesData.Apply(saveData.SkinsPurchasesData, applyType);
            _skinsPurchasesData.Save();

            _skinsSelectedData.Apply(saveData.SkinsSelectedData, applyType);
            _skinsSelectedData.Save();
        }
    }
}