﻿
namespace _Game.Scripts.Game.Data.Purchasing
{
    interface IBuyable
    {
        int price { get; }
    }
}
