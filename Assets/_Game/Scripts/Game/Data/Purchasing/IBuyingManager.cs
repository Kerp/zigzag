﻿namespace _Game.Scripts.Game.Data.Purchasing
{
    interface IBuyingManager
    {
        bool CanBuy(IBuyable item);
        bool CanBuy(int price);
        bool Buy(IBuyable item);
        bool Buy(int price);
    }
}
