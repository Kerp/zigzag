﻿using _Game.Scripts.Game.Statistics;

namespace _Game.Scripts.Game.Data.Purchasing
{
    class BuyingManager : IBuyingManager
    {
        private readonly GameData _gameData;
        private readonly StatisticsData _statistics;

        ///////////////////////////////////////////////////

        public BuyingManager(GameData gameData, StatisticsData statisticsData)
        {
            _gameData = gameData;
            _statistics = statisticsData;
        }

        ///////////////////////////////////////////////////

        public bool CanBuy(IBuyable item)
            => CanBuy(item.price);

        public bool CanBuy(int price)
            => _gameData.gemsCash >= price;

        public bool Buy(IBuyable item)
            => Buy(item.price);

        public bool Buy(int price)
        {
            if (CanBuy(price) == false)
                return false;

            _gameData.gemsCash -= price;
            _statistics.AddSkinBought();
            return true;
        }
    }
}
