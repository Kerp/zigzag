﻿using Core.Extensions;
using Zenject;

namespace _Game.Scripts.Game.Data.UI
{
    abstract class GameDataComponent<T> : MyMonoBehaviour
    {
        [Inject] protected GameData _gameData { get; private set; }

        protected abstract T _data { get; }
    }
}
