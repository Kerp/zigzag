﻿using Core.Components;
using Core.Extensions;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.Data.UI
{
    [RequireComponent(typeof(CompositeText))]
    abstract class GameDataField<T> : MyMonoBehaviour
    {
        protected CompositeText _textComponent;
        [Inject] protected GameData _gameData { get; private set; }

        protected abstract T _data { get; }

        ///////////////////////////////////////////////

        private void OnEnable()
        {
            OnAttaching();
            OnSubscribing();

            _gameData.onChanged += UpdateText;
            UpdateText();
        }

        private void OnDisable()
        {
            _gameData.onChanged -= UpdateText;
            OnUnsubscribing();
        }

        protected override void OnAttaching()
            => Attach(ref _textComponent);

        ///////////////////////////////////////////////

        protected void UpdateText()
            => UpdateText(_data);
        
        protected abstract void UpdateText(T value);

        ///////////////////////////////////////////////

        protected virtual void OnSubscribing()
        { }

        protected virtual void OnUnsubscribing()
        { }
    }
}