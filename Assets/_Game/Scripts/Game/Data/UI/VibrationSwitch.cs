﻿using _Game.Scripts.UI.Elements;
using UnityEngine;

namespace _Game.Scripts.Game.Data.UI
{
    [RequireComponent(typeof(Switch))]
    class VibrationSwitch : SettingsDataSwitch
    {
        protected override bool _data => _settingsData.vibration;

        protected override void OnSwitch(bool result)
        {
            _settingsData.vibration = result;
            
            if (IsDisabledDuringTheGame(result))
                _statistics.SetVibrationDisabledInPause();
        }
    }
}