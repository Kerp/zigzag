﻿using _Game.Scripts.Extensions;
using UnityEngine;

namespace _Game.Scripts.Game.Data.UI
{
    abstract class GameDataAutoComponentsEnabler<T> : GameDataComponent<T>
    {
        [SerializeField] private BoolUnityEvent _EnableEvent = new BoolUnityEvent();
        protected abstract bool _isEnabled { get; }

        //////////////////////////////////////////////

        protected void OnEnable()
            => _EnableEvent?.Invoke(_isEnabled);
    }
}
