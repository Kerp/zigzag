﻿using _Game.Scripts.UI.Elements;
using UnityEngine;

namespace _Game.Scripts.Game.Data.UI
{
    [RequireComponent(typeof(Switch))]
    class SoundSwitch : SettingsDataSwitch
    {
        protected override bool _data => _settingsData.soundOn;

        protected override void OnSwitch(bool result)
        {
            _settingsData.soundOn = result;
            
            if (IsDisabledDuringTheGame(result))
                _statistics.SetSoundDisabledInPause();
        }
    }
}