﻿using _Game.Scripts.UI.Elements;
using UnityEngine;

namespace _Game.Scripts.Game.Data.UI
{
    [RequireComponent(typeof(Switch))]
    class MusicSwitch : SettingsDataSwitch
    {
        protected override bool _data => _settingsData.musicOn;

        protected override void OnSwitch(bool result)
        {
            _settingsData.musicOn = result;

            if (IsDisabledDuringTheGame(result))
                _statistics.SetMusicDisabledInPause();
        }
    }
}