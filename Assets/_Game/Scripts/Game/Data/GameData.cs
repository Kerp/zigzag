﻿using _Game.Scripts.Game.Data.Save;

namespace _Game.Scripts.Game.Data
{
    public class GameData : DataObject<SaveGameData>
    {
        #region PUBLIC_VALUES
        
        public int currentScore {
            get => _currentScore;
            set {
                _currentScore = value;
                newRecord = currentScore >= bestScore;
                
                if (_currentScore > bestScore)
                    bestScore = _currentScore;
                
                InvokeOnChanged();
            }
        }

        public int gemsCash {
            get => _gemsCash;
            set {
                _gemsCash = value;
                InvokeOnChanged();
            }
        }

        public int bestScore { get; private set; }

        public int currentGems
        {
            get => _currentGems;
            set
            {
                _currentGems = value;
                InvokeOnChanged();
            }
        }

        public int gamesPlayed
        {
            get => _gamesPlayed;
            set
            {
                _gamesPlayed = value;
                InvokeOnChanged();
            }
        }
        public bool newRecord { get; private set; }

        #endregion // PUBLIC_VALUES

        #region PRIVATE_VALUES

        private int _currentScore;
        private int _gemsCash;
        private int _currentGems;
        private int _gamesPlayed;

        #endregion // PRIVATE_VALUES

        protected override string _save_key { get; } = "game_data";

        /////////////////////////////////////////////////////////////

        public GameData(SaveManager saveManager) : base(saveManager)
        { }

        /////////////////////////////////////////////////////////////

        public override SaveGameData CreateSaveData() =>
            new SaveGameData()
            {
                bestScore = bestScore, 
                gamesPlayed = gamesPlayed, 
                gemsCash = gemsCash
            };

        protected override void OnApply(SaveGameData saveData, SaveApplyType applyType)
        {
            gemsCash = ResultDataToApply(saveData.gemsCash, gemsCash, applyType);
            bestScore = ResultDataToApply(saveData.bestScore, bestScore, applyType);
            gamesPlayed = ResultDataToApply(saveData.gamesPlayed, gamesPlayed, applyType);
        }
    }
}