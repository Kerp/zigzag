﻿using Core;
using UnityEngine;

namespace _Game.Scripts.Game.Data.Save
{
    public class SaveManager : LoggedClass
    {
        public void Save(string key, string json)
        {
            PlayerPrefs.SetString(key, json);
            PlayerPrefs.Save();

            Log($"Save '{key}': {json}");
        }

        public string Load(string key)
        {
            Log($"Load '{key}'");
            return PlayerPrefs.GetString(key);
        }
    }
}
