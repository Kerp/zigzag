﻿using System;

namespace _Game.Scripts.Game.Data.Save
{
    [Serializable]
    public class SaveGameData : ISaveData
    {
        public int gemsCash;
        public int bestScore;
        public int gamesPlayed;
    }
}
