﻿using System;
using Core.Systems.Reset;

namespace _Game.Scripts.Game.Scoring
{
    interface IScore : IResetable
    {
        int current { get; }
        Action<int> onChanged { get; set; }
        IScoreInner this[ScoreType value] { get; }

        void AddCounter(ScoreType type, IScoreInner score);
        IScoreInner GetCounter(ScoreType type);
        void RemoveCounter(ScoreType type);

        void Add(ScoreType type);
        void Add(ScoreType type, int count);

        void Substract(ScoreType type);
        void Substract(ScoreType type, int count);
    }
}
