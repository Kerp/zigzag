﻿using System;
using System.Collections.Generic;

namespace _Game.Scripts.Game.Scoring
{
    class Score : IScore
    {
        #region PRIVATE_FIELDS
        
        private Dictionary<ScoreType, IScoreInner> _scores;
        
        #endregion // PRIVATE_FIELDS

        #region PUBLIC_FIELDS
        
        public int current
        {
            get
            {
                var result = 0;
                
                foreach (var value in _scores.Values)
                    result += value.current;
                
                return result;
            }
        }

        public Action<int> onChanged { get; set; }

        public IScoreInner this[ScoreType value] => _scores[value];

        #endregion // PUBLIC_FIELDS

        /////////////////////////////////////////////////

        #region CONSTRUCTORS
        
        public Score()
        {
            _scores = new Dictionary<ScoreType, IScoreInner>();
        }
        
        #endregion // CONSTRUCTORS

        /////////////////////////////////////////////////

        #region PUBLIC_METHODS

        public void AddCounter(ScoreType type, IScoreInner score)
        {
            if (_scores.ContainsKey(type))
                return;

            _scores[type] = score;
            score.onChanged += OnChangedEvent;
        }

        public void RemoveCounter(ScoreType type)
        {
            if (_scores.ContainsKey(type) == false)
                return;

            _scores[type].onChanged -= OnChangedEvent;
            _scores.Remove(type);
        }

        private void OnChangedEvent(int count) => onChanged?.Invoke(current);

        public IScoreInner GetCounter(ScoreType type) 
            => _scores.TryGetValue(type, out var result) ?
            result : null;


        public void Add(ScoreType type)
        {
            if (_scores.TryGetValue(type, out var counter))
                counter.Add();
        }

        public void Add(ScoreType type, int count)
        {
            if (_scores.TryGetValue(type, out var counter))
                counter.Add(count);
        }

        public void Substract(ScoreType type)
        {
            if (_scores.TryGetValue(type, out var counter))
                counter.Substract();
        }

        public void Substract(ScoreType type, int count)
        {
            if (_scores.TryGetValue(type, out var counter))
                counter.Substract(count);
        }

        public void Reset()
        {
            foreach (var type in _scores.Keys)
                _scores[type].Reset();
        }
        
        #endregion // PUBLIC_METHODS
    }
}
