﻿using System;
using Core.Systems.Reset;

namespace _Game.Scripts.Game.Scoring
{
    interface IScoreInner : IResetable
    {
        int current { get; }
        Action<int> onChanged { get; set; }

        void Add();
        void Add(int count);

        void Substract();
        void Substract(int count);
    }
}
