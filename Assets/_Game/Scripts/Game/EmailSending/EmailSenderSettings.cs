﻿using UnityEngine;

namespace _Game.Scripts.Game.EmailSending
{
    [CreateAssetMenu(fileName = nameof(EmailSenderSettings), menuName = DefinitionsHelper.Parameters + nameof(EmailSenderSettings))]
    class EmailSenderSettings : ScriptableObject
    {
        [SerializeField] private string _Sender = "";
        [SerializeField] private string _Subject = "";
        [SerializeField] private string _Body = "";

        public string sender => _Sender;
        public string subject => _Subject;
        public string body => _Body;
    }
}
