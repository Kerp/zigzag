﻿using UnityEngine;

namespace _Game.Scripts.Game.Sharing.Settings
{
    [CreateAssetMenu(fileName = nameof(ShareSettings), menuName = DefinitionsHelper.Parameters + nameof(ShareSettings))]
    class ShareSettings : ScriptableObject
    {
        [Header("Concrete settings")]
        [SerializeField, Multiline] private string _MessageText = "";
        [SerializeField, Multiline] private string _PictureText = "";
        [SerializeField] private bool _UseImage = false;

        [Header("Attached Settings")]
        [SerializeField] private string _ImagePath = "";
        [SerializeField] private Color _BackgroundColor = Color.white;

        /////////////////////////////////////////////////////

        public bool useImage => _UseImage;

        public string messageText => _MessageText;
        public string pictureText => _PictureText;
        public string imagePath => _ImagePath;
        public Color backgroundColor => _BackgroundColor;
    }
}
