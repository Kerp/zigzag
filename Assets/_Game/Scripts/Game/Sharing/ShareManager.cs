﻿using System;
using System.Collections;
using _Game.Scripts.Game.Data;
using _Game.Scripts.Game.Sharing.Settings;
using _Game.Scripts.Game.Statistics;
using Core.Extensions;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.Sharing
{
    class ShareManager : MyMonoBehaviour
    {
        [Inject] private StatisticsData _statistics { get; }
        [Inject] private GameData _gameData { get; }

        [Header("Settings")]
        [SerializeField] private ShareSettings _GameShareSettings = null;
        [SerializeField] private ShareSettings _GameResultShareSettings = null;

        [Header("Components")]
        [SerializeField] private ShareCanvas _Canvas = null;

        /////////////////////////////////////////////////////

        protected override void OnChecking()
        {
            CheckField(_GameShareSettings);
            CheckField(_GameResultShareSettings);
            CheckField(_Canvas);
        }

        /////////////////////////////////////////////////////

        public void ShareGame() =>
            StartCoroutine(Sharing(_GameShareSettings, 
                callback: _statistics.AddGameShared ));

        public void ShareGameResult() =>
            StartCoroutine(Sharing(_GameResultShareSettings, _gameData.currentScore,
                _statistics.AddResultShared));

        /////////////////////////////////////////////////////

        private IEnumerator Sharing(ShareSettings settings, int score = 0, Action callback = null)
        {
            var share = new NativeShare();

            share.SetText(string.Format(settings.messageText, score));

            if (settings.useImage)
            {
                ActivateCanvas(settings, score);

                yield return new WaitForEndOfFrame();

                share.AddFile(ScreenCapture.CaptureScreenshotAsTexture());

                DeactivateCanvas();
            }

            share.Share();
            callback?.Invoke();
        }

        private void ActivateCanvas(ShareSettings settings, int score)
        {
            _Canvas.Activate();

            _Canvas.SetText(string.Format(settings.pictureText, score));

            _Canvas.SetImage(settings.imagePath);
            _Canvas.SetBackgroundColor(settings.backgroundColor);
        }

        private void DeactivateCanvas() =>
            _Canvas.Deactivate();
    }
}
