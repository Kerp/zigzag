﻿using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.Toast
{
    public class ToastsManager : MyMonoBehaviour
    {
        [Inject] private ToastsLibrary _Library { get; }
        [Inject] private ToastsProducer _Producer { get; }
        [Inject] private Transform _Container { get; }
        [Inject] private Canvas _Canvas { get; }


        [SerializeField] private int _MaxToastsInOneTime = 3;

        private HashSet<ToastItem> _toastItems = new HashSet<ToastItem>();


        private void OnEnable() => CorrectCanvasActivity();
        private void OnDisable() => HideAllToasts(true);

        #region SHOWING

        public void ShowToast(ToastType type)
        {
            if (_Library.TryGetToast(type, out var toastData) == false)
            {
                Error($"Can't get toast {type}");
                return;
            }

            ShowToast(toastData);
        }

        public void ShowToast(Toast toastData)
        {
            if (_toastItems.Count >= _MaxToastsInOneTime)
            {
                Warning("Can't show more toasts");
                return;
            }

            var toast = CreateToast(toastData);

            toast.shown.AddListener(() => AddItem(toast));
            toast.hidden.AddListener(() =>
            {
                RemoveItem(toast);
                toast.SetDestroyed();
            });

            toast.Show();

            Log($"Show Toast: {toastData.text}");
        }

        private ToastItem CreateToast(Toast toastData)
        {
            var toast = _Producer.Produce();
            var toastTransform = toast.transform;

            toastTransform.parent = _Container;
            toastTransform.SetAsLastSibling();

            toast.Init(toastData);
            return toast;
        }

        #endregion // SHOWING


        #region HIDING

        public void HideAllToasts(bool forced = false)
        {
            foreach (var toast in _toastItems)
                toast.Hide(forced);

            _toastItems.Clear();
            Log("Hide all toasts");
        }

        #endregion // HIDING

        #region IN_COLLECTION_ADDING

        private void AddItem(ToastItem item)
        {
            _toastItems.Add(item);
            CorrectCanvasActivity();
        }

        private void RemoveItem(ToastItem item)
        {
            _toastItems.Remove(item);
            CorrectCanvasActivity();
        }

        private void CorrectCanvasActivity()
        {
            var canvas = _Canvas.gameObject;
            var hasElements = _toastItems.Any();

            if (canvas.activeSelf && !hasElements)
                canvas.SetActive(false);

            if (!canvas.activeSelf && hasElements)
                canvas.SetActive(true);
        }

        #endregion // IN_COLLECTION_ADDING
    }
}
