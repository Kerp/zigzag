﻿using System;
using System.Collections;
using _Game.Scripts.Game.Localization;
using _Game.Scripts.Game.Objects;
using Core.Components;
using Core.Systems.Reset;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace _Game.Scripts.Game.Toast
{
    public class ToastItem : DestroyableMonoBehaviour, IResetable
    {
        [Inject] private LocalizationManager _LocalizationManager { get; }

        [Header("Animation Config")]
        [SerializeField] private float _AnimationDuration = 0.5f;
        [SerializeField] private float _StartAlpha = 0f;
        [SerializeField] private float _EndAlpha = 1f;
        [SerializeField] private Vector3 _StartScale = Vector3.zero;
        [SerializeField] private Vector3 _EndScale = Vector3.one;

        [Header("Components")]
        [SerializeField] private UniversalText _Text = null;
        [SerializeField] private CanvasGroup _CanvasGroup = null;

        [Header("Events")]
        [SerializeField] private UnityEvent _Shown = new UnityEvent();
        [SerializeField] private UnityEvent _Hidden = new UnityEvent();

        public UnityEvent shown => _Shown;
        public UnityEvent hidden => _Hidden;


        private float _waitingDuration;
        private Coroutine _routine;


        public void Init(Toast toast)
        {
            _waitingDuration = toast.duration;

            _Text.SetText(toast.needLocalize ?
                _LocalizationManager.LocalizeText(toast.text) :
                toast.text);
        }

        public void Reset()
        {
            _Shown.RemoveAllListeners();
            _Hidden.RemoveAllListeners();
        }

        public void Show(bool forced = false)
        {
            _Shown?.Invoke();

            if (forced)
            {
                SetStatus(_EndAlpha, _EndScale);
                StartWaiting(_waitingDuration, () => Hide(true));
            }
            else
                StartShowAnimation(_AnimationDuration, () =>
                    StartWaiting(_waitingDuration, () => Hide()));
        }

        public void Hide(bool forced = false)
        {
            if (forced)
            {
                SetStatus(_StartAlpha, _StartScale);
                _Hidden?.Invoke();
            }
            else
                StartHideAnimation(_AnimationDuration, () =>
                    _Hidden?.Invoke());
        }


        private void StartShowAnimation(float duration, Action callback) =>
            StartAnimation(_StartAlpha, _EndAlpha, _StartScale, _EndScale, duration, callback);

        private void StartHideAnimation(float duration, Action callback) =>
            StartAnimation(_EndAlpha, _StartAlpha, _EndScale, _StartScale, duration, callback);

        private void StartAnimation(float startAlpha, float targetAlpha,
            Vector3 startScale, Vector3 targetScale, float duration, Action callback)
        {
            StopRoutine(ref _routine);
            _routine = StartCoroutine(AnimationRoutine(startAlpha, targetAlpha,
                startScale, targetScale, duration, callback));
        }

        private void StartWaiting(float duration, Action callback)
        {
            StopRoutine(ref _routine);
            _routine = StartCoroutine(WaitingRoutine(duration, callback));
        }

        private void StopRoutine(ref Coroutine routine)
        {
            if (routine != null)
                StopCoroutine(routine);

            routine = null;
        }

        private IEnumerator WaitingRoutine(float duration, Action callback)
        {
            yield return new WaitForSecondsRealtime(duration);
            callback?.Invoke();
        }

        private IEnumerator AnimationRoutine(float startAlpha, float targetAlpha,
            Vector3 startScale, Vector3 targetScale, float duration, Action callback)
        {
            SetStatus(startAlpha, startScale);

            for (var t = 0f; t < duration; t += Time.unscaledDeltaTime)
            {
                var ratio = t / duration;
                var alpha = Mathf.Lerp(startAlpha, targetAlpha, ratio);
                var scale = Vector3.Lerp(startScale, targetScale, ratio);
                SetStatus(alpha, scale);
                yield return null;
            }

            SetStatus(targetAlpha, targetScale);
            callback?.Invoke();
        }

        private void SetStatus(float alpha, Vector3 scale)
        {
            transform.localScale = scale;
            _CanvasGroup.alpha = alpha;
        }
    }
}
