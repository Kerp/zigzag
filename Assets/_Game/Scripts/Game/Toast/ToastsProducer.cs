﻿using _Game.Scripts.Game.Objects.Generating;

namespace _Game.Scripts.Game.Toast
{
    public class ToastsProducer : AbstractProducer<ToastItem>
    {
        private readonly ToastsPool _pool;

        public ToastsProducer(ToastsPool pool) =>
            _pool = pool;

        protected override ToastItem CreateEntity()
            => _pool.Spawn();

        protected override void HandleUnusedEntity(ToastItem entity)
            => _pool.Despawn(entity);
    }
}
