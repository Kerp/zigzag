﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace _Game.Scripts.Game.Toast
{
    [CreateAssetMenu(fileName = nameof(ToastsLibrary), menuName = DefinitionsHelper.Parameters + nameof(ToastsLibrary))]
    public class ToastsLibrary : ScriptableObject
    {
        [SerializeField] private ToastLibraryItem[] _Toats = new ToastLibraryItem[0];

        public IReadOnlyList<ToastLibraryItem> toasts => _Toats;


        public Toast GetToast(ToastType type)
        {
            TryGetToast(type, out var toast);
            return toast;
        }

        public bool TryGetToast(ToastType type, out Toast toast)
        {
            for (var i = 0; i < _Toats.Length; i++)
                if (_Toats[i].type == type)
                {
                    toast = _Toats[i].toast;
                    return true;
                }

            toast = default;
            return false;
        }
    }

    public enum ToastType
    {
        None,
        NotEnoughGems,
        CantPurchase,
        CantSyncCloudData
    }

    [Serializable]
    public class ToastLibraryItem
    {
        [SerializeField] private ToastType _Type = ToastType.None;
        [SerializeField] private Toast _Toast = new Toast();

        public ToastType type => _Type;
        public Toast toast => _Toast;
    }

    [Serializable]
    public struct Toast
    {
        [SerializeField] private string _Text;
        [SerializeField] private float _Duration;
        [SerializeField] private bool _NeedLocalize;

        public string text => _Text;
        public float duration => _Duration;
        public bool needLocalize => _NeedLocalize;

        public Toast(string text, float duration, bool needLocalize = false)
        {
            _Text = text;
            _Duration = duration;
            _NeedLocalize = needLocalize;
        }
    }
}
