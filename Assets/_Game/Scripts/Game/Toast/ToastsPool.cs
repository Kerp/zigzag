﻿using _Game.Scripts.Game.Objects.Generating;
using UnityEngine;

namespace _Game.Scripts.Game.Toast
{
    public class ToastsPool : AbstractPool<ToastItem>
    {
        public ToastsPool(Transform parent) : base(parent)
        { }
    }
}
