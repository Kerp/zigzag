﻿using System;
using _Game.Scripts.Extensions;
using _Game.Scripts.Game.Analytics;
using _Game.Scripts.Game.Data;
using _Game.Scripts.Game.Objects.Ball;
using _Game.Scripts.Game.Objects.Generating.Gem;
using _Game.Scripts.Game.Objects.Generating.Platform;
using _Game.Scripts.Game.Scoring;
using _Game.Scripts.Game.Statistics;
using _Game.Scripts.Game.Vibration;
using _Game.Scripts.GooglePlay;
using Cinemachine;
using Core.Extensions;
using Core.Modules;
using Core.Systems.Reset;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace _Game.Scripts.Game
{
    class GameManager : MonoBehaviour, IGameManager
    {
        #region INJECTED_VALUES
        [Inject] public IAnalyticsManager analytics { get; }
        [Inject] public GooglePlayServiceManager gpServices { get; }
        [Inject] public IBall ball { get; }
        [Inject] public VibrationManager vibrationManager { get; }
        [Inject] public CinemachineVirtualCamera virtualCamera { get; }
        [Inject] public GemManager gemManager { get; }
        [Inject] public PlatformManager platformManager { get; }
        
        [Inject] public IScore score { get;}
        [Inject(Id = ScoreType.Gems)] public IScoreInner gemsCounter { get; }

        [Inject] public GameData gameData { get; }
        [Inject] public StatisticsData statistics { get; }
        [Inject] public ITimeScaleManager timeScale { get; }
        #endregion // INJECTED_VALUES

        #region  EVENTS

        [SerializeField] private UnityEvent _OnGameLaunched = new UnityEvent();
        [SerializeField] private UnityEvent _OnGameStart = new UnityEvent();
        [SerializeField] private UnityEvent _OnGameRestart = new UnityEvent();
        [SerializeField] private UnityEvent _OnGamePaused = new UnityEvent();
        [SerializeField] private UnityEvent _OnGameUnpaused = new UnityEvent();
        [SerializeField] private UnityEvent _OnGameOver = new UnityEvent();

        [SerializeField] private IntUnityEvent _OnCurrentGemsChanged = new IntUnityEvent();
        [SerializeField] private IntUnityEvent _OnScoreChanged = new IntUnityEvent();

        public UnityEvent onGameLaunched {
            get => _OnGameLaunched;
            set => _OnGameLaunched = value;
        }
        public UnityEvent onGameStart {
            get => _OnGameStart;
            set => _OnGameStart = value;
        }        
        public UnityEvent onGameRestart {
            get => _OnGameRestart;
            set => _OnGameRestart = value;
        }        
        public UnityEvent onGamePaused {
            get => _OnGamePaused;
            set => _OnGamePaused = value;
        }        
        public UnityEvent onGameUnpaused {
            get => _OnGameUnpaused;
            set => _OnGameUnpaused = value;
        }        
        public UnityEvent onGameOver {
            get => _OnGameOver;
            set => _OnGameOver = value;
        }

        public IntUnityEvent onCurrentGemsChanged {
            get => _OnCurrentGemsChanged;
            set => _OnCurrentGemsChanged = value;
        }

        public IntUnityEvent onScoreChanged {
            get => _OnScoreChanged;
            set => _OnScoreChanged = value;
        }
        #endregion // EVENTS

        #region PUBLIC_VALUES
        public bool gamePaused { get; private set; }
        public bool gameStarted { get; private set; }
        #endregion // PUBLIC_VALUES

        #region PRIVATE_FIELDS
        private int _lastStep { get; set; }
        #endregion // PRIVATE_FIELDS

        //////////////////////////////////////////////

        #region MONO_BEHAVIOUR
        private void Start()
        {
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 60;

            Initialize();
            
            statistics.AddSession(DateTime.Now);
            gpServices.onAuthentificateStatusChanged += statistics.FullReport;
        }

        private void OnEnable()
        {
            onScoreChanged?.Invoke(score.current);
            onCurrentGemsChanged?.Invoke(gemsCounter.current);

            score.onChanged += OnScoreChangedEvent;
            gemsCounter.onChanged += OnCurrentGemsChangedEvent;
        }

        private void OnDisable()
        {
            score.onChanged -= OnScoreChangedEvent;
            gemsCounter.onChanged -= OnCurrentGemsChangedEvent;
        }

        private void OnScoreChangedEvent(int count) => onScoreChanged?.Invoke(count);
        private void OnCurrentGemsChangedEvent(int count) => onCurrentGemsChanged?.Invoke(count);

        private void OnApplicationQuit()
        {
            gameData.Save();
        }

        private void OnApplicationFocus(bool hasFocus)
        {
            if (hasFocus == false)
                SetPause(true);
        }

        #endregion // MONO_BEHAVIOUR

        #region PUBLIC_METHODS
        public void StartGame()
        {
            if (gameStarted)
                return;

            gameStarted = true;
            onGameStart?.Invoke();

            platformManager.SetActive(true);
            ball.StartMove();

            analytics.StartGame();
        }

        public void EndGame()
        {
            if (gameStarted == false)
                return;

            SetPause(false);
            gameStarted = false;
            FinishGameData();
            vibrationManager.VibrateOnLose();

            virtualCamera.enabled = false;
            platformManager.SetActive(false);
            ball.StopMove();

            analytics.GameOver(AnalyticsHelper.GetGameOverEventData(gameData, score));
            onGameOver?.Invoke();
        }

        public void SetPause(bool status)
        {
            if (gameStarted == false)
                return;

            if (gamePaused == status)
                return;

            gamePaused = status;
            timeScale.EnableScale(!status);

            if (status) onGamePaused?.Invoke();
            else onGameUnpaused?.Invoke();
        }

        public void Restart()
        {
            if (gameStarted)
                FinishGameData();

            analytics.RestartGame(AnalyticsHelper.GetRestartEventData(this));

            SetPause(false);
            gameStarted = false;

            timeScale.Reset();
            gemsCounter.Reset();
            score.Reset();

            foreach (var obj in FindObjectsOfType<GameObjectReseter>())
                obj.Reset();

            GC.Collect();
            Resources.UnloadUnusedAssets();

            Initialize();
            _OnGameRestart?.Invoke();
        }

        public void OnCurrentStepChanged(int step)
        {
            if (step <= _lastStep)
                return;

            score.Add(ScoreType.Distance, step - _lastStep);
            _lastStep = step;
        }

        public void OnChangeDirection() =>
            score.Add(ScoreType.Turns);

        public void OnGemCollected()
        {
            vibrationManager.VibrateOnCollect();
            score.Add(ScoreType.Gems);
            gemsCounter.Add();
        }

        public void AddGems(int count)
        {
            gameData.gemsCash += count;
            gameData.Save();
        }

        public void Quit()
        {
            Application.Quit();
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
        }
        #endregion // PUBLIC_METHODS

        #region PRIVATE_METHODS
        private void Initialize()
        {
            gameStarted = false;
            gamePaused = false;

            timeScale.Start();

            _lastStep = 0;
            virtualCamera.enabled = true;

            gemManager.Init();
            platformManager.Init();
            ball.SetPositionOnSurface(platformManager.GetInitialPositionOn());

            onGameLaunched?.Invoke();
        }

        private void FinishGameData()
        {
            gameData.gamesPlayed++;
            gameData.gemsCash += gemsCounter.current;
            gameData.currentGems = gemsCounter.current;
            gameData.currentScore = score.current;
            gameData.Save();

            gpServices.leaderboard.ReportScore(score.current);
            
            statistics.SetGamesPlayed(gameData.gamesPlayed);
            statistics.AddScoreForGame(score.current);
            statistics.AddGemsForGame(gemsCounter.current);
            statistics.AddTurnsForGame(score[ScoreType.Turns].current);
            statistics.AddDistanceForGame(score[ScoreType.Distance].current);
            gpServices.leaderboard.GetRank(rank => statistics.SetLeaderboardPosition(rank));
            
            statistics.Save();
            gpServices.cloudSaving.SaveGame();
        }
        #endregion // PRIVATE_METHODS
    }
}