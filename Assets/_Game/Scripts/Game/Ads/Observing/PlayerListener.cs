﻿using UnityEngine;
using UnityEngine.Events;

namespace _Game.Scripts.Game.Ads.Observing
{
    class PlayerListener : LoggedMonoBehaviour
    {
        public UnityEvent onShowing = new UnityEvent();
        public UnityEvent onHided = new UnityEvent();
        public UnityEvent onDestroy = new UnityEvent();

        private void OnDestroy() =>
            onDestroy?.Invoke();

        public void NotifyShowing()
        {
            Log("Notify Showing");
            onShowing?.Invoke();
        }

        public void NotifyHided()
        {
            Log("Notify Showing");
            onHided?.Invoke();
        }
    }
}
