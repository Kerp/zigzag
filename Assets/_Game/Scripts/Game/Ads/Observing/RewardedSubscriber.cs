﻿namespace _Game.Scripts.Game.Ads.Observing
{
    class RewardedSubscriber : PlayerSubscriber<RewardedListener>
    {
        protected override IAdsPlayer _player => _AdsManager.rewarded;
    }
}
