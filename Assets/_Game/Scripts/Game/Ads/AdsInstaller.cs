﻿using _Game.Scripts.Game.Ads.AdMob;
using _Game.Scripts.Game.Ads.Settings;
using _Game.Scripts.Game.Ads.UnityAds;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.Ads
{
    [CreateAssetMenu(fileName = nameof(AdsInstaller), menuName = DefinitionsHelper.Installers + nameof(AdsInstaller))]
    public class AdsInstaller : ScriptableObjectInstaller<AdsInstaller>
    {
        private enum Type
        {
            None, AdMob, UnityAds
        }

        [Header("Type settings")]
        [SerializeField] private Type _AdsType = Type.None;
        [SerializeField] private AdvertisementSettings _AdMobSettings = null;
        [SerializeField] private AdvertisementSettings _UnityAdsSettings = null;

        [Header("Other settings")]
        [SerializeField] private string _ObjectNameToAddComponent = "";
        [SerializeField] private AdsShowSettings _AdsShowSettings = null;
        [SerializeField] private AdsRewardedByButtonSettings _AdsRewardedByButtonSettings = null;
        
        ////////////////////////////////////////
        
        public override void InstallBindings()
        {
            Container.Bind<AdsShowSettings>().FromInstance(_AdsShowSettings).AsSingle();
            Container.Bind<AdsRewardedByButtonSettings>().FromInstance(_AdsRewardedByButtonSettings).AsSingle();
            
            Container.Bind<AdvertisementSettings>().FromMethod(GetSettings).AsSingle();
            Container.Bind<IAdsManager>().FromMethod(GetAdsManager).AsSingle();
        }

        ////////////////////////////////////////

        private IAdsManager GetAdsManager()
        {
            var obj = GameObject.Find(_ObjectNameToAddComponent);
            if (obj == null)
            {
                UnityEngine.Debug.LogError($"Can't find {_ObjectNameToAddComponent}");
                return new EmptyAdsManager();
            }

            switch (_AdsType)
            {
                case Type.AdMob: return Container.InstantiateComponent<AdMobManager>(obj);
                case Type.UnityAds: return Container.InstantiateComponent<UnityAdsManager>(obj);
                default: return new EmptyAdsManager();
            }
        }

        private AdvertisementSettings GetSettings()
        {
            switch (_AdsType)
            {
                case Type.AdMob: 
                    return _AdMobSettings;
                
                case Type.UnityAds: 
                    return _UnityAdsSettings;
                
                default: 
                    return null;
            }
        }
    }
}