﻿using _Game.Scripts.Game.Ads.Settings;
using _Game.Scripts.Game.Ads.UnityAds.Players;
using _Game.Scripts.Game.Analytics;
using _Game.Scripts.Game.IAP.Management.Saving;
using _Game.Scripts.Game.Statistics;
using Core.Utilities;
using UnityEngine;
using UnityEngine.Advertisements;
using Zenject;

namespace _Game.Scripts.Game.Ads.UnityAds
{
    public class UnityAdsManager : LoggedMonoBehaviour, IAdsManager
    {
        [Inject] private AdvertisementSettings _Settings { get; }
        [Inject] private PurchasingData _PurchasingData { get; }
        [Inject] private IAnalyticsManager _Analytics { get; }
        [Inject] private StatisticsData _Statistics { get; }
        
        public IAdsPlayer banner { get; private set; }
        public IAdsPlayer video { get; private set; }
        public IAdsPlayer rewarded { get; private set; }

        public bool initialized { get; private set; }

        ////////////////////////////////////////////////////

        private void Start()
        {
            Advertisement.Initialize(
                _Settings.idsContainer.GetId(PlatformsUtility.GetCurrentPlatfrom()), 
                _Settings.testMode);

            banner = CreateBannerPlayer(_Settings.bannerSettings);
            video = CreateVideoPlayer(_Settings.videoSettings);
            rewarded = CreateRewardedPlayer(_Settings.rewardedSettings);

            banner.Show();

            if (_PurchasingData.noAds)
                DisableAds();
            else
                _PurchasingData.onNoAdsActivated += DisableAds;

            initialized = true;
        }

        ////////////////////////////////////////////////////

        private IAdsPlayer CreateBannerPlayer(AdsBannerSettings settings)
        {
            var player = settings.enabled ?
                new BannerAdsPlayer(settings) :
                new EmptyAdsPlayer() as IAdsPlayer;

            player.onStart += _Analytics.ShowBanner;
            return player;
        }

        private IAdsPlayer CreateVideoPlayer(AdsVideoSettings settings)
        {
            var player = settings.enabled ?
                new VideoAdsPlayer(settings) :
                new EmptyAdsPlayer() as IAdsPlayer;

            player.onStart += _Analytics.ShowAds;
            return player;
        }

        private IAdsPlayer CreateRewardedPlayer(AdsRewardedSettings settings)
        {
            var player = settings.enabled ?
                new RewardedVideoAdsPlayer(settings) :
                new EmptyAdsPlayer() as IAdsPlayer;

            player.onStart += _Analytics.ShowRewardAds;
            player.onFinish += _Statistics.AddRewardedViewed;
            return player;
        }

        ////////////////////////////////////////////////////

        private void DisableAds()
        {
            banner.Hide();
            video.Hide();

            banner = new EmptyAdsPlayer();
            video = new EmptyAdsPlayer();
        }
    }
}
