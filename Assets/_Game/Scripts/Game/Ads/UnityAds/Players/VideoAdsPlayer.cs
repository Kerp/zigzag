﻿using _Game.Scripts.Game.Ads.Settings;
using UnityEngine.Advertisements;

namespace _Game.Scripts.Game.Ads.UnityAds.Players
{
    class VideoAdsPlayer : UnityAdsPlayer
    {
        private readonly AdsVideoSettings _settings;

        //////////////////////////////////////////////////

        public VideoAdsPlayer(AdsVideoSettings settings) : base()
        {
            _settings = settings;
        }

        //////////////////////////////////////////////////

        protected override bool IsIdMine(string placementId) =>
            _settings.placementId == placementId;

        protected override void StartShow() =>
            Advertisement.Show(_settings.placementId);

        protected override void StopShow() { }
    }
}
