﻿using System;
using UnityEngine;

namespace _Game.Scripts.Game.Ads.Settings
{
    [Serializable]
    public class IdsContainer
    {
        [SerializeField] private IdByPlatform[] _Ids = new IdByPlatform[0];

        public IdByPlatform[] ids => _Ids;

        public string GetId(RuntimePlatform platform)
        {
            foreach (var id in _Ids)
                if (id.platfrom == platform)
                    return id.id;
            
            return "";
        }
    }
}
