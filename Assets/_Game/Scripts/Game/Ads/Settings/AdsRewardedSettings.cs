﻿using System;
using UnityEngine;

namespace _Game.Scripts.Game.Ads.Settings
{
    [Serializable]
    public struct AdsRewardedSettings
    {
        [SerializeField] private bool _Enabled;
        [SerializeField] private string _PlacementId;

        public bool enabled => _Enabled;
        public string placementId => _PlacementId;


        public AdsRewardedSettings(bool enabled, string id)
        {
            _Enabled = enabled;
            _PlacementId = id;
        }
    }
}
