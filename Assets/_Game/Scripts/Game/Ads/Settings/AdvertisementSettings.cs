﻿using UnityEngine;

namespace _Game.Scripts.Game.Ads.Settings
{
    [CreateAssetMenu(fileName = nameof(AdvertisementSettings), menuName = DefinitionsHelper.Parameters + nameof(AdvertisementSettings))]
    public class AdvertisementSettings : ScriptableObject
    {
        [Header("Integration")]
        [SerializeField] private IdsContainer _IdsContainer = null;
        
        [Header("Testing")]
        [SerializeField] private bool _TestMode = false;
        [SerializeField] private string[] _TestDeviceIds = new string[0];

        [Space] [SerializeField] private AdsVideoSettings _VideoSettings = new AdsVideoSettings();
        [Space] [SerializeField] private AdsRewardedSettings _RewardedSettings = new AdsRewardedSettings();
        [Space] [SerializeField] private AdsBannerSettings _BannerSettings = new AdsBannerSettings();

        public IdsContainer idsContainer => _IdsContainer;
        public bool testMode => UnityEngine.Debug.isDebugBuild && _TestMode;
        public string[] testDeviceIds => _TestDeviceIds;

        public AdsVideoSettings videoSettings => _VideoSettings;
        public AdsRewardedSettings rewardedSettings => _RewardedSettings;
        public AdsBannerSettings bannerSettings => _BannerSettings;
    }
}
