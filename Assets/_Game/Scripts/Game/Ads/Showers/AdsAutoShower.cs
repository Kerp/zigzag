﻿using _Game.Scripts.Game.Ads.Settings;
using Core.Extensions;
using Zenject;

namespace _Game.Scripts.Game.Ads.Showers
{
    class AdsAutoShower : MyMonoBehaviour
    {
        [Inject] private IAdsManager _AdsManager { get; }
        [Inject] private AdsShowSettings _Settings { get; }
        
        private uint _gamesPlayed;
        private uint _currentCounter;

        private uint _currentCounterBoundary;

        ////////////////////////////////////////////////////////

        private void Start() =>
            UpdateCurrentBoundary();

        ////////////////////////////////////////////////////////

        public void AddGames()
        {
            _gamesPlayed++;
            _currentCounter++;
            TryShow();
        }

        ////////////////////////////////////////////////////////

        private void TryShow()
        {
            if (NeedShow() == false)
                return;

            ResetCurrentCounter();
            UpdateCurrentBoundary();

            _AdsManager?.video.Show();
        }

        private bool NeedShow() =>
            _currentCounter >= _currentCounterBoundary;

        private void UpdateCurrentBoundary() =>
            _currentCounterBoundary = (uint)_Settings.launchAfterGame.Evaluate(_gamesPlayed);

        private void ResetCurrentCounter() =>
            _currentCounter = 0;
    }
}
