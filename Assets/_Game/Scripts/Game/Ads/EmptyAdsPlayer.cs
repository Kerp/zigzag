﻿using System;

namespace _Game.Scripts.Game.Ads
{
    public class EmptyAdsPlayer : IAdsPlayer
    {
        public event Action onStart;
        public event Action onFinish;
        public event Action onError;
        public event Action onStop;
        public event Action onAdsReady;
        public event Action onAdsReadyChange;
        
        public bool isReady => false;
        public bool isShowing => false;
        
        ////////////////////////////////////////////
        
        public void Show(Action onClose = null, Action onFinished = null, Action onError = null)
        { }

        public void Hide()
        { }
        
        public void Update(float dt) 
        { }
    }
}