﻿using System;
using System.Globalization;
using _Game.Scripts.Game.Data;
using _Game.Scripts.Game.Data.Save;

namespace _Game.Scripts.Game.Ads.Save
{
    public class RewardedCountingData : DataObject<SaveRewardedData>
    {
        #region PRIVATE_VALUES

        private const int DAYS_TO_RESET = 1;

        private bool _useMaxRewardCache = true;

        #endregion // PRIVATE_VALUES

        #region PUBLIC_VALUES

        public int viewsForToday { get; private set; }
        public int viewsCounter { get; private set; }
        public bool needSetViews { get; private set; }
        public bool useMaxReward
        {
            get => _useMaxRewardCache;
            set
            {
                if (_useMaxRewardCache == value)
                    return;

                _useMaxRewardCache = value;
                Save();
            }
        }

        public DateTime startTime { get; private set; }

        public bool isCounterFull => viewsCounter >= viewsForToday;

        #endregion // PUBLIC_VALUES

        #region EVENTS

        public event Action onCounterChanged;
        public event Action onCounterReset;
        public event Action onCounterFull;
        public event Action onViewsForTodayChanged;

        #endregion // EVENTS

        protected override string _save_key { get; } = "ads_rewarded_data";

        ///////////////////////////////////////////////////

        public RewardedCountingData(SaveManager saveManager) : base (saveManager)
        {
            if (IsTimeToReset(startTime))
                ResetCounter();
        }

        ///////////////////////////////////////////////////

        public void IncreaseCounter()
        {
            if (IsTimeToReset(startTime))
                ResetCounter();

            if (isCounterFull)
                return;

            SetCounter(viewsCounter + 1);
        }

        public void SetViewsForToday(int viewsForToday)
        {
            this.viewsForToday = viewsForToday;
            onViewsForTodayChanged?.Invoke();
            InvokeOnChanged();

            needSetViews = false;
            Save();
        }

        ///////////////////////////////////////////////////

        public override SaveRewardedData CreateSaveData() =>
            new SaveRewardedData()
            {
                viewsCounter = viewsCounter,
                viewsForToday = viewsForToday,
                useMaxReward = _useMaxRewardCache,
                startTimeString = startTime.ToString(CultureInfo.InvariantCulture)
            };

        protected override void OnApply(SaveRewardedData saveData, SaveApplyType applyType)
        {
            startTime = ResultDataToApply(saveData.startTime, startTime, applyType);
            viewsCounter = ResultDataToApply(saveData.viewsCounter, viewsCounter, applyType);
            viewsForToday = ResultDataToApply(saveData.viewsForToday, viewsForToday, applyType);

            _useMaxRewardCache = ResultDataToApply(saveData.useMaxReward, _useMaxRewardCache, applyType);

            onCounterChanged?.Invoke();
            onViewsForTodayChanged?.Invoke();

            if (isCounterFull)
                onCounterFull?.Invoke();
        }

        ///////////////////////////////////////////////////

        private void SetCounter(int value)
        {
            viewsCounter = value;
            onCounterChanged?.Invoke();
            InvokeOnChanged();

            if (isCounterFull)
                onCounterFull?.Invoke();

            Save();
        }

        private void ResetCounter()
        {
            startTime = GetCurrentTime();

            SetCounter(0);
            needSetViews = true;
            onCounterReset?.Invoke();
        }

        private bool IsTimeToReset(DateTime startTimeInput)
            => ( GetCurrentTime() - startTimeInput ).TotalDays >= DAYS_TO_RESET;

        private DateTime GetCurrentTime() => DateTime.Now;
    }
}
