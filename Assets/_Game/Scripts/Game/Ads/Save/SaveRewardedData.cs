﻿using System;
using _Game.Scripts.Game.Data;

namespace _Game.Scripts.Game.Ads.Save
{
    [Serializable]
    public class SaveRewardedData : ISaveData
    {
        public int viewsForToday;
        public int viewsCounter;
        public bool useMaxReward = true;
        public string startTimeString = "";

        public DateTime startTime => DateTime.TryParse(startTimeString, out var savedStartTime) ?
                savedStartTime : DateTime.MinValue;
    }
}
