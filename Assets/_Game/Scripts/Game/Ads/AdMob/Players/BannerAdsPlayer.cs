﻿using _Game.Scripts.Game.Ads.Settings;
using GoogleMobileAds.Api;
using UnityEngine.Advertisements;

namespace _Game.Scripts.Game.Ads.AdMob.Players
{
    public class BannerAdsPlayer : AdMobAdsPlayer
    {
        private BannerView _bannerView;
        
        ////////////////////////////////////

        public BannerAdsPlayer(AdsBannerSettings settings, AdRequest.Builder requestBuilder) : base(requestBuilder)
        {
            _bannerView = new BannerView(settings.placementId, 
                GetSize(), GetPosition(settings));
            
            _bannerView.OnAdLoaded += HandleOnLoad;
            _bannerView.OnAdLoaded += HandleOnStart;
            _bannerView.OnAdOpening += HandleOnStart;
            _bannerView.OnAdClosed += HandleOnClose;
            _bannerView.OnAdFailedToLoad += HandleOnError;
            _bannerView.OnAdLeavingApplication += HandleOnLeavingApp;

            LoadAd();
        }
        
        private AdSize GetSize() =>
            AdSize.GetCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(AdSize.FullWidth);

            private AdPosition GetPosition(AdsBannerSettings settings)
        {
            switch (settings.bannerPosition)
            {
                case BannerPosition.CENTER: return AdPosition.Center;
                case BannerPosition.TOP_CENTER: return AdPosition.Top;
                case BannerPosition.TOP_LEFT: return AdPosition.TopLeft;
                case BannerPosition.TOP_RIGHT: return AdPosition.TopRight;
                case BannerPosition.BOTTOM_LEFT: return AdPosition.BottomLeft;
                case BannerPosition.BOTTOM_RIGHT: return AdPosition.BottomRight;
                case BannerPosition.BOTTOM_CENTER: return AdPosition.Bottom;
                default: return AdPosition.Bottom;
            }
        }

        ////////////////////////////////////
        
        protected override void StartShow() =>
            _bannerView.Show();

        protected override void StopShow() =>
            _bannerView.Hide();

        protected sealed override void LoadAd()
        {
            Log("Load ad");
            _bannerView.LoadAd(CreateRequest());
        }
    }
}