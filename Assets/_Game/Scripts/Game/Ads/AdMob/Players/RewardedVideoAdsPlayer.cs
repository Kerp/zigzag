﻿using _Game.Scripts.Game.Ads.Settings;
using GoogleMobileAds.Api;

namespace _Game.Scripts.Game.Ads.AdMob.Players
{
    public class RewardedVideoAdsPlayer : AdMobAdsPlayer
    {
        private readonly string _id;
        
        private RewardedAd _rewardedAd;

        ////////////////////////////////////

        public RewardedVideoAdsPlayer(AdsRewardedSettings settings, AdRequest.Builder requestBuilder) : base(requestBuilder)
        {
            _id = settings.placementId;
            LoadAd();
        }
        
        ////////////////////////////////////

        protected override void StartShow() =>
            _rewardedAd.Show();

        protected override void StopShow()
        { }
        
        protected sealed override void LoadAd()
        {
            CreateNewClient();
            _rewardedAd.LoadAd(CreateRequest());
            
            Log("Load ad");
        }
        
        ////////////////////////////////////

        private void CreateNewClient()
        {
            if (_rewardedAd != null)
            {
                _rewardedAd.OnAdLoaded -= HandleOnLoad;
                _rewardedAd.OnAdOpening -= HandleOnStart;
                _rewardedAd.OnAdClosed -= HandleOnClose;
                _rewardedAd.OnAdFailedToLoad -= HandleOnError;
                _rewardedAd.OnUserEarnedReward -= HandleOnFinish;
                
                Log("Remove old client");
            }
            
            _rewardedAd = new RewardedAd(_id);
            
            _rewardedAd.OnAdLoaded += HandleOnLoad;
            _rewardedAd.OnAdOpening += HandleOnStart;
            _rewardedAd.OnAdClosed += HandleOnClose;
            _rewardedAd.OnAdFailedToLoad += HandleOnError;
            _rewardedAd.OnUserEarnedReward += HandleOnFinish;
            
            Log("Create new client");
        }
    }
}