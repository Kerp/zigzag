﻿namespace _Game.Scripts.Game.Ads
{
    public interface IAdsManager
    {
        IAdsPlayer banner { get; }
        IAdsPlayer video { get; }
        IAdsPlayer rewarded { get; }

        bool initialized { get; }
    }
}