﻿using System;
using UnityEngine;

namespace _Game.Scripts.Game.Analytics.Settings
{
    enum AnalyticsId
    { Empty, AppMetrica, Unity }

    [Serializable]
    struct AnalyticsEnablingStruct
    {
        [SerializeField] private AnalyticsId _AnalyticsId;
        [SerializeField] private bool _Enabled;

        public AnalyticsId analyticsId => _AnalyticsId;
        public bool enabled => _Enabled;


        public AnalyticsEnablingStruct(AnalyticsId id, bool enabled)
        {
            _AnalyticsId = id;
            _Enabled = enabled;
        }
    }
}
