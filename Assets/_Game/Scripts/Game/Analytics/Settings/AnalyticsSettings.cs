﻿using System.Linq;
using UnityEngine;

namespace _Game.Scripts.Game.Analytics.Settings
{
    [CreateAssetMenu(fileName = nameof(AnalyticsSettings), menuName = DefinitionsHelper.Parameters + nameof(AnalyticsSettings))]
    class AnalyticsSettings : ScriptableObject
    {
        // здесь можно сделать кастомный инспектор, который будет
        // сам заполнять _AnalyticsSettings данными из _analytics
        // и блокировать изменение Id. Только Enabled.
        // И можно сделать LayerMask для Work параметров
        [Header("Common configs")]
        [SerializeField] private bool _Enabled = false;
        [SerializeField] private bool _WorkInEditor = false;
        [SerializeField] private bool _WorkInIos = false;
        [SerializeField] private bool _WorkInAndroid = false;
        [SerializeField] private bool _WorkInWindows = false;

        [Header("List")]
        [SerializeField] private AnalyticsEnablingStruct[] _AnalyticsSettings = new AnalyticsEnablingStruct[0];

        public bool enabled => _Enabled &&
            (_WorkInEditor  ||  !Application.isEditor) &&
            (_WorkInIos     ||  Application.platform != RuntimePlatform.IPhonePlayer) &&
            (_WorkInAndroid ||  Application.platform != RuntimePlatform.Android) &&
            (_WorkInWindows ||  Application.platform != RuntimePlatform.WindowsPlayer);

        public AnalyticsId[] enabledSystems => 
            _AnalyticsSettings
            .Where(a => enabled && a.enabled)
            .Select(a => a.analyticsId)
            .ToArray();

        //////////////////////////////////////////////////////////////////

        private void OnEnable()
        {
            CheckAnalyticsMatching();
        }

        private void CheckAnalyticsMatching()
        {
        }
    }
}
