﻿using _Game.Scripts.Game.Data;
using _Game.Scripts.Game.Scoring;
using _Game.Scripts.Game.Social;
using UnityEngine;
using UnityEngine.Purchasing;

namespace _Game.Scripts.Game.Analytics
{
    static class AnalyticsHelper
    {
        public static RestartEventData GetRestartEventData(IGameManager gameManager) =>
            new RestartEventData(gameManager.gamePaused 
                ? RestartGameType.PauseMenu 
                : RestartGameType.GameOver);

        public static GameOverEventData GetGameOverEventData(GameData gameData, IScore score) =>
            new GameOverEventData(gameData.gamesPlayed,
                new AttemptEventData(score.current,
                    score.GetCounter(ScoreType.Distance).current,
                    score.GetCounter(ScoreType.Gems).current,
                    score.GetCounter(ScoreType.Turns).current));

        public static DoubleRewardEventData GetDoubleRewardEventData(int initialGems) =>
            new DoubleRewardEventData(initialGems, initialGems * 2);

        public static FreeGemsEventData GetFreeGemsEventData(int reward) =>
            new FreeGemsEventData(reward);

        public static SetSettingEventData GetSetSettingEventData(bool status) =>
            new SetSettingEventData(status);

        public static SetLanguageEventData GetSetLanguageEventData(SystemLanguage language) =>
            new SetLanguageEventData(language);

        public static GDPREventData GetGDPREventData(string title) =>
            new GDPREventData(title);

        public static SocialEventData GetSocialEventData(SocialParameters.Social socialType) =>
            new SocialEventData(socialType);

        public static LanguageEventData GetLanguageEventData(SystemLanguage language) =>
            new LanguageEventData(language);

        public static ItemEventData GetItemEventData(int groupId, int itemId) =>
            new ItemEventData(groupId.ToString(), itemId.ToString());

        public static PurchaseEventData GetPurchaseEventData(PurchaseEventArgs args)
        {
            var wrapper = new JsonReceiptWrapper(args.purchasedProduct.receipt);

            var receiptData = new ReceiptEventData(
                    wrapper.gpJson,
                    wrapper.gpSignature,
                    args.purchasedProduct.transactionID);

            return new PurchaseEventData(
                args.purchasedProduct.definition.id,
                args.purchasedProduct.metadata.localizedPrice,
                args.purchasedProduct.metadata.isoCurrencyCode,
                1,
                wrapper.payload,
                receiptData);
        }
    }
}
