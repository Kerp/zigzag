﻿
namespace _Game.Scripts.Game.Analytics
{
    public interface IAnalyticsManager
    {
        void StartGame();
        void GameOver(GameOverEventData eventData);
        void RestartGame(RestartEventData eventData);

        void UseDoubleReward(DoubleRewardEventData eventData);
        void UseFreeGems(FreeGemsEventData eventData);

        void ShowRewardAds();
        void ShowAds();
        void ShowBanner();

        void SetSound(SetSettingEventData eventData);
        void SetMusic(SetSettingEventData eventData);
        void SetVibration(SetSettingEventData eventData);
        void SetNotifications(SetSettingEventData eventData);
        void SetLanguage(SetLanguageEventData eventEventData);

        void ReadGDPR(GDPREventData eventData);

        void ViewSocial(SocialEventData eventData);
        void SetLanguage(LanguageEventData eventData);

        void OpenInfo();
        void OpenAchievements();
        void OpenLeaderboard();
        void OpenRate();
        void OpenShare();
        void OpenContact();
        void OpenSkinsShop();
        void OpenIapShop();

        void UnlockItem(ItemEventData eventData);
        void SelectItem(ItemEventData eventData);

        void Purchase(PurchaseEventData eventData);
    }
}
