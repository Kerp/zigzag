﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

namespace _Game.Scripts.Game.Analytics
{
    struct JsonReceiptWrapper
    {
        private Dictionary<string, object> receiptWrapper;
        private Dictionary<string, object> payloadWrapper;

        public string transactionId => GetSafe(receiptWrapper, "TransactionID");
        public string payload => GetSafe(receiptWrapper, "Payload");
        public string store => GetSafe(receiptWrapper, "Store");

        public string gpJson => GetSafe(payloadWrapper, "json");
        public string gpSignature => GetSafe(payloadWrapper, "signature");


        public JsonReceiptWrapper(string receipt)
        {
            receiptWrapper = (Dictionary<string, object>)MiniJson.JsonDecode(receipt) ?? new Dictionary<string, object>();

            payloadWrapper = receiptWrapper != null && receiptWrapper.ContainsKey("Payload") && Application.platform == RuntimePlatform.Android
                ? (Dictionary<string, object>)MiniJson.JsonDecode((string)receiptWrapper["Payload"])
                : new Dictionary<string, object>();
        }


        private string GetSafe(Dictionary<string, object> dict, string key) =>
            dict.TryGetValue(key, out var obj) ? (string)obj : "";
    }
}
