﻿using System.Text;
using _Game.Scripts.Extensions;
using _Game.Scripts.Game.Analytics.Settings;
using _Game.Scripts.Game.Analytics.Systems;
using UnityEngine;

namespace _Game.Scripts.Game.Analytics
{
    public class AnalyticsManager : MonoBehaviour, IAnalyticsManager
    {
        [SerializeField] private AnalyticsSettings _Settings = null;

        private readonly IAnalytics[] _analytics = new IAnalytics[0];

        ///////////////////////////////////////////

        #region CONSTRUCTORS

        private void Awake()
        {
            var stringBuilder = new StringBuilder();

            foreach (var s in _Settings.enabledSystems)
            {
                var analytics = Create(s);
                stringBuilder.Append(analytics.id + ", ");
            }

            UnityEngine.Debug.Log($"Enabled {_analytics.Length} analytics: {stringBuilder}");
        }

        private IAnalytics Create(AnalyticsId analyticsId)
        {
            switch(analyticsId)
            {
                case AnalyticsId.AppMetrica: return new AppMetricaAnalytics();
                case AnalyticsId.Unity: return new UnityAnalytics();
                default: return new EmptyAnalytics();
            }
        }

        #endregion // CONSTRUCTORS

        #region EVENTS

        public void StartGame() => Event("game_start");
        public void GameOver(GameOverEventData eventData) => Event("game_over", eventData);
        public void RestartGame(RestartEventData eventData) => Event("game_restart", eventData);

        public void UseDoubleReward(DoubleRewardEventData eventData) => Event("bonus_double", eventData);
        public void UseFreeGems(FreeGemsEventData eventData) => Event("bonus_free", eventData);

        public void ShowRewardAds() => Event("ads_reward_show");
        public void ShowAds() => Event("ads_video_show");
        public void ShowBanner() => Event("ads_banner_show");

        public void SetSound(SetSettingEventData eventData) => Event("set_sound", eventData);
        public void SetMusic(SetSettingEventData eventData) => Event("set_music", eventData);
        public void SetVibration(SetSettingEventData eventData) => Event("set_vibration", eventData);
        public void SetNotifications(SetSettingEventData eventData) => Event("set_notifications", eventData);
        public void SetLanguage(SetLanguageEventData eventEventData) => Event("set_language", eventEventData);

        public void ReadGDPR(GDPREventData eventData) => Event("read_gdpr", eventData);

        public void ViewSocial(SocialEventData eventData) => Event("view_social", eventData);
        public void SetLanguage(LanguageEventData eventData) => Event("set_language", eventData);

        public void OpenInfo() => Event("open_info");
        public void OpenAchievements() => Event("open_achievements");
        public void OpenLeaderboard() => Event("open_leaderboard");
        public void OpenRate() => Event("open_rate");
        public void OpenShare() => Event("open_share");
        public void OpenContact() => Event("open_contact");
        public void OpenSkinsShop() => Event("open_skins_shop");
        public void OpenIapShop() => Event("open_iap_shop");

        public void UnlockItem(ItemEventData eventData) => Event("item_unlock", eventData);
        public void SelectItem(ItemEventData eventData) => Event("item_select", eventData);

        public void Purchase(PurchaseEventData eventData) => Revenue(eventData);

        #endregion // EVENTS

        #region PRIVATE_METHODS

        private void Event(string eventName) =>
            _analytics.ForEach(a => a.Event(eventName));

        private void Event(string eventName, IEventData eventData) =>
            _analytics.ForEach(a => a.Event(eventName, eventData));

        private void Revenue(IRevenueData revenueData) =>
            _analytics.ForEach(a => a.Revenue(revenueData));

        #endregion // PRIVATE_METHODS
    }
}
