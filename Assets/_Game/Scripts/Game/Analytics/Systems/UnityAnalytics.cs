﻿using System.Collections.Generic;
using _Game.Scripts.Game.Analytics.Settings;
using UnityEngine.Analytics;

namespace _Game.Scripts.Game.Analytics.Systems
{
    class UnityAnalytics : Analytics<IStringObjectEventData, IUnityRevenueData>
    {
        public override AnalyticsId id => AnalyticsId.Unity;

        public UnityAnalytics()
        { }

        public override void Event(string eventName) =>
            AnalyticsEvent.Custom(eventName);

        public override void Event(string eventName, IStringObjectEventData eventData) =>
            AnalyticsEvent.Custom(eventName, eventData.ToStringObjectData());

        public override void Revenue(IUnityRevenueData revenueData)
        {
            var data = revenueData.ToUnityRevenueData();

            AnalyticsEvent.IAPTransaction(data.Payload, data.Price, data.ProductID, 
                transactionId: data.Receipt.TransactionID, 
                eventData: new Dictionary<string, object>()
                {
                    { nameof(data.Quantity), data.Quantity },
                    { nameof(data.Currency), data.Currency },
                    { nameof(data.Receipt.Data), data.Receipt.Data },
                    { nameof(data.Receipt.Signature), data.Receipt.Signature }
                });
        }
    }
}
