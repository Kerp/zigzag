﻿using System;
using System.Collections;
using _Game.Scripts.Game.Objects.Ball;
using UnityEngine;

namespace _Game.Scripts.Game.Automation
{
    [RequireComponent(typeof(Collider))]

    public class DirectionChanger : LoggedMonoBehaviour
    {
        [SerializeField] private bool _UseDelay;
        [SerializeField, Min(0)] private float _Delay;

        private IBall _ball;
        private Coroutine _waitingRoutine;
        private bool _needUseDelay => _UseDelay && _Delay > 0;

        //////////////////////////////////////

        private void OnTriggerEnter(Collider collider)
        {
            if (collider.gameObject.TryGetComponent<IBall>(out var ball) == false)
                return;

            _ball = ball;

            if (_needUseDelay) StartDelay(ChangeDirection);
            else ChangeDirection();
        }

        //////////////////////////////////////

        private void ChangeDirection()
        {
            if (_ball == null)
            {
                Log("Can't change direction - null ball!");
                return;
            }

            _ball.ChangeDirection();
            Log("Change direction");
        }

        //////////////////////////////////////

        private void StartDelay(Action callback)
        {
            StopDelay();
            _waitingRoutine = StartCoroutine(WaitingRoutine(callback));
        }

        private void StopDelay()
        {
            if (_waitingRoutine == null)
                return;

            StopCoroutine(_waitingRoutine);
            _waitingRoutine = null;
            Log("Stop delay");
        }

        private IEnumerator WaitingRoutine(Action callback)
        {
            Log("Start delay");
            yield return new WaitForSeconds(_Delay);
            Log("Finish delay");

            callback?.Invoke();
        }
    }
}