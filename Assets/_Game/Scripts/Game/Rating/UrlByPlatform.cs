﻿using System;
using UnityEngine;

namespace _Game.Scripts.Game.Rating
{
    [Serializable]
    struct UrlByPlatform
    {
        [SerializeField] private RuntimePlatform _Platform;
        [SerializeField] private string _Url;

        public RuntimePlatform Platform => _Platform;
        public string Url => _Url;


        public UrlByPlatform(RuntimePlatform platform, string url)
        {
            _Platform = platform;
            _Url = url;
        }
    }
}
