﻿using Core.Systems.Reset;

namespace _Game.Scripts.Game.Handlers.Probability
{
    interface IProbabilitier : IResetable
    {
        float successProbability { get; }
        bool IsSuccess();
    }
}
