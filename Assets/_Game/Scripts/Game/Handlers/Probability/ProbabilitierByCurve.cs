﻿using UnityEngine;

namespace _Game.Scripts.Game.Handlers.Probability
{
    class ProbabilitierByCurve : IProbabilitierByCurve
    {
        public AnimationCurve successProbability { get; private set; }

        ////////////////////////////////////////////////////

        public ProbabilitierByCurve(AnimationCurve successProbability)
            => this.successProbability = successProbability;

        ////////////////////////////////////////////////////

        public bool IsSuccess(int stepsCompleted)
            => RandomValue() < successProbability.Evaluate(stepsCompleted);

        public void Reset()
        { }

        ////////////////////////////////////////////////////

        private float RandomValue()
            => Random.Range(0f, 100f);
    }
}
