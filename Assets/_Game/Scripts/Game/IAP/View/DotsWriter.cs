﻿using Core.Components;
using Core.Extensions;
using UnityEngine;

namespace _Game.Scripts.Game.IAP.View
{
    class DotsWriter : MyMonoBehaviour
    {
        [SerializeField] private UniversalText _TextComponent = null;
        [SerializeField] private int _DotsCount = 0;

        private string _Output => new string('.', _DotsCount);

        ///////////////////////////////////////////////////

        protected override void OnChecking()
            => CheckField(_TextComponent);

        private void Update()
        {
            _TextComponent?.SetText(_Output);
        }
    }
}