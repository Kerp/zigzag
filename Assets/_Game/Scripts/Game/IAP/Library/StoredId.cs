﻿using System;
using UnityEngine;

namespace _Game.Scripts.Game.IAP.Library
{
    public enum StoreType
    {
        None = 0,
        Google = 1,
        Apple = 2
    }
    
    [Serializable]
    public struct StoredId
    {
        [SerializeField] private StoreType _Store;
        [SerializeField] private string _Id;

        public StoreType StoreType => _Store;

        public string StoreName{
            get{
                switch (_Store){
                    //case StoreType.Apple: return AppleAppStore.Name;
                    //case StoreType.Google: return GooglePlay.Name;
                    default: return "";
                }
            }
        }
        
        public string ID => _Id;


        public StoredId(StoreType store, string id)
        {
            _Store = store;
            _Id = id;
        }
    }
}