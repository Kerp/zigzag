﻿using System;
using UnityEngine;
using UnityEngine.Purchasing;

namespace _Game.Scripts.Game.IAP.Library
{
    [Serializable]
    public struct IAPProduct : I_IAPProduct
    {
        [SerializeField] private string _Name;
        [SerializeField] private StoredId[] _Ids;

        public string Name => _Name;
        public IDs IDs{
            get{
                var result = new IDs();
                foreach (var id in _Ids)
                    result.Add(id.ID, id.StoreName);

                return result;
            }
        }
        public string CurrentId
        {
            get
            {
#if UNITY_ANDROID
                var store = StoreType.Google;
#elif UNITY_IOS
                var store = StoreType.Apple;
#else
                var store = StoryType.None;
#endif

                foreach (var storedId in _Ids)
                    if (storedId.StoreType == store)
                        return storedId.ID;

                return "";
            }
        }


        public bool ContainsId(string id)
        {
            foreach (var storedId in _Ids)
                if (storedId.ID == id)
                    return true;

            return false;
        }


        public IAPProduct(string name)
        {
            _Name = name;
            _Ids = new StoredId[0];
        }
    }
}