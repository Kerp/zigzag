﻿using System.Collections.Generic;
using _Game.Scripts.Game.Analytics;
using _Game.Scripts.Game.IAP.Library;
using _Game.Scripts.Instruments;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;
using Zenject;

namespace _Game.Scripts.Game.IAP
{
    [RequireComponent(typeof(Unique))]
    class IAPManager : MonoBehaviour, I_IAPManager
    {
        #region SERIZLIE_FIELDS

        [Header("Events")]
        [SerializeField] private UnityEvent _OnPurchasingStart = new UnityEvent();
        [SerializeField] private UnityEvent _OnPurchasingEnd = new UnityEvent();
        [SerializeField] private UnityEvent _OnPurchasingError = new UnityEvent();

        [Header("Data")]
        [SerializeField] private IAPLibrary _NonConsumerProducts = null;
        [SerializeField] private IAPLibrary _ConsumerProducts = null;

        #endregion // SERIZLIE_FIELDS

        [Inject] private IAnalyticsManager _analytics { get; }

        private IStoreController s_StoreController;
        private IExtensionProvider s_StoreExtensionProvider;

        public event OnSuccessConsumable onPurchaseConsumable;
        public event OnSuccessNonConsumable onPurchaseNonConsumable;
        public event OnFailedPurchase onPurchaseFailed;

        private string m_currentProductId;

        public I_IAPLibrary nonConsumerProducts => _NonConsumerProducts ?? new IAPLibrary();
        public I_IAPLibrary consumerProducts => _ConsumerProducts ?? new IAPLibrary();
        public IEnumerable<Product> storeProducts => s_StoreController.products.all;

        public bool isInitialized =>
            s_StoreController != null && s_StoreExtensionProvider != null;

        /////////////////////////////////////////////

        #region MONO_BEHAVIOUR

        private void Start()
        {
            InitializePurchasing();
        }

        private void Log(string message) =>
            UnityEngine.Debug.Log($"[{GetType().Name}] {message}");

        private void Error(string message) =>
            UnityEngine.Debug.LogError($"[{GetType().Name}] {message}");

        #endregion // MONO_BEHAVIOUR

        #region INITIALIZING

        public void InitializePurchasing()
        {
            if (isInitialized)
                return;

            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
            AddProducts(ref builder, consumerProducts.Products, ProductType.Consumable);
            AddProducts(ref builder, nonConsumerProducts.Products, ProductType.NonConsumable);

            Log("Start Initialization");
            UnityPurchasing.Initialize(this, builder);
        }

        private void AddProducts(ref ConfigurationBuilder builder, I_IAPProduct[] purchases, ProductType type)
        {
            foreach (var s in purchases) 
                builder.AddProduct(s.CurrentId, type);
        }

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            Log("OnInitialized: PASS");

            s_StoreController = controller;
            s_StoreExtensionProvider = extensions;

            if (s_StoreController == null)
                Error("StoreController is null!");

            if (s_StoreExtensionProvider == null)
                Error("StoreExtensionProvider is null!");
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
            Error("OnInitializeFailed InitializationFailureReason:" + error);
        }

        #endregion // INITIALIZING

        #region BUYING

        public bool CheckBuyState(string id) =>
            isInitialized && s_StoreController.products.WithID(id).hasReceipt;

        public void BuyConsumable(string name)
        {
            if (consumerProducts.ContainsPurchaseItem(name) == false) {
                Error($"'{name}' isn't considered!");
                return;
            }

            if (consumerProducts.TryGetPurchase(name, out var item) == false)
                return;

            BuyProductID(item.CurrentId);
        }

        public void BuyNonConsumable(string name)
        {
            if (nonConsumerProducts.ContainsPurchaseItem(name) == false)
            {
                Error($"'{name}' isn't considered!");
                return;
            }

            if (nonConsumerProducts.TryGetPurchase(name, out var item) == false)
                return;

            if (CheckBuyState(item.CurrentId)) {
                Log($"'{name}' already bought!");
                return;
            }

            BuyProductID(item.CurrentId);
        }

        private void BuyProductID(string productId)
        {
            m_currentProductId = productId;

            if (isInitialized == false) {
                Error("IAP isn't initialised! Can't buy!");
                return;
            }

            var product = s_StoreController.products.WithID(productId);

            if (product?.availableToPurchase != true) {
                Error($"Can't buy '{productId}'");
                OnPurchaseFailed(product, PurchaseFailureReason.ProductUnavailable);
                return;
            }

            InitiatePurchase(product);
            Log($"Initiate Purchase '{productId}'");
        }

        #endregion // BUYING

        #region PURCHASING

        private void InitiatePurchase(Product product)
        {
            if (product == null) {
                Error("Null product to purchase!");
                return;
            }

            _OnPurchasingStart?.Invoke();
            s_StoreController.InitiatePurchase(product);
            Log($"Purchasing product asychronously: '{product.definition.id}'");
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
        {
            if (Validate(args) == false)
            {
                OnProductNonValid(args);
                return PurchaseProcessingResult.Complete;
            }

            if (IsBoughtProductConsumerable(args))
                OnSuccessConsumer(args);
            else if (IsBoughtProductNonConsumerable(args))
                OnSuccessNonConsumer(args);
            else
                OnProductUnrecognized(args);

            _analytics.Purchase(AnalyticsHelper.GetPurchaseEventData(args));
            return PurchaseProcessingResult.Complete;
        }

        private bool IsBoughtProductNonConsumerable(PurchaseEventArgs args) =>
            IsBoughtProductInThatLibrary(args, nonConsumerProducts);

        private bool IsBoughtProductConsumerable(PurchaseEventArgs args) =>
            IsBoughtProductInThatLibrary(args, consumerProducts);

        private bool IsBoughtProductInThatLibrary(PurchaseEventArgs args, I_IAPLibrary library) 
            => library?.ContainsPurchaseItemId(args.purchasedProduct.definition.id) == true;

        protected virtual void OnSuccessConsumer(PurchaseEventArgs args)
        {
            _OnPurchasingEnd?.Invoke();
            onPurchaseConsumable?.Invoke(args);
            Log(args.purchasedProduct.definition.id + " Bought Consumerable!");
        }

        protected virtual void OnSuccessNonConsumer(PurchaseEventArgs args)
        {
            _OnPurchasingEnd?.Invoke();
            onPurchaseNonConsumable?.Invoke(args);
            Log(args.purchasedProduct.definition.id + " Bought NonConsumerable!");
        }

        protected virtual void OnProductNonValid(PurchaseEventArgs args)
        {
            _OnPurchasingError?.Invoke();
            Error($"ProcessPurchase: FAIL. Non valid product: '{args.purchasedProduct.definition.id}'");
        }

        protected virtual void OnProductUnrecognized(PurchaseEventArgs args)
        {
            _OnPurchasingError?.Invoke();
            Error($"ProcessPurchase: FAIL. Unrecognized product: '{args.purchasedProduct.definition.id}'");
        }

        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            _OnPurchasingError?.Invoke();
            onPurchaseFailed?.Invoke(product, failureReason);

            Error($"OnPurchaseFailed: FAIL. " +
                $"Product: '{product.definition.storeSpecificId}', " +
                $"PurchaseFailureReason: {failureReason}");
        }
        #endregion // PURCHASING

        #region VALIDATING

        private bool Validate(PurchaseEventArgs args)
        {
            var isValid = true;

#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS || UNITY_STANDALONE_OSX)

            var validator = new CrossPlatformValidator(GooglePlayTangle.Data(),
                AppleTangle.Data(), Application.identifier);

            try
            {
                var result = validator.Validate(args.purchasedProduct.receipt);
            }
            catch (IAPSecurityException)
            {
                isValid = false;
            }
#endif

            var logMessage = isValid ?
                $"Receipt is valid. Contents: {args.purchasedProduct.receipt}" :
                "Invalid receipt, not unlocking content";

            UnityEngine.Debug.Log(logMessage);
            return isValid;
        }

        #endregion // VALIDATING

        #region RESTORING

        public void RestorePurchases()
        {
            if (!isInitialized) {
                Error("RestorePurchases FAIL. Not initialized.");
                return;
            }

            if (Application.platform == RuntimePlatform.IPhonePlayer ||
                Application.platform == RuntimePlatform.OSXPlayer) {
                Log("RestorePurchases started ...");

                s_StoreExtensionProvider.GetExtension<IAppleExtensions>().RestoreTransactions(
                    result => Log("RestorePurchases continuing: " + result +
                    ". If no further messages, no purchases available to restore."));
            }
            else 
                Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }

        #endregion // RESTORING
    }
}
