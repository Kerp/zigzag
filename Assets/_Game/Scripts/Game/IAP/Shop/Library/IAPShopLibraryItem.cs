﻿using System;
using _Game.Scripts.Game.IAP.Library;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace _Game.Scripts.Game.IAP.Shop.Library
{
    [Serializable]
    struct IAPShopLibraryItem : I_IAPShopLibraryItem
    {
        [SerializeField] private string _NameInProductLibrary;
        [SerializeField] private string _Header;
        [SerializeField] private AssetReferenceSprite _ImageReference;
        [SerializeField] private IAPProductType _Type;
        [SerializeField] private int _Amount;
        [SerializeField] private string _Price;
        [SerializeField] private bool _IsBestOffer;

        public string name => _NameInProductLibrary;
        public string header => _Header;
        public AssetReferenceSprite imageReference => _ImageReference;
        public IAPProductType type => _Type;
        public int amount => _Amount;
        public string price => _Price;
        public bool isBestOffer => _IsBestOffer;

        /////////////////////////////////////////////////////////

        public IAPShopLibraryItem(string name, string header, AssetReferenceSprite imageReference, IAPProductType type, int amount,
            string price, bool isBestOffer)
        {
            _NameInProductLibrary = name;
            _Header = header;
            _ImageReference = imageReference;
            _Type = type;
            _Amount = amount;
            _Price = price;
            _IsBestOffer = isBestOffer;
        }

        /////////////////////////////////////////////////////////

        public override bool Equals(object obj)
        {
            if (obj is IAPShopLibraryItem item)
                return item.name == name;
            return false;
        }

        public override int GetHashCode()
            => name.GetHashCode();
    }
}
