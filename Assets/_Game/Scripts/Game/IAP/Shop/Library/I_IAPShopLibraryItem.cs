﻿using _Game.Scripts.Game.IAP.Library;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace _Game.Scripts.Game.IAP.Shop.Library
{
    interface I_IAPShopLibraryItem
    {
        string name { get; }
        string header { get; }
        AssetReferenceSprite imageReference { get; }
        IAPProductType type { get; }
        int amount { get; }
        string price { get; }
        bool isBestOffer { get; }


        bool Equals(object obj);
        int GetHashCode();
    }
}
