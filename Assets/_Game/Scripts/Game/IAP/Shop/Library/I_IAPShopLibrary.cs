﻿
namespace _Game.Scripts.Game.IAP.Shop.Library
{
    interface I_IAPShopLibrary
    {
        I_IAPShopLibraryItem[] items { get; }

        bool TryGetItem(string name, out I_IAPShopLibraryItem item);
        I_IAPShopLibraryItem GetItem(string name);
        bool Contains(string name);
    }
}
