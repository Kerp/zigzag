﻿using _Game.Scripts.Game.Data;
using _Game.Scripts.Game.IAP.Management;
using _Game.Scripts.Game.IAP.Management.Saving;
using _Game.Scripts.Game.IAP.Shop.Library;
using _Game.Scripts.Game.IAP.Shop.View;
using _Game.Scripts.Game.IAP.Shop.View.Item;
using _Game.Scripts.Game.Statistics;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.IAP.Shop.Installers
{
    class IapShopLibraryInstaller : MonoInstaller<IapShopLibraryInstaller>
    {
        [SerializeField] private Transform _Container = null;
        [SerializeField] private GameObject _ItemPrefab = null;
        [SerializeField] private IAPShopLibrary _ShopLibrary = null;

        [Inject] private GameData _gameData { get; }

        public override void InstallBindings()
        {
            Container.Bind<I_IAPShopLibrary>().To<IAPShopLibrary>().FromInstance(_ShopLibrary).AsSingle();

            Container.Bind<IPurchasesManager>().FromMethod(GetPurchasesManager).AsSingle();

            Container.BindFactory<I_IAPShopItem, IAPShopItemFactory>().FromComponentInNewPrefab(_ItemPrefab).AsSingle();
            Container.Bind<I_IAPShopItemFactory>().To<IAPShopItemFactory>().FromResolve().AsTransient();

            Container.Bind<Transform>().FromInstance(_Container).WhenInjectedInto<IAPShopView>();
            Container.Bind<I_IAPShopView>().To<IAPShopView>().AsSingle();

            Container.Bind<I_IAPShopViewBehaviour>().To<IAPShopViewBehaviour>().FromComponentOn(gameObject).AsSingle();
        }

        private IPurchasesManager GetPurchasesManager()
            => new PurchasesManager(
                Container.Resolve<I_IAPShopLibrary>(),
                GetDiamondsManager(),
                GetNoAdsManager());

        private ItemTypedPurchasesManager GetDiamondsManager()
            => new DiamondsPurchasesManager(
                Container.Resolve<I_IAPManager>(),
                Container.Resolve<I_IAPShopLibrary>(),
                Container.Resolve<StatisticsData>(),
                _gameData);

        private ItemTypedPurchasesManager GetNoAdsManager()
            => new NoAdsPurchasesManager(
                Container.Resolve<I_IAPManager>(),
                Container.Resolve<I_IAPShopLibrary>(),
                Container.Resolve<PurchasingData>(),
                Container.Resolve<StatisticsData>());
    }
}
