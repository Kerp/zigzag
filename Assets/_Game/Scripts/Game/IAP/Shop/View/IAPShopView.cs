﻿using System;
using _Game.Scripts.Game.IAP.Management;
using _Game.Scripts.Game.IAP.Shop.Library;
using _Game.Scripts.Game.IAP.Shop.View.Item;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Game.Scripts.Game.IAP.Shop.View
{
    class IAPShopView : I_IAPShopView
    {
        private readonly Transform _container;
        private readonly I_IAPShopItemFactory _itemsFactory;
        private readonly I_IAPShopLibrary _library;
        private readonly IPurchasesManager _manager;
        private readonly I_IAPManager _IAPManager;

        private bool _initialized { get; set; }
        private I_IAPShopItem[] _items
            => _container.GetComponentsInChildren<I_IAPShopItem>(true);

        public event Action<I_IAPShopItem> onItemClicked;

        /////////////////////////////////////////////////////

        public IAPShopView(Transform container, I_IAPShopLibrary library, IPurchasesManager manager, I_IAPShopItemFactory itemsFactory, I_IAPManager iapManager)
        {
            _library = library;
            _manager = manager;
            _container = container;
            _IAPManager = iapManager;
            _itemsFactory = itemsFactory;
        }

        /////////////////////////////////////////////////////

        #region PUBLIC_METHODS

        public void Initialize()
        {
            if (_initialized)
                return;

            foreach (var item in _library.items)
                if (_manager.IsAvailable(item))
                    InitItem(item);

            _initialized = true;
            UnityEngine.Debug.Log("IAP Shop View Initialized");
        }

        public void Deinitialize()
        {
            if (_initialized == false)
                return;

            foreach (Transform child in _container)
                Object.Destroy(child.gameObject);

            _initialized = false;
            UnityEngine.Debug.Log("IAP Shop View Deinitialized");
        }

        public void Reinitialize()
        {
            if (_initialized == false) {
                Initialize();
                return;
            }

            foreach (var item in _items)
                if (!_manager.IsAvailable(item.data))
                    Object.Destroy(item.gameObject);

            UnityEngine.Debug.Log("IAP Shop View Reinitialized");
        }
        #endregion // PUBLIC_METHODS

        /////////////////////////////////////////////////////

        private void InitItem(I_IAPShopLibraryItem item)
        {
            var itemPrefab = _itemsFactory.Create();

            itemPrefab.onClick += OnItemClicked;
            itemPrefab.gameObject.transform.SetParent(_container);

            itemPrefab.Construct(_IAPManager, item);
        }

        private void OnItemClicked(I_IAPShopItem itemView)
        {
            onItemClicked?.Invoke(itemView);
        }
    }
}
