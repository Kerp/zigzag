﻿using System;
using _Game.Scripts.Game.IAP.Shop.Library;
using Core.Interfaces;

namespace _Game.Scripts.Game.IAP.Shop.View.Item
{
    interface I_IAPShopItem : IGameObjectHost
    {
        I_IAPShopLibraryItem data { get; }
        event Action<I_IAPShopItem> onClick;

        void Construct(I_IAPManager iAPManager, I_IAPShopLibraryItem data);
    }
}
