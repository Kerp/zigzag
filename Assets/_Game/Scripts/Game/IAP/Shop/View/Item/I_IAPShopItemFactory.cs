﻿using Zenject;

namespace _Game.Scripts.Game.IAP.Shop.View.Item
{
    interface I_IAPShopItemFactory : IFactory<I_IAPShopItem>
    {
    }
}
