﻿using System;
using _Game.Scripts.Game.IAP.Shop.View.Item;

namespace _Game.Scripts.Game.IAP.Shop.View
{
    interface I_IAPShopView
    {
        event Action<I_IAPShopItem> onItemClicked;

        void Initialize();
        void Reinitialize();
        void Deinitialize();
    }
}
