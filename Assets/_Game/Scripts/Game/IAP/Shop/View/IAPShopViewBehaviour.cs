﻿using _Game.Scripts.Game.IAP.Library;
using _Game.Scripts.Game.IAP.Management;
using _Game.Scripts.Game.IAP.Shop.Library;
using _Game.Scripts.Game.IAP.Shop.View.Item;
using Core.Extensions;
using Zenject;

namespace _Game.Scripts.Game.IAP.Shop.View
{
    class IAPShopViewBehaviour : MyMonoBehaviour, I_IAPShopViewBehaviour
    {
        [Inject] private IPurchasesManager _Manager { get; }
        [Inject] private I_IAPShopView _LibraryView { get; }

        ///////////////////////////////////////////////////////////////

        private void OnEnable()
        {
            if (_LibraryView == null)
                return;

            _LibraryView.Initialize();
            _LibraryView.onItemClicked += Purchase;
        }

        private void OnDisable()
        {
            if (_LibraryView == null)
                return;

            _LibraryView?.Deinitialize();
            _LibraryView.onItemClicked -= Purchase;
        }

        ///////////////////////////////////////////////////////////////

        private void Purchase(I_IAPShopItem item)
        {
            if (item == null || _Manager == null)
                return;

            SubscribeManager();

            if (_Manager.Buy(item.data) == false)
            {
                UnsubscribeManager();
                return;
            }

            UnityEngine.Debug.Log("Start Purchase");
        }

        private void SubscribeManager()
        {
            if (_Manager == null)
                return;

            _Manager.onSuccess += BuyingSuccessed;
            _Manager.onFailed += BuyingFailed;
        }

        private void UnsubscribeManager()
        {
            if (_Manager == null)
                return;

            _Manager.onSuccess -= BuyingSuccessed;
            _Manager.onFailed -= BuyingFailed;
        }

        private void BuyingSuccessed(I_IAPProduct purItem, I_IAPShopLibraryItem shopItem)
        {
            _LibraryView?.Reinitialize();
            UnsubscribeManager();
        }

        private void BuyingFailed(I_IAPProduct purItem, I_IAPShopLibraryItem shopItem)
        {
            _LibraryView?.Reinitialize();
            UnsubscribeManager();
        }
    }
}
