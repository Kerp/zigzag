﻿using System;
using _Game.Scripts.Game.Data;
using _Game.Scripts.Game.IAP.Library;
using _Game.Scripts.Game.IAP.Shop.Library;
using _Game.Scripts.Game.Statistics;

namespace _Game.Scripts.Game.IAP.Management
{
    class DiamondsPurchasesManager : ItemTypedPurchasesManager
    {
        private GameData _gameData;

        protected override Action<string> _buyMethods => _iapManager.BuyConsumable;
        protected override I_IAPLibrary _library => _iapManager.consumerProducts;

        ///////////////////////////////////////////////////////

        public DiamondsPurchasesManager(I_IAPManager iapManager, I_IAPShopLibrary iapShop, 
            StatisticsData statisticsData, GameData gameData) : base(iapManager, iapShop, statisticsData)
        {
            _gameData = gameData;
        }

        ///////////////////////////////////////////////////////

        public override bool IsAvailable(I_IAPShopLibraryItem shopItem)
            => shopItem.type == IAPProductType.Diamonds && Contains(shopItem);

        ///////////////////////////////////////////////////////

        protected override void OnSuccessPurchasingActions(I_IAPProduct item, I_IAPShopLibraryItem shopItem)
        {
            if (shopItem.type != IAPProductType.Diamonds)
                return;

            _gameData.gemsCash += shopItem.amount;
            _gameData.Save();
        }

        protected override void OnFailedPurchasingActions(I_IAPProduct item, I_IAPShopLibraryItem shopItem)
        {

        }
    }
}
