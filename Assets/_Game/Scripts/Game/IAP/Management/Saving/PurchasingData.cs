﻿using System;
using _Game.Scripts.Game.Data;
using _Game.Scripts.Game.Data.Save;

namespace _Game.Scripts.Game.IAP.Management.Saving
{
    public class PurchasingData : DataObject<SavePurchasingData>
    {
        public bool noAds { get; private set; }
        public event Action onNoAdsActivated;

        protected override string _save_key { get; } = "purchasing_data";

        /////////////////////////////////////////////////////

        public PurchasingData(SaveManager saveManager) : base(saveManager)
        { }

        /////////////////////////////////////////////////////

        public void SetNoAds(bool status)
        {
            if (noAds == status)
                return;

            if (status)
                onNoAdsActivated?.Invoke();

            noAds = status;
            InvokeOnChanged();
            Save();
        }

        /////////////////////////////////////////////////////

        public override SavePurchasingData CreateSaveData() =>
            new SavePurchasingData()
            {
                noAds = noAds
            };

        protected override void OnApply(SavePurchasingData saveData, SaveApplyType applyType)
        {
            noAds = ResultDataToApply(saveData.noAds, noAds, applyType);

            if (noAds)
                onNoAdsActivated?.Invoke();
        }
    }
}
