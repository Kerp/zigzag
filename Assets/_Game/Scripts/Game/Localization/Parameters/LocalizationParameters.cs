﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace _Game.Scripts.Game.Localization.Parameters
{
    [CreateAssetMenu(fileName = nameof(LocalizationParameters), menuName = DefinitionsHelper.Parameters + nameof(LocalizationParameters))]
    public class LocalizationParameters : ScriptableObject
    {
        [SerializeField] private SystemLanguage _DefaultLanguage = SystemLanguage.English;
        [SerializeField] private bool _AutoDetectInitialLanguage = false;
        [SerializeField] private LocalizationData[] _Localizations = new LocalizationData[0];

        public SystemLanguage defaultLanguage => _DefaultLanguage;
        public bool autoDetectInitialLanguage => _AutoDetectInitialLanguage;
        public IEnumerable<LocalizationData> localizations => _Localizations;

        ////////////////////////////////////////////////////////

        public bool IsAvailable(SystemLanguage language)
        {
            foreach (var loc in _Localizations)
                if (loc.language == language)
                    return true;

            return false;
        }
    }

    [Serializable]
    public class LocalizationData
    {
        [SerializeField] private SystemLanguage _Language = SystemLanguage.English;
        [SerializeField] private AssetReferenceSprite _IconReference = null;
        [SerializeField] private AssetReference _FontReference = null;
        [SerializeField] private AssetReference _TextAssetReference = null;

        public SystemLanguage language => _Language;
        public AssetReferenceSprite iconReference => _IconReference;
        public AssetReference fontReference => _FontReference;
        public AssetReference textAssetReference => _TextAssetReference;

        ////////////////////////////////////////////////////////

        private LocalizationData(SystemLanguage language, AssetReferenceSprite iconReference,
            AssetReference fontReference, AssetReference textAssetReference)
        {
            _Language = language;
            _IconReference = iconReference;
            _FontReference = fontReference;
            _TextAssetReference = textAssetReference;
        }
    }
}
