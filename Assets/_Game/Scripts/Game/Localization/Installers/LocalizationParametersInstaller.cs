﻿using _Game.Scripts.Game.Localization.Parameters;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.Localization.Installers
{
    [CreateAssetMenu(fileName = nameof(LocalizationParametersInstaller), menuName = DefinitionsHelper.Installers + nameof(LocalizationParametersInstaller))]
    public class LocalizationParametersInstaller : ScriptableObjectInstaller<LocalizationParametersInstaller>
    {
        [SerializeField] private LocalizationParameters _LocalizationParameters;

        ////////////////////////////////////////////////////////

        public override void InstallBindings()
        {
            Container.Bind<LocalizationParameters>().FromInstance(_LocalizationParameters).AsSingle();
        }
    }
}
