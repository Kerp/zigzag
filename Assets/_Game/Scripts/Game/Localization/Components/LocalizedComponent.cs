﻿using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.Localization.Components
{
    public abstract class LocalizedComponent : LoggedMonoBehaviour
    {
        [Inject] protected LocalizationManager _Manager { get; }

        [SerializeField] protected bool _OverrideLocalization = false;
        [SerializeField] protected SystemLanguage _OverridedLocalization = SystemLanguage.English;

        ////////////////////////////////////////////////////

        private void OnEnable()
        {
            UpdateValue();
            _Manager.onChangeLanguage.AddListener(UpdateValue);
        }

        private void OnDisable()
        {
            _Manager.onChangeLanguage.RemoveListener(UpdateValue);
        }

        ////////////////////////////////////////////////////

        protected void UpdateValue() =>
            SetContent(_OverrideLocalization ? _OverridedLocalization : _Manager.currentLanguage);

        ////////////////////////////////////////////////////

        protected abstract void SetContent(SystemLanguage language);
    }
}
