﻿using TMPro;
using UnityEngine;

namespace _Game.Scripts.Game.Localization.Components
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class LocalizedFont : LocalizedComponent
    {
        private TextMeshProUGUI _textComponentCache;
        protected TextMeshProUGUI _textComponent => _textComponentCache
            ? _textComponentCache
            : _textComponentCache = GetComponent<TextMeshProUGUI>();

        ////////////////////////////////////////////////////

        protected override async void SetContent(SystemLanguage language)
        {
            Log($"Set lang: {language}");
            _textComponent.font = await _Manager.GetFont(language);
        }
    }
}
