﻿using Core.Components;
using UnityEngine;

namespace _Game.Scripts.Game.Localization.Components
{
    [RequireComponent(typeof(CompositeText))]
    public class CompositeLocalizedText : LocalizedText<CompositeText>
    {
        [SerializeField] private int _CompositePart = 0;

        protected override void SetText(string text) =>
            _textComponent.SetPart(text, _CompositePart);
    }
}
