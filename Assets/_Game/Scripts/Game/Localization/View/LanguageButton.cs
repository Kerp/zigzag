using _Game.Scripts.AssetsLoading;
using _Game.Scripts.Extensions;
using Core.Extensions;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace _Game.Scripts.Game.Localization.View
{
    public class LanguageButton : MyMonoBehaviour
    {
        [Header("Components")]
        [SerializeField] private Image _Icon = null;
        [SerializeField] private Button _Button = null;
        [SerializeField] private GameObject _SelectedView = null;

        [Header("Events")]
        [SerializeField] private LanguageButtonUnityEvent _OnClicked = new LanguageButtonUnityEvent();

        private bool? _selected;
        public bool isSelected => _selected == true;
        public SystemLanguage language { get; private set; }

        public LanguageButtonUnityEvent onClicked => _OnClicked;

        private AssetLoaderToImage _loaderCache;
        private AssetLoaderToImage _iconLoader => _loaderCache ??= new AssetLoaderToImage(_Icon);

        /////////////////////////////////////////////////////////

        protected override void OnChecking()
        {
            CheckField(_Icon);
            CheckField(_Button);
        }

        private void OnEnable() =>
            _Button.onClick.AddListener(OnClickAction);

        private void OnDisable() =>
            _Button.onClick.RemoveListener(OnClickAction);

        private void OnDestroy() =>
            _iconLoader.Unload();

        /////////////////////////////////////////////////////////

        public void Init(SystemLanguage language, AssetReferenceSprite iconReference)
        {
            this.language = language;
            _iconLoader.Load(iconReference);
        }

        public void SetSelected(bool status)
        {
            if (status == _selected)
                return;

            _selected = status;
            _SelectedView.SetActive(status);

            Log($"Set selected: {status}");
        }

        /////////////////////////////////////////////////////////

        private void OnClickAction()
        {
            Log("Clicked");
            _OnClicked?.Invoke(this);
        }
    }
}
