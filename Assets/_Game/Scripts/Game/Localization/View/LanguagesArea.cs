using System.Collections.Concurrent;
using _Game.Scripts.Game.Analytics;
using _Game.Scripts.Game.Localization.Parameters;
using _Game.Scripts.Game.Statistics;
using Core.Extensions;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.Localization.View
{
    public class LanguagesArea : MyMonoBehaviour
    {
        [Inject] private Transform _Container { get; }
        [Inject] private StatisticsData _Statistics { get; }
        [Inject] private IAnalyticsManager _Analytics { get; }
        [Inject] private LocalizationManager _Manager { get; }
        [Inject] private LanguageButtonFactory _Factory { get; }


        private readonly ConcurrentQueue<LanguageButton> _buttons = new ConcurrentQueue<LanguageButton>();

        /////////////////////////////////////////////////////////

        private void OnEnable() => Init();
        private void OnDisable() => Deinit();

        /////////////////////////////////////////////////////////

        private void Init()
        {
            foreach (var data in _Manager.localizations)
                _buttons.Enqueue(CreateButton(data));
        }

        private void Deinit()
        {
            while (!_buttons.IsEmpty)
                if (_buttons.TryDequeue(out var button))
                    DestroyButton(button);
        }

        /////////////////////////////////////////////////////////

        private LanguageButton CreateButton(LocalizationData data)
        {
            var button = _Factory.Create();
            button.transform.SetParent(_Container);
            button.onClicked.AddListener(OnButtonClicked);

            button.Init(data.language, data.iconReference);
            button.SetSelected(_Manager.currentLanguage == data.language);

            return button;
        }

        private void DestroyButton(LanguageButton button)
        {
            button.onClicked.RemoveListener(OnButtonClicked);
            Destroy(button.gameObject);
        }

        /////////////////////////////////////////////////////////

        private void OnButtonClicked(LanguageButton button)
        {
            if (button.isSelected)
                return;

            foreach (var b in _buttons)
                b.SetSelected(b == button);

            _Manager.SetLanguage(button.language);

            _Statistics.ChangeLanguage();
            _Analytics.SetLanguage(AnalyticsHelper.GetLanguageEventData(button.language));
        }
    }
}
