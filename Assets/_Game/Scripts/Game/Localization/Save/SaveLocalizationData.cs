﻿using _Game.Scripts.Game.Data;
using UnityEngine;

namespace _Game.Scripts.Game.Localization.Save
{
    public class SaveLocalizationData: ISaveData
    {
        public bool selected;
        public SystemLanguage language;
    }
}
