﻿using _Game.Scripts.Game.Data;
using _Game.Scripts.Game.Localization;
using _Game.Scripts.Game.Notification.Centers;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.Notification
{
    public class NotificationsInstaller : MonoInstaller<NotificationsInstaller>
    {
        [SerializeField] private NotificationParameters _NotificatoParameters = null;
        [SerializeField] private NotificationsChannelParameters _ChannelParameters = null;

        public override void InstallBindings()
        {
            var factory = new NotificationsCentersFactory(Container.Resolve<LocalizationManager>(), _ChannelParameters);
            var center = factory.Create(_NotificatoParameters.useLocalizations);

            Container.Bind<NotificationsCenter>().FromInstance(center).AsSingle();
            Container.Bind<NotificationParameters>().FromInstance(_NotificatoParameters).AsSingle();
            Container.Bind<NotificationsChannelParameters>().FromInstance(_ChannelParameters).AsSingle();
        }
    }
}
