﻿using _Game.Scripts.Game.Localization;

namespace _Game.Scripts.Game.Notification.Centers
{
    public class EmptyNotificationsCenter : NotificationsCenter
    {
        public EmptyNotificationsCenter(LocalizationManager localizationManager, NotificationsChannelParameters channelParameters) : base(localizationManager, channelParameters)
        { }

        public EmptyNotificationsCenter(NotificationsChannelParameters channelParameters) : base(channelParameters)
        { }


        public override void CancelAllNotifications() => Log($"Cancel all notifications");
        public override void ScheduleNotification(NotificationConfig notification) => Log($"Schedule {notification.title}");

        protected override void InitChannel(NotificationsChannelParameters channelParameters) => Log($"Init channel {channelParameters.name}");
    }
}
