﻿using _Game.Scripts.Game.Localization;
using Core;

namespace _Game.Scripts.Game.Notification.Centers
{
    public abstract class NotificationsCenter : LoggedClass
    {
        private readonly LocalizationManager _localizationManager;
        private readonly bool _useLocalization;


        protected NotificationsCenter(LocalizationManager localizationManager, NotificationsChannelParameters channelParameters)
            : this(channelParameters)
        {
            _useLocalization = true;
            _localizationManager = localizationManager;
        }

        protected NotificationsCenter(NotificationsChannelParameters channelParameters) =>
            InitChannel(channelParameters);



        public abstract void CancelAllNotifications();
        public abstract void ScheduleNotification(NotificationConfig notification);

        protected abstract void InitChannel(NotificationsChannelParameters channelParameters);

        protected string HandleStringData(string data) =>
            _useLocalization ? _localizationManager.LocalizeText(data) : data;
    }
}
