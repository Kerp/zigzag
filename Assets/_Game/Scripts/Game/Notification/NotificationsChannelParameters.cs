﻿using Unity.Notifications.Android;
using UnityEngine;

namespace _Game.Scripts.Game.Notification
{
    [CreateAssetMenu(fileName = nameof(NotificationsChannelParameters), menuName = DefinitionsHelper.Parameters + nameof(NotificationsChannelParameters))]
    public class NotificationsChannelParameters : ScriptableObject
    {
        [SerializeField] private string _Id = "";
        [SerializeField] private string _Name = "";
        [SerializeField] private string _Description = "";
        [SerializeField] private Importance _Importance = Importance.Default;

        public string id => _Id;
        public string name => _Name;
        public string description => _Description;
        public Importance Importance => _Importance;
    }
}
