﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace _Game.Scripts.Game.Notification
{
    [CreateAssetMenu(fileName = nameof(NotificationParameters), menuName = DefinitionsHelper.Parameters + nameof(NotificationParameters))]
    public class NotificationParameters : ScriptableObject
    {
        [SerializeField] private bool _UseLocalizations = false;
        [SerializeField] private NotificationConfig[] _Notifications = new NotificationConfig[0];

        public bool useLocalizations => _UseLocalizations;
        public IEnumerable<NotificationConfig> notifications => _Notifications;
    }

    [Serializable]
    public struct NotificationConfig
    {
        [SerializeField] private string _Title;
        [SerializeField] private string _Body;
        [SerializeField] private string _LargeIcon;
        [SerializeField] private string _SmallIcon;
        [SerializeField] private TimeOffset _TimeOffset;

        public string title => _Title;
        public string body => _Body;
        public string largeIcon => _LargeIcon;
        public string smallIcon => _SmallIcon;
        public TimeSpan timeOffset => _TimeOffset.value;

        public NotificationConfig(string title, string body, string largeImage, string smallImage, TimeOffset timeOffset)
        {
            _Title = title;
            _Body = body;
            _LargeIcon = largeImage;
            _SmallIcon = smallImage;
            _TimeOffset = timeOffset;
        }
    }

    [Serializable]
    public struct TimeOffset
    {
        public enum OffsetType
        {
            None,
            Seconds,
            Minutes,
            Hours,
            Days,
            Weeks,
            Months,
            Years
        }

        private const int _DAYS_IN_WEEK = 7;
        private const int _DAYS_IN_MONTH = 31;
        private const int _DAYS_IN_YEAR = 365;

        [SerializeField] private OffsetType _Type;
        [SerializeField] private int _Count;

        public TimeSpan value
        {
            get
            {
                switch (_Type)
                {
                    case OffsetType.Seconds: return TimeSpan.FromSeconds(_Count);
                    case OffsetType.Minutes: return TimeSpan.FromMinutes(_Count);
                    case OffsetType.Hours: return TimeSpan.FromHours(_Count);
                    case OffsetType.Days: return TimeSpan.FromDays(_Count);
                    case OffsetType.Weeks: return TimeSpan.FromDays(_Count * _DAYS_IN_WEEK);
                    case OffsetType.Months: return TimeSpan.FromDays(_Count * _DAYS_IN_MONTH);
                    case OffsetType.Years: return TimeSpan.FromDays(_Count * _DAYS_IN_YEAR);
                    default: return TimeSpan.Zero;
                }
            }
        }

        public TimeOffset(OffsetType type, int count)
        {
            _Type = type;
            _Count = count;
        }
    }
}
