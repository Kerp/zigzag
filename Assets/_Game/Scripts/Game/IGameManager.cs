﻿using _Game.Scripts.Extensions;
using _Game.Scripts.Game.Data;
using _Game.Scripts.Game.Objects.Ball;
using _Game.Scripts.Game.Objects.Generating.Gem;
using _Game.Scripts.Game.Objects.Generating.Platform;
using Cinemachine;
using Core.Extensions;
using Core.Modules;
using UnityEngine.Events;

namespace _Game.Scripts.Game
{
    public interface IGameManager
    {
        IBall ball { get; }
        CinemachineVirtualCamera virtualCamera { get; }
        PlatformManager platformManager { get; }
        GemManager gemManager { get; }

        GameData gameData { get; }
        ITimeScaleManager timeScale { get; }

        UnityEvent onGameLaunched { get; set; }
        UnityEvent onGameStart { get; set; }
        UnityEvent onGameRestart { get; set; }
        UnityEvent onGamePaused { get; set; }
        UnityEvent onGameUnpaused { get; set; }
        UnityEvent onGameOver { get; set; }
        IntUnityEvent onScoreChanged { get; set; }
        IntUnityEvent onCurrentGemsChanged { get; set; }

        bool gamePaused { get; }
        bool gameStarted { get; }


        void StartGame();
        void EndGame();
        void SetPause(bool status);
        void Restart();
        void Quit();
    }
}