﻿using UnityEngine;

namespace _Game.Scripts.Game.Social
{
    [CreateAssetMenu(fileName = nameof(SocialParameters), menuName = DefinitionsHelper.Parameters + nameof(SocialParameters))]
    public class SocialParameters : ScriptableObject
    {
        [SerializeField] private string _Facebook;
        [SerializeField] private string _Instagram;
        [SerializeField] private string _Linkedin;
        [SerializeField] private string _VK;

        public string facebook => _Facebook;
        public string instagram => _Instagram;
        public string linkedin => _Linkedin;
        public string vk => _VK;

        ////////////////////////////////////////

        public string GetLink(Social social)
        {
            switch (social)
            {
                case Social.Facebook: return _Facebook;
                case Social.Instagram: return _Instagram;
                case Social.Linkedin: return _Linkedin;
                case Social.VK: return _VK;
                default: return "";
            }
        }

        ////////////////////////////////////////

        public enum Social
        {
            Facebook,
            Instagram,
            Linkedin,
            VK
        }
    }
}
