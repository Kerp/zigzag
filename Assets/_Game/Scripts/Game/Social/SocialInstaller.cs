﻿using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.Social
{
    [CreateAssetMenu(fileName = nameof(SocialInstaller), menuName = DefinitionsHelper.Installers + nameof(SocialInstaller))]
    public class SocialInstaller : ScriptableObjectInstaller<SocialInstaller>
    {
        [SerializeField] private SocialParameters _Social;

        public override void InstallBindings()
        {
            Container.Bind<SocialParameters>().FromInstance(_Social).AsSingle();
        }
    }
}
