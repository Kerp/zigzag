﻿using System;
using _Game.Scripts.Game.Localization.View;
using UnityEngine.Events;

namespace _Game.Scripts.Extensions
{
    [Serializable]
    public class LanguageButtonUnityEvent : UnityEvent<LanguageButton>
    { }
}
