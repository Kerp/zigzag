﻿using System.Threading.Tasks;

namespace _Game.Scripts.Extensions
{
    public static class TaskExtensions
    {
        public static async Task<TOut> Convert<TIn, TOut>(this Task<TIn> task) where TOut : class
        {
            var result = await task;
            return result as TOut;
        }
    }
}
