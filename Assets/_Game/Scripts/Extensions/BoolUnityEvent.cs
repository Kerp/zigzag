﻿using System;
using UnityEngine.Events;

namespace _Game.Scripts.Extensions
{
    [Serializable]
    public class BoolUnityEvent : UnityEvent<bool>
    { }
}
