﻿using Zenject;
using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using UnityEngine.SceneManagement;
using System.Linq;
using _Game.Scripts.Game;

public class MainSceneTestFixture : SceneTestFixture
{
    protected IGameManager _gameManager { get; set; }

    private const string SCENE_NAME = "MainScene";

    protected virtual IEnumerator LoadMainScene()
    {
        yield return LoadScene(SCENE_NAME);
        _gameManager = SceneManager.GetActiveScene().GetRootGameObjects().First().GetComponent<IGameManager>();
    }
}
