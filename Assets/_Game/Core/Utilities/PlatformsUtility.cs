﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Core.Utilities
{
    public static class PlatformsUtility
    {
#if UNITY_EDITOR
        private static Dictionary<RuntimePlatform, BuildTarget> m_Convertions
            = new Dictionary<RuntimePlatform, BuildTarget>() {
                { RuntimePlatform.Android, BuildTarget.Android },
                { RuntimePlatform.IPhonePlayer, BuildTarget.iOS },
                { RuntimePlatform.WindowsPlayer, BuildTarget.StandaloneWindows },
                { RuntimePlatform.OSXPlayer, BuildTarget.StandaloneOSX }
            };
        public static Dictionary<BuildTarget, string> platfromsEditor
            => platformsRuntime.ToDictionary(v => m_Convertions[v.Key], v => v.Value);
#endif

        public static Dictionary<RuntimePlatform, string> platformsRuntime
            => new Dictionary<RuntimePlatform, string>() {
                 { RuntimePlatform.Android, "Android" },
                { RuntimePlatform.IPhonePlayer, "iOS" },
                { RuntimePlatform.WindowsPlayer, "Windows" },
                { RuntimePlatform.OSXPlayer, "OSX" }
            };

        /////////////////////////////////////////////////////////////////

        public static RuntimePlatform GetCurrentPlatfrom()
        {
#if UNITY_ANDROID
            return RuntimePlatform.Android;
#elif UNITY_IOS
            return RuntimePlatform.IPhonePlayer;
#elif UNITY_STANDALONE_WIN
            return RuntimePlatform.WindowsPlayer;
#elif UNITY_STANDALONE_OSX
            return RuntimePlatform.OSXPlayer;
#elif UNITY_STANDALONE_WIN
            return RuntimePlatform.WindowsEditor;
#endif
        }

        public static string GetCurrentPlatformFolderName()
            => GetPlatfromFolderName(GetCurrentPlatfrom());

        public static string GetPlatfromFolderName(RuntimePlatform platfrom)
        {
            if (platformsRuntime.TryGetValue(platfrom, out var folder))
                return folder;

            return "";
        }

#if UNITY_EDITOR
        public static string GetPlatfromFolderName(BuildTarget platfrom)
        {
            if (platfromsEditor.TryGetValue(platfrom, out var folder))
                return folder;

            return "";
        }
#endif
    }
}
