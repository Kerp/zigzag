﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Core
{
    public class CacheQueue<T>
    {
        private Queue<T> _queue;

        private byte MIN_CACHE_SIZE = 1;

        public byte count { get; private set; }
        public byte cacheSize { get; }

        public byte average =>
            (byte) (_queue.Sum(t => (float)(object)t) / count);
        
        //////////////////////////////////////////////////////////////////
        
        public CacheQueue()
        {
            _queue = new Queue<T>();
            cacheSize = MIN_CACHE_SIZE;
            count = 0;
        }

        public CacheQueue(byte cacheSize) : this() =>
            this.cacheSize = (byte)Mathf.Max(MIN_CACHE_SIZE, cacheSize);

        //////////////////////////////////////////////////////////////////

        public void Enqueue(T value)
        {
            _queue.Enqueue(value);
            count++;
            
            CorrectQueueLength();
        }

        public T Dequeue()
        {
            if (count == 0)
                return default;
            
            count--;
            return _queue.Dequeue();
        }

        public void Clear()
        {
            _queue.Clear();
            count = 0;
        }

        //////////////////////////////////////////////////////////////////

        private void CorrectQueueLength()
        {
            while (count > cacheSize)
                Dequeue();
        }
    }
}