﻿using System;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Core.Structs.Weights
{
    [Serializable]
    public struct WeightedValueCollectionInt : IWeightedValueCollection<int>
    {
        [SerializeField] private WeightedValueInt[] _Values;

        public int[] values => _Values.Select(v => v.value).ToArray();
        public float fullWeight => _Values.Sum(v => v.weight);

        public int randomValue
        {
            get
            {
                var weight = Random.Range(0, fullWeight);
                var sum = 0f;

                foreach(var item in _Values)
                {
                    sum += item.weight;
                    if (sum >= weight)
                        return item.value; 
                }

                return _Values.LastOrDefault().value;
            }
        }

        public int minValue => _Values.Min(v => v.value);
        public int maxValue => _Values.Max(v => v.value);


        public WeightedValueCollectionInt(WeightedValueInt[] values) =>
            _Values = values;
    }
}
