﻿namespace Core.Structs.Weights
{
    public interface IWeightedValueCollection<T>
    {
        T[] values { get; }
        float fullWeight { get; }

        T randomValue { get; }
        T minValue { get; }
        T maxValue { get; }
    }
}
