﻿namespace Core.Structs.Weights
{
    public interface IWeightedValue<T>
    {
        T value { get; }
        float weight { get; }
    }
}
