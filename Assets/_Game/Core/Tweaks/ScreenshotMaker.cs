﻿using System;
using System.Collections;
using System.IO;

using UnityEngine;

namespace Core
{
    public class ScreenshotMaker : MonoBehaviour
    {
        [SerializeField] private string _ScreensPath = "";
        [SerializeField] private KeyCode _KeyToCapture = KeyCode.None;
        [SerializeField] private GameObject[] _ObjectsToDisable = new GameObject[0];

        private bool _disabled = false;
        private bool[] _needToEnableBack;

        ////////////////////////////////////////////////////

        #region UNITY_EDITOR

        private void Update()
        {
            if (Input.GetKeyDown(_KeyToCapture))
                MakeScreenhoot();
        }

        #endregion // UNITY_EDITOR

        ////////////////////////////////////////////////////

        [ContextMenu("Make screenshot")]
        public void MakeScreenhoot() =>
            StartCoroutine(MakingScreenshot());

        ////////////////////////////////////////////////////

        private IEnumerator MakingScreenshot()
        {
            DisableObjects();

            yield return new WaitForEndOfFrame();

            var screenshotName = Path.Combine(_ScreensPath, DateTime.Now.ToString().Replace(".", "_").Replace(":", "_"));
            ScreenCapture.CaptureScreenshot(screenshotName + ".png");
            Debug.Log("Screenshot has been made");

            EnableObjectsBack();
        }

        private void DisableObjects()
        {
            _needToEnableBack = new bool[_ObjectsToDisable.Length];

            for (var i = 0; i < _ObjectsToDisable.Length; i++)
            {
                _needToEnableBack[i] = false;

                if (_ObjectsToDisable[i] != null && _ObjectsToDisable[i].activeSelf == true)
                {
                    _needToEnableBack[i] = true;
                    _ObjectsToDisable[i].SetActive(false);

                }
            }

            _disabled = true;
        }

        private void EnableObjectsBack()
        {
            if (_disabled == false)
                return;

            for (var i = 0; i < _ObjectsToDisable.Length; i++)
            {
                if (_needToEnableBack[i])
                    _ObjectsToDisable[i].SetActive(true);
            }

            _disabled = false;
        }
    }
}
