﻿using UnityEngine;

namespace Core.ScriptableObjects
{
    [CreateAssetMenu(fileName = "SpeedConfigs", menuName = "Configs/Speed")]
    public class SpeedConfigs : ScriptableObject, ISpeedConfigs
    {
        [SerializeField] private float _StartSpeed = 0f;
        [SerializeField] private float _MaxSpeed = 0f;
        [SerializeField] private float _Acceleration = 0f;
        [SerializeField] private bool _NavMesh = false;

        ////////////////////////////////////////////////////

        public float startSpeed => _StartSpeed;
        public float maxSpeed => _MaxSpeed;
        public float acceleration => _Acceleration;
        public bool navMesh => _NavMesh;
    }
}
