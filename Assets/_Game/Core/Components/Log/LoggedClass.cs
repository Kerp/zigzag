﻿namespace Core
{
    public class LoggedClass
    {
        private string _source => GetType().Name;


        protected void Log(string message) => Logging.Log(_source, message);
        protected void Error(string message) => Logging.Error(_source, message);
        protected void Warning(string message) => Logging.Warning(_source, message);

        protected void NullReferenceExeption(string message) => Logging.NullReferenceException(_source, message);
    }
}