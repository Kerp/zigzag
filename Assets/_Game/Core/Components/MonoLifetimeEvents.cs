﻿using UnityEngine;
using UnityEngine.Events;

namespace Core.Components
{
    public class MonoLifetimeEvents : MonoBehaviour
    {
        public UnityEvent onAwake;
        public UnityEvent onStart;
        public UnityEvent onDestroy;

        public UnityEvent onEnable;
        public UnityEvent onDisable;

        //////////////////////////////////////////////////

        private void Awake() => onAwake?.Invoke();
        private void Start() => onStart?.Invoke();
        private void OnDestroy() => onDestroy?.Invoke();

        private void OnEnable() => onEnable?.Invoke();
        private void OnDisable() => onDisable?.Invoke();
    }
}
