﻿using System.Collections.Generic;
using System.Linq;
using Core.Components.UI.Screens;
using Core.Extensions;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Components.UI
{
    public class UIManager : MyMonoBehaviour, IUIManager
    {
        [SerializeField] private UnityEvent _OnFirstScreenOpened = new UnityEvent();
        [SerializeField] private UnityEvent _OnLastScreenClosed = new UnityEvent();
        [SerializeField] private UnityEvent _OnScreenOpened = new UnityEvent();
        [SerializeField] private UnityEvent _OnScreenClosed = new UnityEvent();

        private IScreen[] _screens { get; set; }
            = new IScreen[0];

        private LinkedList<IScreen> _screensQueue = new LinkedList<IScreen>();

        public UnityEvent onFirstScreenOpened {
            get => _OnFirstScreenOpened;
            set => _OnFirstScreenOpened = value;
        }
        public UnityEvent onLastScreenClosed {
            get => _OnLastScreenClosed;
            set => _OnLastScreenClosed = value;
        }
        public UnityEvent onScreenOpened {
            get => _OnScreenOpened;
            set => _OnScreenOpened = value;
        }
        public UnityEvent onScreenClosed {
            get => _OnScreenClosed;
            set => _OnScreenClosed = value;
        }

        //////////////////////////////////////////////////////

        private void Start()
            => Initialize();

        //////////////////////////////////////////////////////

        private void Initialize()
        {
            _screens = GetComponentsInChildren<IScreen>(true);

            foreach (var screen in _screens) {
                screen.onOpened.AddListener(() => OnScreenOpened(screen));
                screen.onClosed.AddListener(() => OnScreenClosed(screen));
                screen.onHided.AddListener(() => OnScreenHided(screen));

                if (screen.opened)
                    _screensQueue.AddLast(screen);
            }
        }

        //////////////////////////////////////////////////////

        private void OnScreenOpened(IScreen screen)
        {
            if (screen.countedAsFirst && !_screensQueue.Any(s => s.countedAsFirst))
                _OnFirstScreenOpened?.Invoke();

            _OnScreenOpened?.Invoke();

            if (TryFindFromLast(screen, out var node))
                _screensQueue.Remove(node);

            _screensQueue.AddLast(screen);

            HideExcept(screen);
        }

        private void OnScreenClosed(IScreen screen)
        {
            OnScreenHided(screen);

            if (TryFindFromLast(screen, out var node))
            {
                var prevScreen = node.Previous?.Value;
                _screensQueue.Remove(node);

                prevScreen?.Open();
            }
        }

        private void OnScreenHided(IScreen screen)
        {
            if (screen.countedAsLast && _screensQueue.Count(s => s.countedAsLast) == 1)
                _OnLastScreenClosed?.Invoke();

            _OnScreenClosed?.Invoke();
        }

        //////////////////////////////////////////////////////

        public IScreen Open(ScreenType screenType)
        {
            var screen = GetScreen(screenType);
            screen?.Open();

            return screen;
        }

        [ContextMenu("Close All")]
        public void CloseAll()
            => CloseExcept();

        public bool ContainsInQueue(ScreenType type) =>
            _screensQueue.Any(s => s.type == type);

        //////////////////////////////////////////////////////

        private IScreen GetScreen(ScreenType screenType)
        {
            var result = _screens.FirstOrDefault(s => s.type == screenType);

            if (result == null)
                Logging.Error(_source, $"Can' find screen of type {screenType:F}");

            return result;
        }

        private void CloseExcept(params IScreen[] exceptions)
        {
            foreach (var screen in _screens.Except(exceptions).ToArray())
                screen.Close();
        }

        private void HideExcept(params IScreen[] exceptions)
        {
            foreach (var screen in _screensQueue.Except(exceptions).ToArray())
                screen.Hide();
        }

        private bool TryFindFromLast(IScreen searchingValue, out LinkedListNode<IScreen> resultNode)
        {
            resultNode = _screensQueue.Last;

            while (resultNode?.Value != null && resultNode.Value != searchingValue)
                resultNode = resultNode.Previous;

            return resultNode?.Value != null;
        }
    }
}
