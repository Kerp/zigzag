﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Components.UI.Views
{
    class ViewByCoroutine : IView
    {
        public UnityAction onPlayOpenStarted { get; set; }
        public UnityAction onPlayOpenFinished { get; set; }
        public UnityAction onPlayCloseStarted { get; set; }
        public UnityAction onPlayCloseFinished { get; set; }

        private readonly CanvasGroup _canvasGroup;
        private readonly MonoBehaviour _host;

        private Coroutine _coroutine { get; set; }

        private const float _DISABLE_ALPHA = 0F;
        private const float _ENABLE_ALPHA = 1F;
        private const float _DURATION = 0.25f;

        //////////////////////////////////////////////////////

        public ViewByCoroutine(MonoBehaviour host, CanvasGroup canvasGroup)
        {
            _host = host;
            _canvasGroup = canvasGroup;
        }

        //////////////////////////////////////////////////////

        public void PlayOpen()
            => Play(true, onPlayOpenStarted, onPlayOpenFinished);

        public void PlayClose()
            => Play(false, onPlayCloseStarted, onPlayCloseFinished);

        //////////////////////////////////////////////////////

        private void Play(bool status, UnityAction startCallback, UnityAction endCallback)
        {
            if (_coroutine != null)
                _host?.StopCoroutine(_coroutine);

            startCallback?.Invoke();

            if (_host && _canvasGroup)
                _coroutine = _host?.StartCoroutine(Playing(
                    _canvasGroup, status, 
                    () => endCallback?.Invoke()));
            else
                endCallback?.Invoke();
        }

        private IEnumerator Playing(CanvasGroup canvasGroup, bool status, Action callback = null)
        {
            var time = _DURATION;
            var startAlpha = status ? _DISABLE_ALPHA : _ENABLE_ALPHA;
            var targetAlpha = status ? _ENABLE_ALPHA : _DISABLE_ALPHA;

            for (var t = 0f; t < time; t += Time.unscaledDeltaTime) {
                canvasGroup.alpha = Mathf.Lerp(startAlpha, targetAlpha, t / time);
                yield return null;
            }

            canvasGroup.alpha = targetAlpha;
            callback?.Invoke();
        }
    }
}
