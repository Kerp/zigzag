﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Core
{
    [RequireComponent(typeof(Button))]
    public class VisibleButtonClickRestrictor : LoggedMonoBehaviour
    {
        [SerializeField] private LifecycleType _WorksOn = LifecycleType.None;
        [SerializeField, Min(0)] private float _EnableDelay = 0f;
        
        private Button _buttonCache;
        private Coroutine _coroutineCache;
        
        ////////////////////////////////////
        
        private void OnBecameVisible()
        {
            if (!_WorksOn.HasFlag(LifecycleType.Visible))
                return;

            Log("OnBecameVisible");
            WorkOn();
        }

        private void OnBecameInvisible()
        {
            if (!_WorksOn.HasFlag(LifecycleType.Visible))
                return;

            Log("OnBecameInvisible");
            WorkOff();
        }
        
        private void OnEnable()
        {
            if (!_WorksOn.HasFlag(LifecycleType.Enabling))
                return;

            Log("OnEnable");
            WorkOn();
        }

        private void OnDisable()
        {
            if (!_WorksOn.HasFlag(LifecycleType.Enabling))
                return;

            Log("OnDisable");
            WorkOff();
        }

        private void OnDestroy() =>
            StopCoroutine();

        ////////////////////////////////////

        private void WorkOn()
        {
            if (_EnableDelay > 0)
            {
                SetInteractable(false);
                StartCoroutine(() => SetInteractable(true));
            }
            else
                SetInteractable(true);
        }

        private void WorkOff()
        {
            StopCoroutine();
            SetInteractable(false);
        }
        
        private void SetInteractable(bool status)
        {
            GetButton().interactable = status;
            Log($"Set interactable: {status}");
        }

        private Button GetButton()
        {
            if (_buttonCache == null)
                _buttonCache = GetComponent<Button>();

            return _buttonCache;
        }

        ////////////////////////////////////

        private void StartCoroutine(Action callback)
        {
            StopCoroutine();
            _coroutineCache = StartCoroutine(Waiting(callback));
            Log("Start coroutine");
        }
        
        private void StopCoroutine()
        {
            if (_coroutineCache != null)
                StopCoroutine(_coroutineCache);

            _coroutineCache = null;
            Log("Stop coroutine");
        }

        private IEnumerator Waiting(Action callback)
        {
            yield return new WaitForSecondsRealtime(_EnableDelay);
            Log("Coroutine callback");
            callback?.Invoke();
        }
        
        ////////////////////////////////////

        [Flags]
        private enum LifecycleType
        {
            None = 0,
            Visible = 1, 
            Enabling = 2
        }
    }
}