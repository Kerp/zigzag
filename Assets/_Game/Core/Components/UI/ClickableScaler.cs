﻿using UnityEngine;

namespace Core
{
    [RequireComponent(typeof(RectTransform))]
    public class ClickableScaler : ClickableStateChanger
    {
        private Vector3 _initialScale;

        ////////////////////////////////////////////

        protected override void CacheState()
        {
            _initialScale = GetTransform().localScale;
            Log($"Cache scale {_initialScale}");
        }

        protected override void ResetState()
        {
            SetScale(_initialScale);
            Log($"Reset scale to {_initialScale}");
        }

        protected override void SetClickState() =>
            SetScale(_initialScale * _ClickStateMultiplier);

        ////////////////////////////////////////////

        private void SetScale(Vector3 scale)
        {
            GetTransform().sizeDelta = scale;
            Log($"Set scale {scale}");
        }
    }
}