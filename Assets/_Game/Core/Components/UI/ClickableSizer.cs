﻿using UnityEngine;

namespace Core
{
    [RequireComponent(typeof(RectTransform))]
    public class ClickableSizer : ClickableStateChanger
    {
        private Vector2 _initialSize;

        ////////////////////////////////////////////

        protected override void CacheState()
        {
            _initialSize = GetTransform().sizeDelta;
            Log($"Cache size {_initialSize}");
        }

        protected override void ResetState()
        {
            SetSize(_initialSize);
            Log($"Reset size to {_initialSize}");
        }

        protected override void SetClickState() =>
            SetSize(_initialSize * _ClickStateMultiplier);
        
        ////////////////////////////////////////////

        private void SetSize(Vector2 size)
        {
            GetTransform().sizeDelta = size;
            Log($"Set size to {size}");
        }
    }
}