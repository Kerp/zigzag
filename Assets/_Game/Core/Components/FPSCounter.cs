﻿using Core.Extensions;
using UnityEngine;

namespace Core
{
    public class FPSCounter : MonoBehaviour
    {
        [Min(0)]
        [SerializeField] private byte _ValuesInCache;

        [Header("Events")]
        [SerializeField] private IntUnityEvent _OnChanged;

        private CacheQueue<float> _queueValues { get; set; }

        public int fps { get; private set; }
        public IntUnityEvent onChanged {
            get => _OnChanged;
            set => _OnChanged = value;
        }

        /////////////////////////////////////////////////
        
        private void OnEnable()
        {
            _queueValues = new CacheQueue<float>(_ValuesInCache);
        }

        private void OnDisable()
        {
            _queueValues.Clear();
        }
        
        private void Update()
        {
            if (enabled == false)
                return;

            _queueValues.Enqueue(currentValue);
            SetFps(currentFps);
        }
        
        /////////////////////////////////////////////////

        private float currentValue =>
            1f / Time.unscaledDeltaTime;
        
        private byte currentFps =>
            _queueValues.average;

        private void SetFps(byte value)
        {
            if (value == fps)
                return;

            fps = value;
            onChanged?.Invoke(fps);
        }
    }
}