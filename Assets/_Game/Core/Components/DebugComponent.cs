﻿using UnityEngine;

namespace Core
{
    public class DebugComponent : MonoBehaviour
    {
        private void OnEnable()
        {
            if (!Debug.isDebugBuild)
                gameObject.SetActive(false);
        }
    }
}