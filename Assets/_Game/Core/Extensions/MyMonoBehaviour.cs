﻿using UnityEngine;

namespace Core.Extensions
{
    public class MyMonoBehaviour : LoggedMonoBehaviour
    {
        protected virtual string _source => GetType().ToString();


        private void Awake() => OnAttaching();
        private void Start() => OnChecking();

        protected virtual void OnAttaching() { }
        protected virtual void OnChecking() { }


        protected bool TryAttach<T>(ref T component) where T : class
        {
            AttachSafe(ref component);

            if (component == null)
                Logging.Error(_source, $"{typeof(T)} isn't attached!");

            return component != null;
        }

        protected T Attach<T>(ref T component) where T : class
        {
            AttachSafe(ref component);

            if (component == null)
                Logging.Error(_source, $"{typeof(T)} isn't attached!");

            return component;
        }

        protected bool AttachSafe<T>(ref T component) where T : class
        {
            if (component == null)
            {
                foreach (var t in GetComponents<T>())
                    if (t as Object != this)
                    {
                        component = t;
                        break;
                    }
            }
            return component != null;
        }

        protected void CheckField<T>(T component)
        {
            if (component == null)
                Logging.Error(_source, $"{typeof(T)} isn't attached!");
        }
    }
}
