﻿namespace Core.Modules
{
    internal class EmptyVibrator : LoggedClass, IVibrator
    {
        public void Vibrate(long milliseconds = 250) =>
            Log($"Vibrate: {milliseconds}");

        public void Stop() =>
            Log("Stop");
    }
}