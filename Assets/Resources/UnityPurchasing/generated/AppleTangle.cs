#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("3tu33Y/c5t3k6+646ev+77i+3P7BzY6In5mEi4SOjJmIzZ2CgYSOlG/s7evkx2ulaxqOiejs3Wwf3cfrgYjNpIOOw9zL3cnr7rjp5v7wrJ2dgYjNroifmYSLhI6MmYSCg82smOvd4uvuuPD+7OwS6ejd7uzsEt3w5cbr7Ojo6u/s+/OFmZmdntfCwpqkNZty3vmITJp5JMDv7uzt7E5v7MOtSxqqoJLls93y6+648M7p9d37UxmedgM/ieImlKLZNU/TFJUShiXp6/7vuL7c/t386+646ef+56ydnVjXQBni4+1/5lzM+8OZONHgNo/76O3ub+zi7d1v7Ofvb+zs7Ql8ROTQy4rNZ96HGuBvIjMGTsIUvoe2ib+IgYSMg46IzYKDzZmFhJ7Njoif3fzr7rjp5/7nrJ2dgYjNpIOOw9yKYuVZzRomQcHNgp1b0uzdYVquIonYzvim+LTwXnkaG3FzIr1XLLW96+648OPp++n5xj2Eqnmb5BMZhmCSrEV1FDwni3HJhvw9TlYJ9scu8p2BiM2/goKZza6s3fP64N3b3dnfLY7emhrX6sG7BjfizOM3V570oliDic2OgoOJhJmEgoOezYKLzZieiI+BiM2emYyDiYyfic2ZiJ+Ans2Mn4yOmYSOiM2emYyZiICIg5mew90025Isarg0SnRU368WNTicc5NMv1r2UH6vyf/HKuLwW6Bxs44lpm36y93J6+646eb+8KydnYGIza6In5mEi4SOjJmEgoPNrJiZhYKfhJmU3CT0nxiw4ziSsnYfyO5XuGKgsOAc3W/pVt1v7k5N7u/s7+/s793g6+Rc3bUBt+nfYYVeYvAziJ4SirOIUZqaw4ydnYGIw46CgMKMnZ2BiI6MYp5sjSv2tuTCf18VqaUdjdVz+BiZhIuEjoyZiM2PlM2Mg5TNnYyfmckPBjxanTLiqAzKJxyAlQAKWPr68mhuaPZ00KraH0R2rWPBOVx9/zXY39zZ3d7bt/rg3tjd393U39zZ3UZOnH+qvrgsQsKsXhUWDp0gC06hwt1sLuvlxuvs6Ojq7+/dbFv3bF54c5fhSapmtjn72t4mKeKgI/mEPLRK6OSR+q27/POZPlpmztaqTjiC4Ovkx2ulaxrg7Ozo6O3ub+zs7bHNgovNmYWIzZmFiIPNjJ2dgYSOjMdrpWsa4Ozs6Ojt3Y/c5t3k6+64RTGTz9gnyDg04juGOU/JzvwaTEHqAZDUbma+zT7VKVxSd6LnhhLGEaiT8qGGvXusZCmZj+b9bqxq3mdsZvRkMxSmgRjqRs/d7wX10xW95D6X3W/sm93j6+648OLs7BLp6e7v7PJ8NvOqvQboALOUacAG20+6obgBzYyDic2OiJ+ZhIuEjoyZhIKDzZ2ZhYKfhJmU3Pvd+evuuOnu/uCsnc2urN1v7M/d4Ovkx2ulaxrg7Ozs23ShwJVaAGF2MR6adh+bP5rdoizicNAexqTF9yUTI1hU4zSz8Tsm0OWz3W/s/OvuuPDN6W/s5d1v7Ond+9356+646e7+4KydnYGIzb+Cgplt+cY9hKp5m+QTGYZgw61LGqqgkpTNjJ6emICIns2Mjo6InZmMg46IvUdnODcJET3k6tpdmJjM");
        private static int[] order = new int[] { 8,44,34,3,4,29,35,27,31,50,34,23,14,56,38,50,21,20,30,20,35,57,43,42,46,58,59,34,42,35,31,59,36,55,39,53,49,39,46,39,59,52,47,52,44,54,51,58,57,58,50,54,53,56,57,56,56,57,58,59,60 };
        private static int key = 237;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
