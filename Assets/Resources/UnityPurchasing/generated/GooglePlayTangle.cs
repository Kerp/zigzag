#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("6IfSXRUTdaQwj0QGQtDSpYQDF+tDAAqU+lgJ1hGes9X7UIXn6mHUkMopJGgfCh927ue2pPbc8FjGmpc7/iJYVu7rKy11AwQVIqkQLlcpsydcLNqNBUfEb1Wgw8O1NNACluPaLV9n/lZwn7KNBHK/XFpEoTOCojQRzgboZ4hM3xMdUV33ZfVuDjTc37m+jrSRcTDgsoYLhjQz7Euj3ImYrw2/PB8NMDs0F7t1u8owPDw8OD0+8a5UrIK7tP3c2usYq1s8ifBQ8G3H5K0mmmwmKKwlWglGr9jfAYH8ir88Mj0Nvzw3P788PD2eDXy9vnVSv0dSprBNAVqZFFKB4fLpr7XB0Chub1tiis2p/oZD7ib3hEvDm+GBp4F2KXHEJJESZD8+PD08");
        private static int[] order = new int[] { 11,2,4,7,6,9,8,12,11,13,10,11,12,13,14 };
        private static int key = 61;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
